import { CommunitybarFormApplication } from './app-communitybar.js';
import { MacroUtil } from "../utils.js";

export class Communitybar {
    // ===============================================
    // CONSTRUCTOR
    // ===============================================
    constructor() {
        this.set_communitybar();
    }

    // ===============================================
    // REFRESH COMMUNITY BAR
    // ===============================================
    async refresh_communitybar() {
        await this.communitybarForm.fermer();
        this.communitybarForm = null;
        this.set_communitybar();
    }

    // ===============================================
    // BUILD AND DISPLAY THE COMMUNITY BAR
    // ===============================================
    async set_communitybar() {
        let mode = game.settings.get('tor-2e-macros', 'Community-Bar-Display-Mode');
        let canSeeDetails = this.can_see_details();

        // Template HTML 
        const template_communitybar_file = "modules/tor-2e-macros/html/bars/communitybar-" + mode + ".html";

        // Data qui seront envoyées au template HTML
        let community_photo = "";
        let community_info = "";
        let arrayCharacters = new Array();

        let macroUtil = new MacroUtil("");
        let actorCommunity = await macroUtil.getCommunity();

        if (actorCommunity != null && actorCommunity != '') {
            community_photo = this.get_photo("community", actorCommunity, "fromUuidSync(\"Actor." + actorCommunity.id + "\").sheet.render(true)");
            community_info = this.get_community_info(actorCommunity);

            actorCommunity.system.members.forEach(a => {
                let perso = game.actors.get(a.id);
                if (perso != undefined) {
                    if (perso.type != undefined) {
                        if (perso.type === "character") {
                            let character_photo = this.get_photo("character", perso, "fromUuidSync(\"Actor." + perso.id + "\").sheet.render(true)");
                            let character_info = this.get_character_info(canSeeDetails, perso);

                            arrayCharacters.push({ character_photo: character_photo, character_info: character_info });
                        }
                    }
                }
            })
            // Création de l'application qui affiche la fenêtre contenant la macrobar
            const template_communitybar_data = { community_photo: community_photo, community_info: community_info, characters: arrayCharacters };
            this.communitybarForm = new CommunitybarFormApplication(template_communitybar_data, { template: template_communitybar_file, option: "une option" }); // data, options   


            let communitybar_token_height_setting = game.settings.get('tor-2e-macros', 'Community-Bar-Token-Height');
            let communitybar_token_height_variant = "10px";
            if (communitybar_token_height_setting === "large") {
                communitybar_token_height_variant = "0px";
            } else if (communitybar_token_height_setting === "medium") {
                communitybar_token_height_variant = "10px";
            } else if (communitybar_token_height_setting === "small") {
                communitybar_token_height_variant = "20px";
            } else if (communitybar_token_height_setting === "tiny") {
                communitybar_token_height_variant = "30px";
            }

            const rootStyle = document.querySelector(':root').style;
            rootStyle.setProperty('--cb-base-height-variant', communitybar_token_height_variant);


            // Faire le rendu et afficher la fenêtre
            this.communitybarForm.render(true);
        }
    }
    // ===============================================
    // GET COMMUNITY INFO
    // ===============================================
    get_community_info(actorCommunity) {
        let code = "<section class='tor2e-cb-table-section'>";

        // -------------------------------------------
        // FELLOWSHIP POINTS
        // -------------------------------------------
        let pointsDeCommunaute = actorCommunity.system.fellowshipPoints.value;
        let pointsMaxDeCommunaute = actorCommunity.system.fellowshipPoints.max;

        code = code + "<div class='tor2e-cb-table-row'>";
        code = code + "     <div class='tor2e-cb-table-cell-icon'>";
        code = code + "         <img src='modules/tor-2e-macros/icons/Gandalf-blanc.webp' class='tor2e-cb-icon' />";
        code = code + "     </div>";
        code = code + "     <div class='tor2e-cb-table-cell-bar'>";
        code = code + "         <span class='tor2e-cb-text'>" + pointsDeCommunaute + " / " + pointsMaxDeCommunaute + "</span>";
        code = code + "     </div>";
        code = code + "</div>";

        // -------------------------------------------
        // EYE OF MORDOR
        // -------------------------------------------
        let eye_visibility_by_players = game.settings.get("tor-2e-macros", "Eye awareness visibility");
        if (game.user.isGM || (!game.user.isGM && eye_visibility_by_players === "all")) {
            let huntLimit = actorCommunity.system.eyeAwareness.max;
            let currentEye = actorCommunity.system.eyeAwareness.value;

            code = code + "<div class='tor2e-cb-table-row'>";
            code = code + "     <div class='tor2e-cb-table-cell-icon'>";
            code = code + "         <img src='modules/tor-2e-macros/icons/Oeil-blanc.webp' class='tor2e-cb-icon' />";
            code = code + "     </div>";
            code = code + "     <div class='tor2e-cb-table-cell-bar'>";
            code = code + "         <span class='tor2e-cb-text'>" + currentEye + " / " + huntLimit + "</span>";
            code = code + "     </div>";
            code = code + "</div>";
        }

        code = code + "</section>";

        return code;
    }

    // ===============================================
    // GET CHARACTER INFO
    // ===============================================
    get_character_info(canSeeDetails, perso) {
        let macroUtil = new MacroUtil("communitybar-content");

        let code = "<section class='tor2e-cb-table-section'>";

        // -------------------------------------------
        // ENDURANCE / FATIGUE POINTS
        // -------------------------------------------
        // Endurance
        let endurance = perso.system.resources.endurance.value;
        let enduranceMax = perso.system.resources.endurance.max;

        // Pourcentage endurance
        let pourcentage_endurance = Math.round(endurance / enduranceMax * 100);
        if (pourcentage_endurance > 100) {
            pourcentage_endurance = 100;
        }

        // Load and Fatigue
        let travelFatigue = perso.system.resources.travelLoad.value;
        let gearLoad = 0;
        perso.items.forEach(i => {
            if (i.type === "miscellaneous" || i.type === "weapon" || i.type === "armour") {
                if (i.system.dropped.value === false) {
                    gearLoad = gearLoad + i.system.load.value;
                }
            }
        });
        let totalLoad = travelFatigue + gearLoad;


        if (canSeeDetails || this.is_owner(perso.id)) {
            code = code + "<div class='tor2e-cb-table-row'>";
            code = code + "     <div class='tor2e-cb-table-cell-icon'>";
            code = code + "         <img src='modules/tor-2e-macros/icons/health_physical-icon.webp' class='tor2e-cb-icon'></img>";
            code = code + "     </div>";
            code = code + "     <div class='tor2e-cb-table-cell-bar'>";
            code = code + this.get_bar(perso, "character", "endurance", "health_physical-icon.webp", endurance, enduranceMax, totalLoad, canSeeDetails);
            code = code + "     </div>";
            code = code + "</div>";
        } else {

            let approx_0 = macroUtil.getTraduction("approximate_0");
            let approx_25 = macroUtil.getTraduction("approximate_25");
            let approx_50 = macroUtil.getTraduction("approximate_50");
            let approx_75 = macroUtil.getTraduction("approximate_75");
            let approx_100 = macroUtil.getTraduction("approximate_100");

            let couleur = "";
            let valeur = "";
            if (pourcentage_endurance > 75) {
                couleur = "#81d31a;";
                valeur = approx_100;
            } else if (pourcentage_endurance > 50) {
                couleur = "#a19e13;";
                valeur = approx_75;
            } else if (pourcentage_endurance > 25) {
                couleur = "#c06a0d;";
                valeur = approx_50;
            } else if (pourcentage_endurance > 0) {
                couleur = "#df3607;";
                valeur = approx_25;
            } else if (pourcentage_endurance === 0) {
                couleur = "#fe0200;";
                valeur = approx_0;
            }
            code = code + "<div class='tor2e-cb-table-row'>";
            code = code + "     <div class='tor2e-cb-table-cell-bar'>";
            code = code + "         <span class='tor2e-cb-approx' style='color:" + couleur + ";'>" + valeur + "</span>";
            code = code + "     </div>";
            code = code + "</div>";
        }

        // -------------------------------------------
        // HOPE / SHADOW POINTS
        // -------------------------------------------
        if (canSeeDetails || this.is_owner(perso.id)) {
            // Espoir
            let hope = perso.system.resources.hope.value;
            let hopeMax = perso.system.resources.hope.max;

            // Shadow
            let shadow = perso.system.resources.shadow.temporary.value + perso.system.resources.shadow.shadowScars.value;

            code = code + "<div class='tor2e-cb-table-row'>";
            code = code + "     <div class='tor2e-cb-table-cell-icon'>";
            code = code + "         <img src='modules/tor-2e-macros/icons/health_moral-icon.webp' class='tor2e-cb-icon'></img>";
            code = code + "     </div>";
            code = code + "     <div class='tor2e-cb-table-cell-bar'>";
            code = code + this.get_bar(perso, "character", "espoir", "health_moral-icon.webp", hope, hopeMax, shadow, canSeeDetails);
            code = code + "     </div>";
            code = code + "</div>";
        }

        code = code + "</section>";

        return code;
    }

    // NOT USED : Test for displaying values in place of a bar
    get_values(perso, typeActor, typeRessource, icon, current, max, limit, canSeeDetails) {
        let macroUtil = new MacroUtil("communitybar-content");
        let vertical_bar = game.settings.get("tor-2e-macros", "health_reports_vertical_bar");
        let persoId = perso.id;

        // Resource
        let pourcentage_resource = Math.round(current / max * 100);
        if (pourcentage_resource > 100) {
            pourcentage_resource = 100;
        }

        // Limit
        let pourcentage_limit = 0;
        if (limit >= max) {
            pourcentage_limit = 100;
        } else {
            pourcentage_limit = Math.round(limit / max * 100);
        }

        // Color
        let color_resource = "";
        if (typeRessource === "endurance") {
            color_resource = "darkred";
        } else {
            color_resource = "#3959bc";
        }

        let code = "";
        code = code + "               <div class='box overlay tor2e-cb-values'>";
        code = code + "                   <span class='tor2e-cb-text'>";
//        if (vertical_bar && limit != -1) {
//            code = code + "<span style='color: orange;'>(" + limit + ")</span>";
//        }
        code = code + "                   &nbsp;" + current + "/" + max + "";
        code = code + "                   </span>";
        code = code + "               </div>";
        code = code + "               <div class='box overlay tor2e-cb-clic' onClick='game.tor2eMacros.communitybar.focus_on_token(`" + persoId + "`)'></div>";
        code = code + "           </div>";

        return code;
    }

    // -------------------------------------------
    // GET BAR
    // -------------------------------------------    
    get_bar(perso, typeActor, typeRessource, icon, current, max, limit, canSeeDetails) {
        let macroUtil = new MacroUtil("communitybar-content");
        let vertical_bar = game.settings.get("tor-2e-macros", "health_reports_vertical_bar");
        let persoId = perso.id;

        // Resource
        let pourcentage_resource = Math.round(current / max * 100);
        if (pourcentage_resource > 100) {
            pourcentage_resource = 100;
        }

        // Limit
        let pourcentage_limit = 0;
        if (limit >= max) {
            pourcentage_limit = 100;
        } else {
            pourcentage_limit = Math.round(limit / max * 100);
        }

        // Color
        let color_resource = "";
        if (typeRessource === "endurance") {
            color_resource = "darkred";
        } else {
            color_resource = "#3959bc";
        }

        // Tooltip direction
        let tooltip_position = game.settings.get('tor-2e-macros', 'Community-Bar-Display-Tooltip-Position');


        let code = "";

        code = code + "           <div class='container tor2e-cb-bar-container'>";
        code = code + "               <div class='box tor2e-cb-bar-fond'></div>";
        code = code + "               <div class='box overlay tor2e-cb-bar-resource' style='width: " + pourcentage_resource + "%; background-color: " + color_resource + ";'>&nbsp;</div>";

        if (vertical_bar && limit != -1) {
            code = code + "               <div class='box overlay tor2e-cb-bar-threshold' style='width: " + pourcentage_limit + "%; '>&nbsp;</div>";
        }

        code = code + "               <div class='box overlay tor2e-cb-values'>";
        code = code + "                   <span class='tor2e-cb-text'>";
//        if (vertical_bar && limit != -1) {
//            code = code + "<span style='color: orange;'>(" + limit + ")</span>";
//        }
        code = code + "                   &nbsp;" + current + "/" + max + "";
        code = code + "                   </span>";
        code = code + "               </div>";
        code = code + "               <div class='box overlay tor2e-cb-clic' ";
        if (typeRessource === "endurance") {
            code = code + "               data-tooltip='" + this.get_health_endurance_tooltip(perso) + "' data-tooltip-direction='" + tooltip_position +"' ";
        } else {
            code = code + "               data-tooltip='" + this.get_health_hope_tooltip(perso) + "' data-tooltip-direction='" + tooltip_position +"' ";
        }
        code = code + "                 onClick='game.tor2eMacros.communitybar.focus_on_token(`" + persoId + "`)'></div>";
        code = code + "           </div>";

        return code;
    }

    get_health_endurance_tooltip(perso) {
        // Traductions
        let macroUtil = new MacroUtil("communitybar-tooltip");
        let trad_endurance = macroUtil.getTraduction("endurance");
        let trad_charge_totale = macroUtil.getTraduction("charge_totale");
        let trad_fatigue = macroUtil.getTraduction("fatigue");
        let trad_charge_equipement = macroUtil.getTraduction("charge_equipement");

        // DATA : Endurance, Load, Fatigue
        let endurance = perso.system.resources.endurance.value;
        let enduranceMax = perso.system.resources.endurance.max;
        let travelFatigue = perso.system.resources.travelLoad.value;
        let gearLoad = 0;
        perso.items.forEach(i => {
            if (i.type === "miscellaneous" || i.type === "weapon" || i.type === "armour") {
                if (i.system.dropped.value === false) {
                    gearLoad = gearLoad + i.system.load.value;
                }
            }
        });
        let totalLoad = travelFatigue + gearLoad;

        let fontSize = this.get_tooltip_text_size();

        let monToolTip = "<table >";
        monToolTip = monToolTip + "<tr>";
        monToolTip = monToolTip + "     <th style=\"text-align: left; font-size: " + fontSize + ";\" colspan=\"2\">" + trad_endurance +"</th>";
        monToolTip = monToolTip + "     <td style=\"padding-left: 5px; font-size: " + fontSize + ";\">" + endurance + " / " + enduranceMax + "</td>";
        monToolTip = monToolTip + "</tr><tr>";
        monToolTip = monToolTip + "     <th style=\"text-align: left; font-size: " + fontSize + ";\" colspan=\"2\">" + trad_charge_totale + "</th>";
        monToolTip = monToolTip + "     <td style=\"padding-left: 5px; font-size: " + fontSize + ";\">" + totalLoad + "</td>";
        monToolTip = monToolTip + "</tr><tr>";
        monToolTip = monToolTip + "     <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>";
        monToolTip = monToolTip + "     <th style=\"text-align: left; font-size: " + fontSize + ";\"><i>" + trad_fatigue + "</i></th>";
        monToolTip = monToolTip + "     <td style=\"padding-left: 5px; font-size: " + fontSize + ";\"><i>" + travelFatigue + "</i></td>";
        monToolTip = monToolTip + "</tr><tr>";
        monToolTip = monToolTip + "     <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>";
        monToolTip = monToolTip + "     <th style=\"text-align: left; font-size: " + fontSize + ";\"><i>" + trad_charge_equipement + "</i></th>";
        monToolTip = monToolTip + "     <td style=\"padding-left: 5px; font-size: " + fontSize + ";\"><i>" + gearLoad + "</i></td>";
        monToolTip = monToolTip + "</tr>";
        monToolTip = monToolTip + "</table>";

        return monToolTip;

    }

    get_tooltip_text_size() {
        let tokenHeight = game.settings.get('tor-2e-macros', 'Community-Bar-Token-Height');
        if(tokenHeight === "large") {
            return "medium";
        } else if (tokenHeight === "medium") {
            return "small";
        } else if (tokenHeight === "small") {
            return "smaller";
        } else if (tokenHeight === "tiny") {
            return "x-small";
        }
        return "medium";
    }

    get_health_hope_tooltip(perso) {
        // Traductions
        let macroUtil = new MacroUtil("communitybar-tooltip");
        let trad_espoir = macroUtil.getTraduction("espoir");
        let trad_ombre = macroUtil.getTraduction("ombre");
        let trad_ombre_temporaire = macroUtil.getTraduction("ombre_temporaire");
        let trad_sequelles = macroUtil.getTraduction("sequelles");
        let trad_defauts = macroUtil.getTraduction("defauts");

        // DATA : Hope, Shadow, Scars, Flaws
        let hope = perso.system.resources.hope.value;
        let hopeMax = perso.system.resources.hope.max;
        let tempShadow = perso.system.resources.shadow.temporary.value;
        let scars = perso.system.resources.shadow.shadowScars.value;
        let shadow = tempShadow + scars;
        let nbFlaws = 0;
        let flaws = "vide";
        perso.items.forEach(monItem => {
            if (monItem.type === "trait") {
                if (monItem.system.group.value === "flaw") {
                    nbFlaws++;
                    if(flaws != "vide") {
                        flaws = flaws + "<br>" + monItem.name;
                    } else {
                        flaws = monItem.name;
                    }
                }
            }
        });

        let fontSize = this.get_tooltip_text_size();

        let monToolTip = "<table>";
        monToolTip = monToolTip + "<tr>";
        monToolTip = monToolTip + "     <th style=\"text-align: left; font-size: " + fontSize + ";\" colspan=\"2\">" + trad_espoir +"</th>";
        monToolTip = monToolTip + "     <td style=\"padding-left: 10px; font-size: " + fontSize + ";\">" + hope + " / " + hopeMax + "</td>";
        monToolTip = monToolTip + "</tr><tr>";
        monToolTip = monToolTip + "     <th style=\"text-align: left; font-size: " + fontSize + ";\" colspan=\"2\">" + trad_ombre + "</th>";
        monToolTip = monToolTip + "     <td style=\"padding-left: 10px; font-size: " + fontSize + ";\">" + shadow + "</td>";
        monToolTip = monToolTip + "</tr><tr>";
        monToolTip = monToolTip + "     <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>";
        monToolTip = monToolTip + "     <th style=\"text-align: left; font-size: " + fontSize + ";\"><i>" + trad_ombre_temporaire + "</i></th>";
        monToolTip = monToolTip + "     <td style=\"padding-left: 10px; font-size: " + fontSize + ";\"><i>" + tempShadow + "</i></td>";
        monToolTip = monToolTip + "</tr><tr>";
        monToolTip = monToolTip + "     <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>";
        monToolTip = monToolTip + "     <th style=\"text-align: left; font-size: " + fontSize + ";\"><i>" + trad_sequelles + "</i></th>";
        monToolTip = monToolTip + "     <td style=\"padding-left: 10px; font-size: " + fontSize + ";\"><i>" + scars + "</i></td>";
        monToolTip = monToolTip + "</tr>";
        if(nbFlaws > 0) {
            monToolTip = monToolTip + "<tr>";
            monToolTip = monToolTip + "     <th style=\"text-align: left; font-size: " + fontSize + ";\" colspan=\"2\">" + trad_defauts + "</th>";
            monToolTip = monToolTip + "     <td style=\"padding-left: 10px; font-size: " + fontSize + ";\">" + flaws + "</td>";
            monToolTip = monToolTip + "</tr>";
        }
        monToolTip = monToolTip + "</table>";
        return monToolTip;
    }

    // ===============================================
    // GET PHOTO CODE
    // ===============================================
    get_photo(type, actor, macro) {
        let macroUtil = new MacroUtil("communitybar-tooltip");
        let trad_attachement = macroUtil.getTraduction("attachement");

        let stateWeary = false;
        let stateWounded = false;
        let statePoisoned = false;
        let stateMiserable = false;

        if (type === "character") {
            // DATA : Health state
            actor.effects.forEach(monEffet => {
                monEffet.statuses.forEach(monTypeEffet => {
                    if (monTypeEffet === "weary") {
                        stateWeary = true;
                    } else if (monTypeEffet === "wounded") {
                        stateWounded = true;
                    } else if (monTypeEffet == "poisoned") {
                        statePoisoned = true;
                    } else if (monTypeEffet === "miserable") {
                        stateMiserable = true;
                    }
                })
            });
        }

        // if necessary to add for drag&drop from community-bar to the scene : add << data-token-id='F5fz3tuj3bvdO8SU' >> inside the div below

        let mode = game.settings.get('tor-2e-macros', 'Community-Bar-Display-Mode');
        let tooltip_position = "UP";
        if (mode === "vertical") {
            tooltip_position = "RIGHT";
        }

        let focus = "";
        if(actor.type == "character") {
            focus = actor.system.biography.fellowshipFocus.value;
            if(focus != "") {
                focus = "<br><span style='font-size: smaller; color: grey;'>(" + trad_attachement + " : " + focus + ")</span>";
            }
        }

        let code = "<form>";
        code = code + "<div class='container tor2e-cb-photo-container'>";
        code = code + "     <div class='box tor2e-cb-photo-image token' data-actor-id='" + actor.id + "' movement draggable='true' >";

        if (game.user.isGM || type === "community" || (type === "character" && !game.user.isGM && game.user.character != null && game.user.character.id === actor.id)) {
            code = code + "         <button type='button' class='tor2e-cb-photo-button tor2e-cb-photo-button-grow' ";
            code = code + "             style='position: relative;' ";
            code = code + "             onClick='" + macro + ";' ";
            code = code + "             aria-label=\"" + actor.name + focus + "\" data-tooltip=\"" + actor.name + focus + "\" data-tooltip-direction=\"" + tooltip_position + "\" > ";
            code = code + "             <img src='" + actor.img + "' alt=\"" + actor.name + focus + "\" class='tor2e-cb-photo-button-img' />";
            code = code + "         </button>";
        } else {
            code = code + "         <img src='" + actor.img + "' alt=\"" + actor.name + "\" class='tor2e-cb-photo-button-img' ";
            code = code + "             aria-label=\"" + actor.name + "\" data-tooltip=\"" + actor.name + "\" data-tooltip-direction=\"" + tooltip_position + "\" ";
            code = code + "         />";
        }
        code = code + "     </div>";


        if (type === "character") {
            code = code + "<div class='box overlay' style='position: absolute; z-index: 9; display: flex; flex-orientation: row;'>";
            code = code + "";
            code = code + "";

            if (stateWeary) {
                code = code + "     <img src='systems/tor2e/assets/images/icons/effects/weary.svg' style='margin-right: 2px; max-width: 18px; max-height: 18px; background-color: black;' />";
            }
            if (stateWounded) {
                code = code + "     <img src='systems/tor2e/assets/images/icons/effects/wounded.svg' style='margin-right: 2px; max-width: 18px; max-height: 18px; background-color: black;' />";
            }
            if (statePoisoned) {
                code = code + "     <img src='systems/tor2e/assets/images/icons/effects/poisoned.svg' style='margin-right: 2px; max-width: 18px; max-height: 18px; background-color: black;' />";
            }
            if (stateMiserable) {
                code = code + "     <img src='systems/tor2e/assets/images/icons/effects/miserable.svg' style='margin-right: 2px; max-width: 18px; max-height: 18px; background-color: black;' />";
            }
            code = code + "</div>";
        }

        code = code + "</div>";
        code = code + "</form>";
        return code;
    }

    // ===============================================
    // CAN SEE DETAILS ?
    // ===============================================
    can_see_details(perso) {
        let canSeeDetails = false;
        if (game.user.isGM) {
            canSeeDetails = true;
        } else {
            let players_details = game.settings.get("tor-2e-macros", "Community-Bar-Players-Details");

            if (players_details) {
                canSeeDetails = true;
            }
        }
        return canSeeDetails;
    }

    // ===============================================
    // IS OWNER ?
    // ===============================================    
    is_owner(persoId) {
        if(game.user.character != null) {
            if (game.user.character.id === persoId) {
                return true;
            }
        }
        return false;
    }

    // ===============================================
    // FOCUS ON TOKEN
    // ===============================================    
    focus_on_token(persoId) {
        let token = null;
        canvas.tokens.objects.children.forEach(t => {
            if (t.document?.actorId === persoId) {
                token = t;
            }
        });

        if (token != null) {
            token.control({ releaseOthers: true });
            canvas.animatePan({ x: token.x, y: token.y });
        }
    }
}
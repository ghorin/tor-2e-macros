import { MacrobarFormApplication } from './app-macrobar.js';
import { MacroUtil } from "../utils.js";

export class Macrobar {

    // CONSTRUCOR of the Macrobar
    constructor() {
        this.set_macrobar();
    }

    // Function for closing the current Macrobar and opening a new Macrobar = refresh
    async refresh_macrobar() {
        await this.macrobarForm.fermer();
        this.set_macrobar();
    }

    // Building the new Macrobar
    set_macrobar() {
        let macroUtil = new MacroUtil("macros-group");

        // Récupération des settings d'affichage ou non des sous-barres (catégories)
        let groupTokens = game.settings.get("tor-2e-macros", "Macro-Bar-Tokens");
        let groupMessages = game.settings.get("tor-2e-macros", "Macro-Bar-Messages");
        let groupEye = game.settings.get("tor-2e-macros", "Macro-Bar-Eye");
        let groupReports = game.settings.get("tor-2e-macros", "Macro-Bar-Reports");
        let groupHealth = game.settings.get("tor-2e-macros", "Macro-Bar-Health");
        let groupDiceRolls = game.settings.get("tor-2e-macros", "Macro-Bar-DiceRolls");
        let groupCharacters = game.settings.get("tor-2e-macros", "Macro-Bar-Characters");

        // Récupération des settings de pliage / dépliage des sous-barres (catégories)
        // Puis calcul des propriété de style des sous-barres pour les plier ou déplier
        let settingDisplay_tokens = game.settings.get("tor-2e-macros", "groupBar-tokens");
        let settingDisplay_message = game.settings.get("tor-2e-macros", "groupBar-message");
        let settingDisplay_oeil = game.settings.get("tor-2e-macros", "groupBar-oeil");
        let settingDisplay_infos = game.settings.get("tor-2e-macros", "groupBar-infos");
        let settingDisplay_sante= game.settings.get("tor-2e-macros", "groupBar-sante");
        let settingDisplay_des = game.settings.get("tor-2e-macros", "groupBar-des");
        let settingDisplay_pjs = game.settings.get("tor-2e-macros", "groupBar-pjs");

        let displayGroup_tokens = "";
        if(settingDisplay_tokens)       displayGroup_tokens = "style=\"display: flex ;flex-direction: row;\"";
        else                            displayGroup_tokens = "style=\"display: none ;flex-direction: row;\"";

        let displayGroup_message = "";
        if(settingDisplay_message)      displayGroup_message = "style=\"display: flex ;flex-direction: row;\"";
        else                            displayGroup_message = "style=\"display: none ;flex-direction: row;\"";

        let displayGroup_oeil = "";
        if(settingDisplay_oeil)         displayGroup_oeil = "style=\"display: flex ;flex-direction: row;\"";
        else                            displayGroup_oeil = "style=\"display: none ;flex-direction: row;\"";

        let displayGroup_infos = "";
        if(settingDisplay_infos)        displayGroup_infos = "style=\"display: flex ;flex-direction: row;\"";
        else                            displayGroup_infos = "style=\"display: none ;flex-direction: row;\"";

        let displayGroup_sante = "";
        if(settingDisplay_sante)        displayGroup_sante = "style=\"display: flex ;flex-direction: row;\"";
        else                            displayGroup_sante = "style=\"display: none ;flex-direction: row;\"";

        let displayGroup_des = "";
        if(settingDisplay_des)          displayGroup_des = "style=\"display: flex ;flex-direction: row;\"";
        else                            displayGroup_des = "style=\"display: none ;flex-direction: row;\"";

        let displayGroup_pjs = "";
        if(settingDisplay_pjs)          displayGroup_pjs = "style=\"display: flex ;flex-direction: row;\"";
        else                            displayGroup_pjs = "style=\"display: none ;flex-direction: row;\"";

        

        // Template HTML 
        const template_macrobar_file = "modules/tor-2e-macros/html/bars/macrobar.html";

        // Conteneur des data qui seront envoyées au template HTML
        // Contenu :
        //      contentgroup :          identifiant de la catégorie
        //      contentgroupName :      Nom affiché de la catégorie
        //      contentbar :            code html affichant les boutons d'actions de la catégorie
        //      displaybar :            code du style css pour plier ou déplier la catégorie
        let arrayMacrogroups = new Array();
        if (groupTokens) arrayMacrogroups.push({ contentgroup: "tokens", contentgroupName: macroUtil.getTraduction("tokens"), contentbar: this.get_macrobar_group_launchers("tokens"), displayBar: displayGroup_tokens });
        if (groupMessages) arrayMacrogroups.push({ contentgroup: "message", contentgroupName: macroUtil.getTraduction("messages"), contentbar: this.get_macrobar_group_launchers("message"), displayBar: displayGroup_message });
        if (groupEye) arrayMacrogroups.push({ contentgroup: "oeil", contentgroupName: macroUtil.getTraduction("eye"), contentbar: this.get_macrobar_group_launchers("oeil"), displayBar: displayGroup_oeil });
        if (groupReports) arrayMacrogroups.push({ contentgroup: "infos", contentgroupName: macroUtil.getTraduction("reports"), contentbar: this.get_macrobar_group_launchers("infos"), displayBar: displayGroup_infos });
        if (groupHealth) arrayMacrogroups.push({ contentgroup: "sante", contentgroupName: macroUtil.getTraduction("health"), contentbar: this.get_macrobar_group_launchers("sante"), displayBar: displayGroup_sante });
        if (groupDiceRolls) arrayMacrogroups.push({ contentgroup: "des", contentgroupName: macroUtil.getTraduction("dice-roll"), contentbar: this.get_macrobar_group_launchers("des"), displayBar: displayGroup_des });
        if (groupCharacters) arrayMacrogroups.push({ contentgroup: "pjs", contentgroupName: macroUtil.getTraduction("characters"), contentbar: this.get_macrobar_group_launchers("pjs"), displayBar: displayGroup_pjs });

        let fold_unfold_button = "<form class='tor2e-macrobar-poigner-cacher-form'><button class='tor2e-macrobar-poigner-cacher-button' type='button' style='position: relative;' onClick='switchDisplay()' ><img src='modules/tor-2e-macros/images/poignee-plier.webp' class='tor2e-macrobar-poigner-cacher-img'/></button></form>";

        const template_macrobar_data = { macrogroups: arrayMacrogroups, fold_unfold: fold_unfold_button  };

        // Création de l'application qui affiche la fenêtre contenant la macrobar
        this.macrobarForm = new MacrobarFormApplication(template_macrobar_data, { template: template_macrobar_file, option: "une option" }); // data, options   

        // Faire le rendu et afficher la fenêtre
        this.macrobarForm.render(true);
    }

    // Building the CSS code for all the macro actions
    get_macrobar_group_launchers(group) {
        let macroUtil = new MacroUtil("macros");
        let macrobar_launchers = "";
        if (group === "des") {
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.skills_character_roll()", "systems/tor2e/assets/images/icons/skill.png", macroUtil.getTraduction("skill-roll"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.skills_private_roll()", "modules/tor-2e-macros/icons/skill_private_roll.webp", macroUtil.getTraduction("private-roll"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.combat_lanceAttaque()", "modules/tor-2e-macros/icons/attack.webp", macroUtil.getTraduction("attack"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.roll_ManualRoll()", "systems/tor2e/assets/images/chat/dice_icons/chat_f_gandalf.png", macroUtil.getTraduction("manual-dice-roll"));
        } else if (group === "oeil") {
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.community_eyeAwareness()", "modules/tor-2e-macros/images/EyeOfSauron.webp", macroUtil.getTraduction("increase-eye-awareness"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.community_huntLimit()", "icons/environment/wilderness/terrain-forest-gray.webp", macroUtil.getTraduction("change-region"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.adv_decrement_hate()", "modules/tor-2e-macros/icons/hate_down.webp", macroUtil.getTraduction("hate-down"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.adv_increment_hate()", "modules/tor-2e-macros/icons/hate_up.webp", macroUtil.getTraduction("hate-up"));
        } else if (group === "sante") {
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2e.macro.utility.setHealthStatus(\"weary\")", "systems/tor2e/assets/images/icons/effects/weary.svg", macroUtil.getTraduction("switch-status-weary"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2e.macro.utility.setHealthStatus(\"wounded\")", "systems/tor2e/assets/images/icons/effects/wounded.svg", macroUtil.getTraduction("switch-status-wounded"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2e.macro.utility.setHealthStatus(\"poisoned\")", "systems/tor2e/assets/images/icons/effects/poisoned.svg", macroUtil.getTraduction("switch-status-poisoned"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2e.macro.utility.setHealthStatus(\"miserable\")", "systems/tor2e/assets/images/icons/effects/miserable.svg", macroUtil.getTraduction("switch-status-miserable"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.token_Set_UnconsciousOrNot()", "icons/svg/unconscious.svg", macroUtil.getTraduction("unconscious-or-not"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.token_Set_DeadOrNot()", "icons/svg/skull.svg", macroUtil.getTraduction("dead-or-not"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.characters_Set_EtatPhysiqueMax()", "modules/tor-2e-macros/icons/health_max.webp", macroUtil.getTraduction("max-physical-status"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.characters_reduceWoundDuration()", "modules/tor-2e-macros/icons/day.webp", macroUtil.getTraduction("one_more_day"));
        } else if (group === "tokens") {
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.token_Set_Hidden_or_not()", "modules/tor-2e-macros/icons/invisible.webp", macroUtil.getTraduction("hidden-or-not"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.token_Set_Size()", "modules/tor-2e-macros/icons/token_resize.webp", macroUtil.getTraduction("change-size"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.token_DisplayTokenImage()", "modules/tor-2e-macros/icons/art_work.webp", macroUtil.getTraduction("show-image"));
            if(game.ghorinlightson != undefined) {
                macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.ghorinlightson.openGhorinLightOn()", "modules/ghorin-light-on/images/bougie.webp", macroUtil.getTraduction("ghorinlighton-module"));
            }
        } else if (group === "message") {
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.chat_SendMessage()", "modules/tor-2e-macros/icons/message.webp", macroUtil.getTraduction("send-message"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.chat_SendMessageBuble()", "modules/tor-2e-macros/icons/chat_bubble.webp", macroUtil.getTraduction("send-bubble-message"));
        } else if (group === "infos") {
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.characters_List_Health_Physical()", "modules/tor-2e-macros/icons/health_physical-icon.webp", macroUtil.getTraduction("report-physical-status"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.characters_List_Health_Moral()", "modules/tor-2e-macros/icons/health_moral-icon.webp", macroUtil.getTraduction("report-moral-status"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.characters_List_Valour_Wisdom()", "modules/tor-2e-macros/icons/valour_wisdom.webp", macroUtil.getTraduction("report-wisdom-valour"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.mj_charactersSynthesis()", "modules/tor-2e-macros/icons/group.webp", macroUtil.getTraduction("report-characters"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.characters_List_Experience()", "modules/tor-2e-macros/icons/experience.webp", macroUtil.getTraduction("report-experience"));
            macrobar_launchers = macrobar_launchers + this.get_tool_code(null, "game.tor2eMacros.characters_Add_SessionProgression()", "icons/skills/social/diplomacy-peace-alliance.webp", macroUtil.getTraduction("experience"));
        } else if (group === "pjs") {
            let actorCommunity = game.tor2e.macro.utility.getCommunity();
            if (actorCommunity == null || actorCommunity === '') {
                console.log("Macrobar : No Community found => Characters Group bar isn't displayed.");
            } else {
                macrobar_launchers = macrobar_launchers + this.get_tool_code(actorCommunity.id, "fromUuidSync(\"Actor." + actorCommunity.id + "\").sheet.render(true)", actorCommunity.img, actorCommunity.name);
                actorCommunity.system.members.forEach(a => {
                    let perso = game.actors.get(a.id);
                    if(perso === undefined) {
                        console.log("Macrobar : Character " + a.name + " (id : " + a.id + ") isn't found as Actor");
                    } else {
                        if (perso.type === undefined) {
                            console.log("Macrobar : Character " + a.name + " (id : " + a.id + ") is found as Actor but the actor doesn't have any Type property.");
                        } else {
                            if (perso.type === "character") {
                                macrobar_launchers = macrobar_launchers + this.get_tool_code(perso.id, "fromUuidSync(\"Actor." + perso.id + "\").sheet.render(true)", perso.img, perso.name);
                            }
                        }
                    }
                })
            }
        }
        return macrobar_launchers;
    }

    // Build the code for an action button
    get_tool_code(persoId, macro, icone, titre) {
        let dragandrop = "";
        let tokenclass = "";
        if(persoId != null) {
            tokenclass = "token";
            dragandrop = "data-actor-id='" + persoId + "' movement draggable='true'";
        }
        return "<form><button class='tor2e-macrobar-button tor2e-macrobar-button-grow " + tokenclass +"' " + dragandrop + " type='button' style='position: relative;' onClick='" + macro + ";' aria-label=\"" + titre + "\" data-tooltip=\"" + titre + "\" data-tooltip-direction=\"UP\" ><img src='" + icone + "' alt=\"" + titre + "\" class='tor2e-macrobar-button-img' /></button></form>";
    }
}

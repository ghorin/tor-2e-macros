export class CommunitybarFormApplication extends FormApplication {
    constructor(object, options) {
        super(object, options);
    }

    static get defaultOptions() {
        let posLeft = game.settings.get("tor-2e-macros", "Community-Bar-PosLeft");
        let posTop = game.settings.get("tor-2e-macros", "Community-Bar-PosTop");
        let mode = game.settings.get('tor-2e-macros', 'Community-Bar-Display-Mode');
        let classe = "tor2e-cb-window-" + mode;
        let template = "modules/tor-2e-macros/html/bars/communitybar-" + mode + ".html";

        return foundry.utils.mergeObject(super.defaultOptions, {
            id: "communitybar-id",
            template: template,
            closeOnSubmit: false,
            submitOnClose: false,
            submitOnChange: false,
            resizable: false,
            minimizable: false,
            popOut: true,
            editable: true,
            shareable: false,
            top: posTop,
            left: posLeft,
            width: "auto",
            classes: [classe, game.system.id],
            dragDrop: [{ dragSelector: ".token" }],
        });
    }

    _canDragStart(selector) {
        return game.user.isGM;
    }

    _onDragStart(event) {
        const target = event.currentTarget;
        const dragData = { uuid: `Actor.${target.dataset.actorId}`, type: "Actor" };
        event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
    }
    
    activateListeners(html) {
        super.activateListeners(html);
    }

    async fermer() {
        //super.close();
    }

    // =================================
    // Surcharge de la mathode close de FormApplication
    //
    // but : Empêcher la fermeture de la Macrobar via la touche ECHAP
    //
    // Mise en place : copier le contenu de la méthode d'origine de Application et FormApplication et neutraliser 
    //                  - le code qui appelle le super.close(...) (de Application)
    //                  - le code qui ferme les éléments
    // Code Foundry dans : resources/app/client/apps    form.js et apps.js
    // =================================
    close(options = {}) {
        // Used for closing of the Macrobar by the ESCape key
        //console.log("CLOSE of Macrobar : not possible with ESCAPE key");

        // ----------------------------------------------
        // ========== CODE DE FormApplication ===========
        // ----------------------------------------------
        // Trigger saving of the form
        const submit = options.submit ?? this.options.submitOnClose;
        if (submit) this.submit({ preventClose: true, preventRender: true });

        // Close any open FilePicker instances
        for (let fp of this.filepickers) {
            fp.close();
        }
        this.filepickers = [];

        // Close any open MCE editors
        for (let ed of Object.values(this.editors)) {
            if (ed.mce) ed.mce.destroy();
        }
        this.editors = {};


        // ----------------------------------------------
        // ==========   CODE DE Application   ===========
        // ----------------------------------------------
        const states = Application.RENDER_STATES;
        if (!options.force && ![states.RENDERED, states.ERROR].includes(this._state)) return;
        this._state = states.CLOSING;

        // Get the element
        let el = this.element;
        if (!el) return this._state = states.CLOSED;
        el.css({ minHeight: 0 });

        // Dispatch Hooks for closing the base and subclass applications
        for (let cls of this.constructor._getInheritanceChain()) {
            Hooks.call(`close${cls.name}`, this, el);
        }

        // Code fermant les éléments
        /*
            // Animate closing the element
            return new Promise(resolve => {
              el.slideUp(200, () => {
                el.remove();
        
                // Clean up data
                this._element = null;
                delete ui.windows[this.appId];
                this._minimized = false;
                this._scrollPositions = null;
                this._state = states.CLOSED;
                resolve();
              });
            });
        */

        // Code faisant appel au super.close() de Application
        //return super.close(options);        

    }

    setPosition(position) {
        game.settings.set("tor-2e-macros", "Community-Bar-PosLeft", position.left);
        game.settings.set("tor-2e-macros", "Community-Bar-PosTop", position.top);
        super.setPosition(position)
    };

    getData(options = {}) {
        return super.getData().object;
    }

    async _updateObject(event, formData) {
        return;
    }

}

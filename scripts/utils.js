export class MacroUtil {
    constructor(_perimetre) {
        this.perimetre = _perimetre;
    }

    getTraduction(_termeOriginal) {
        let _termeTraduit = game.i18n.localize("tor-2e-macros." + this.perimetre + "." + _termeOriginal + "");
        return _termeTraduit;
    }

    getFoundryMajorVersion() {
        let majorVersion = game.version.split(".")[0];
        return majorVersion;
    }

    isFoundryV12() {
        return (this.getFoundryMajorVersion() == "12");
    }

    async getCommunity() {
        let actorCommunity = game.tor2e.macro.utility.getCommunity();
        if (game.user.isGM) {
            // Gardien des Légendes
            if (actorCommunity == null || actorCommunity === '') {
                console.log("Communitybar : No default Community found => The Community bar isn't displayed.");
            }
        } else {
            // Joueur
            if (actorCommunity != null && actorCommunity != '') {
                if(game.user.character != null) {
                    // Test : est-ce que le joueur a un personnage dans cette communauté ?
                    let trouve = false;
                    actorCommunity.system.members.forEach(m => {
                        if(m.id === game.user.character.id) {
                            // Le joueur a bien un personnage dans la communauté active : nickel
                            trouve = true;
                        }
                    });
                    if(!trouve) {
                        // Le joueur n'a pas de personnage dans la communauté active, a t-il un joueur dans une autre communauté même non active ?
                        game.actors.forEach(i => {
                            if (i.type === "community") {
                                i.system.members.forEach(m => {
                                    if(m.id === game.user.character.id) {
                                        actorCommunity = i;
                                    }
                                });
                            }
                        });
                    }
                }
            } 
        }

        return actorCommunity;
    }
}
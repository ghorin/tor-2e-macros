import { MacroUtil } from "../utils.js";

class myDialog extends Dialog {
    processWeapon( weaponName ) {
        game.tor2e.macro.utility.rollItemMacro(weaponName, 'weapon'),
        this.close();
    }

    activateListeners( html ) {
        super.activateListeners(html);
        html.find('.weaponList').click(ev => {
            const weaponName = $(ev.currentTarget).data("weapon-name");    
            this.processWeapon( weaponName );
        });
    }
}  

export class MacroCombat {

    constructor() {
    }

    // ==================================================
    // LANCE UNE ATTAQUE D'UN PERSONNAGE
    // ==================================================
    async combat_lanceAttaque() {
        // Get traduction of terms      
        let macroUtil = new MacroUtil("combat_lanceAttaque");

        let trad_selectToken = macroUtil.getTraduction("selectToken");
        let trad_titre = macroUtil.getTraduction("titre");
        let trad_choixArme = macroUtil.getTraduction("choixArme");

        let trad_boutonArme = macroUtil.getTraduction("arme");
        let trad_boutonRang = macroUtil.getTraduction("rang");
        let trad_boutonDegats = macroUtil.getTraduction("degats");
        let trad_boutonBlessure = macroUtil.getTraduction("blessure");

        let trad_haches = macroUtil.getTraduction("haches");
        let trad_arcs = macroUtil.getTraduction("arcs");
        let trad_lances = macroUtil.getTraduction("lances");
        let trad_epees = macroUtil.getTraduction("epees");

        if (game.tor2eMacros.TOR2E_MACROS_DEBUG) {
            console.log('====================================================');
            console.log('========= TOR2E MACROS : ATTACK DEBUG MODE =========');
            console.log('====================================================');
            console.log("trad_haches=" + trad_haches);
            console.log("trad_arcs=" + trad_arcs);
            console.log("trad_lances=" + trad_lances);
            console.log("trad_epees=" + trad_epees);
        }

        // RECUPERER LE TOKEN
        let tokens = canvas.tokens.controlled;
        let token = null;
        if (tokens.length > 0) {
            token = tokens[0];
        }
        if (token == null) {
            ui.notifications.warn(trad_selectToken);
        } else {
            // PERSONNAGE
            let monPerso = token.actor;

            // FORM
            let pas = 0;
            let form = `<div style="display: inline-block;"><h3>`+trad_choixArme+`</h3></div>`;
            form = form + `<table>`;
            form = form + `<tr style='text-align:center'><th></th><th>`+trad_boutonArme+`</th><th>`+trad_boutonRang+`</th><th>`+trad_boutonDegats+`</th><th>`+trad_boutonBlessure+`</th></tr>`;

            if (monPerso.type === "adversary") {
                // CAS 1 : ADVERSAIRE
                monPerso.items.forEach(a => {
                    if (a.type == "weapon") {
                        form = form + `<tr style='text-align:center'>`;
                        form = form + `<td><input type="image" src="` + a.img + `" width="64px" height="64px" class="weaponList" data-weapon-name="${a.name}" border="0px" id="weapon${pas}"></input></td>`;
                        form = form + `<td style='text-align:left'>&nbsp;` + a.name + `</td>`;
                        form = form + `<td>` + a.system.skill.value + `</td>`;
                        form = form + `<td>` + a.system.damage.value + `</td>`;
                        form = form + `<td>` + a.system.injury.value + `</td>`;
                        form = form + `</tr>`;
                        pas = pas + 1;
                    }
                });
            } else if (monPerso.type === "character") {
                // CAS 2 : PERSONNAGE JOUEUR
                monPerso.items.forEach(a => {
                    if (a.type == "weapon") {
                        let nomCompetence = a.system.skill.name;
                        let nomCompetenceCulturelle = "";
                        let groupeCompetence = a.system.group.value;
                        let equipee = a.system.equipped.value;
                        let nomCompetenceUtilisee = nomCompetence;
                        if (equipee == true) {
                            if (game.tor2eMacros.TOR2E_MACROS_DEBUG) console.log("========== " + a.name + " ==========");
                            if (game.tor2eMacros.TOR2E_MACROS_DEBUG) console.log("Data : \n..... Skill=" + nomCompetence + "\n..... Group=" + groupeCompetence);
                            if (game.tor2eMacros.TOR2E_MACROS_DEBUG) console.log("Weapon skill ?");

                            let rangCompetence = -1;
                            if( groupeCompetence === "swords" ) {
                                // Epées
                                rangCompetence = monPerso.system.combatProficiencies.swords.value;
                            } else if( groupeCompetence === "axes" ) {
                                // Haches
                                rangCompetence = monPerso.system.combatProficiencies.axes.value;
                            } else if( groupeCompetence === "bows" ) {
                                // Bows
                                rangCompetence = monPerso.system.combatProficiencies.bows.value;
                            } else if( groupeCompetence === "spears" ) {
                                // Lances
                                rangCompetence = monPerso.system.combatProficiencies.spears.value;
                            } else if( groupeCompetence === "brawling" ) {
                                // Brawling = (valeur max des autres compétences de combat) -1
                                rangCompetence = monPerso.system.combatProficiencies.swords.value;       // swords
                                if(monPerso.system.combatProficiencies.axes.value > rangCompetence) {    // axes
                                    rangCompetence = monPerso.system.combatProficiencies.axes.value;
                                }
                                if(monPerso.system.combatProficiencies.bows.value > rangCompetence) {    // bows
                                    rangCompetence = monPerso.system.combatProficiencies.bows.value;
                                }
                                if(monPerso.system.combatProficiencies.spears.value > rangCompetence) {    // spears
                                    rangCompetence = monPerso.system.combatProficiencies.spears.value;
                                }                                                                                               
                                rangCompetence = rangCompetence - 1;  
                                if( rangCompetence < 0) {
                                    rangCompetence = 0;
                                }
                            }
                            form = form + `<tr style='text-align:center'>`;
                            form = form + `<td><input type="image" src="` + a.img + `" width="64px" height="64px" class="weaponList" data-weapon-name="${a.name}" border="0px" id="weapon${pas}"></input></td>`;
                            form = form + `<td style='text-align:left'>` + a.name + `</td>`;
                            form = form + `<td>` + rangCompetence + `</td>`;
                            form = form + `<td>` + a.system.damage.value + `</td>`;
                            form = form + `<td>` + a.system.injury.value + `</td>`;
                            form = form + `</tr>`;
                            pas = pas + 1;
                        }
                    }
                });

                pas = pas + 1;                
            }
            // FIN DU FORM
            form = form + `</table>`;

            // AFFICHER LA BOITE DE DIALOGUE DE CHOIX DE L'ARME
            let data = {
                title: trad_titre + " " + monPerso.name,
                content: form,
                buttons: {
                    //    use: {
                    //        label: "Fermer",
                    //        callback: html => close()
                    //    }
                },
                default: "Fermer",
                close: () => { }
            };
            const options = { width: 600, height: 70 + (pas * 90) };
            let mydiag = new myDialog(data, options);
            mydiag.render(true);
        }
    }
}
export class MacroDev {

    /*
    OUTILS DE DEV
    - Supprimer
        - tout supprimer
        - actors
        - items
    - Lister les Active Effects des PJ
    - Hooks debug mode      CONFIG.debug.hooks = true 


    */

    async dev_Delete_all() {
        new Dialog({
            title: 'Removal of Actors, Items and Folders',
            content: `
              <form>
                <div class="form-group">
                    <table>
                        <tr>
                            <th nowrap style="text-align: left;">Type of objects to remove</th>
                            <td>
                                <select id="objet-Type" multiple rows=5 style='width: 150px' />
                                    <option value="none" selected>None</option>
                                    <option value="all">All</option>
                                    <option value="actors">Characters</option>
                                    <option value="items">Items</option>
                                    <option value="folders">Folders</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th nowrap style="text-align: left;">Only actors or items at root folder</th>
                            <td>
                                <select id="objet-racine" />
                                <option value="non" selected>No</option>
                                <option value="oui">Yes</option>
                                </select>
                          </td>
                        </tr>
                    </table
                </div>
              </form>`,
            buttons: {
                yes: {
                    icon: "<i class='fas fa-check'></i>",
                    label: `Delete`
                }
            },
            default: 'yes',
            close: html => {

                let objetType = html.find('select#objet-Type')[0]?.value || null;
                let objetRacine = html.find('select#objet-racine')[0]?.value || null;

                if (objetType !== null && objetRacine != null) {
                    if (objetType === "none") {
                        console.log("none");
                    } 
                    if (objetType === "items" || objetType === "all") {
                        game.items.forEach(t => {
                            if (objetRacine === "oui") {
                                if (!t.folder) {
                                    t.delete();
                                }
                            } else {
                                t.delete();
                            }
                        });
                    }

                    if (objetType === "actors" || objetType === "all") {
                        game.actors.forEach(t => {
                            if (objetRacine === "oui") {
                                if (!t.folder) {
                                    t.delete();
                                }
                            } else {
                                t.delete();
                            }
                        });
                    }

                    if (objetType === "folders" || objetType === "all") {
                        game.folders.forEach(t => {
                            if (objetRacine === "oui") {
                                if (!t.folder) {
                                    t.delete();
                                }
                            } else {
                                t.delete();
                            }
                        });
                    }
                }
            }
        },{width:400, height:240}).render(true);
    }

    async dev_Debug() {
        new Dialog({
            title: 'Debug Actor, Item or Folder',
            content: `
              <form>
                <div class="form-group">
                    <table>
                        <tr>
                            <th nowrap style="text-align: left;">Type</th>
                            <td>
                                <select id="objet-Type" multiple rows=3 style='width: 150px' />
                                    <option value="actors" selected>Character</option>
                                    <option value="items">Item</option>
                                    <option value="folders">Folder</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th nowrap style="text-align: left;">Name</th>
                            <td>
                                <input type='text' name='objet-Nom' style='width: 250px'></input>
                            </td>
                        </tr>
                    </table
                </div>
              </form>`,
            buttons: {
                yes: {
                    icon: "<i class='fas fa-check'></i>",
                    label: `Debug`
                }
            },
            default: 'yes',
            close: html => {

                let objetType = html.find('select#objet-Type')[0]?.value || null;
                let objetNom = html.find('input[name=\'objet-Nom\']').val();

                if (objetNom !== '') {
                    if (objetType === "items") {
                        game.items.forEach(t => {
                            if (t.name == objetNom) {
                                console.log(t);
                            }
                        });
                    }

                    if (objetType === "actors") {
                        game.actors.forEach(t => {
                            if (t.name == objetNom) {
                                console.log(t);
                            }
                        });
                    }

                    if (objetType === "folders") {
                        game.folders.forEach(t => {
                            if (t.name == objetNom) {
                                console.log(t);
                            }
                        });
                    }
                }
            }
        },{width:350, height:240}).render(true);
    }
}

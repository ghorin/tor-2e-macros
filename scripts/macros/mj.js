import { MacroUtil } from "../utils.js";

class myDialogCharacterListAction extends Dialog
{
    processActorArt(actor_id)
    {
        game.actors.get(actor_id).sheet.render(true);
        // this.close();
    }

    activateListeners(html)
    {
        super.activateListeners(html);
        html.find('.unActor').click(ev =>
        {
            const actor_id = $(ev.currentTarget).data("actor-id");
            this.processActorArt(actor_id);
        });
    }
}

export class MacroMj {

    constructor() {
        this.macroUtil = null;
    }

    // ==================================================
    // LIST OF ACTIVE EFFECTS ON CHARACTERS
    // ==================================================
    listCharactersActiveEffects() {
        let message = {
            content: "<B>HEROES : ITEMS WITH ACTIVE EFFECTS</B><br>",
            whisper: ChatMessage.getWhisperRecipients('GM'),
          };
          
          game.actors.forEach(perso => {
            if(perso.type === "character") {
              message.content = message.content + "- " + perso.name + "<ul>";
              
              perso.items.forEach(item => {
                if(item.effects?.size > 0) {
                  item.effects.forEach(effect => {
                    message.content = message.content + "<li>"+item.name+" ("+item.type+") => "+effect.name+"</li>";
                  });
                }
              });
              message.content = message.content + "</ul>";
            }
          });
          
          ChatMessage.create(message);
    }

    // ==================================================
    // OPTIONS
    // ==================================================
    getOption(nomOption) {
        /*
        let sizeTokenImage = 64;
        let sizeDiamondRank = 10;
        let displayEmptyRanks = false;
        let windowWidth = 1600;
        let windowHeight = 1050; // 850;
        let col1PercentWidth = 12;
        let col2PercentWidth = 12;
        let primaryColor = "#ce5860";
        let diamondSize = 24;
        */        
        switch (nomOption) {
            case "sizeTokenImage":
              return 64;
              break;
            case "sizeDiamondRank":
                return 10;
              break;
            case "displayEmptyRanks":
              return false;
              break;
            case "windowWidth":
              return 1600;
              break;
            case "windowHeight":
                return 1050;
                break;
            case "col1PercentWidth":
              return 12;
              break;
            case "col2PercentWidth":
                return 12;
                break;
            case "primaryColor":
                return "#ce5860";
                break;
            case "diamondSize":
                return 24;
                break;
            default:
              console.log("Missing option : " + nomOption);
          }
    }

    // ==================================================
    // TRADUCTION
    // ==================================================
    getTrad(texte) {
        return this.macroUtil.getTraduction(texte);
    }

    async mj_charactersSynthesis() {
        // --------------------------------------------
        // Get traduction of terms      
        // --------------------------------------------
        this.macroUtil = new MacroUtil("mj_charactersSynthesis");
        
        // --------------------------------------------
        // LIST OF ACTORS
        // --------------------------------------------
        this.listeActors = null;
        this.actorCommunity = null;
        this.content_form_window = null;

        this.setCommunityAndActors();

        // --------------------------------------------
        // DIALOG CONTENTS
        // --------------------------------------------
        this.content_form_window = "";
        this.content_form_window = this.content_form_window + `<div><table>`;
        this.ajouteCompagnie();
        this.ajouteActorTokenImage();
        this.ajouteActorNom();
        this.ajouteActorCulture();
        this.ajouteActorAvantageCulturel();
        this.ajouteActorVocation();
        this.ajouteActorPartOmbre();
        this.ajouteActorDefautsOmbre();
        this.ajouteActorLienCommunaute();
        this.ajouteActorNiveauDeVie();
        this.ajouteParticularites();
        this.ajouteVertus();
        this.ajouteRecompenses();
        this.ajouteStats("Stats", "valour");
        this.ajouteStats("", "wisdom");
        this.ajouteAttribut("strength");
        this.ajouteCompetence("awe");
        this.ajouteCompetence("athletics");
        this.ajouteCompetence("awareness");
        this.ajouteCompetence("hunting");
        this.ajouteCompetence("song");
        this.ajouteCompetence("craft");
        this.ajouteAttribut("heart");
        this.ajouteCompetence("enhearten");
        this.ajouteCompetence("travel");
        this.ajouteCompetence("insight");
        this.ajouteCompetence("healing");
        this.ajouteCompetence("courtesy");
        this.ajouteCompetence("battle");
        this.ajouteAttribut("wits");
        this.ajouteCompetence("persuade");
        this.ajouteCompetence("stealth");
        this.ajouteCompetence("scan");
        this.ajouteCompetence("explore");
        this.ajouteCompetence("riddle");
        this.ajouteCompetence("lore");
        this.ajouteCompetencesArme();
        this.ajouteArmes();
        this.ajouteArmures();
        this.ajouteEquipementNonMartial();
        this.content_form_window = this.content_form_window + `</table></div>`;        


        // ============================================
        // DIALOG
        // ============================================
        let data = {
            title: this.getTrad("trad_title"),
            content: this.content_form_window,
            buttons: {
                fermer: {
                    label: game.i18n.localize("Close"),
                },
            },
            default: game.i18n.localize("Close"),
            close: html => { },            
        };
        const options = { width: this.getOption("windowWidth"), height: this.getOption("windowHeight") };
        let mydiag = new myDialogCharacterListAction(data, options);
        mydiag.render(true);        
    }


    // ============================================
    // Function : Compagnie
    // ============================================
    ajouteCompagnie() {
        this.content_form_window = this.content_form_window + `<tr><th style="border-bottom: inset; font-size: large; color:` + this.getOption("primaryColor") + `; text-transform: uppercase;" colspan=` + (this.listeActors.length + 2) + `>` + this.actorCommunity.name + `</th></tr>`;
    }

    // ============================================
    // Function : Entete de ligne
    // ============================================
    ajouteEnteteLigne(col_1, col_2) {
        let table_border_top = "";
        if( col_1 != "" ) {
            table_border_top = "border-top: inset; ";
        }
        return `<tr style="` + table_border_top + `vertical-align: top;"><th style='width="` + this.getOption("col1PercentWidth") + `%"; text-align:left; color:` + this.getOption("primaryColor") + `'>` + col_1 + `</th><th style='width="` + this.getOption("col2PercentWidth") + `%"; text-align:left'>` + col_2 + `</th>`;
    }
    // ============================================
    // Function : Fin de ligne
    // ============================================
    ajouteFinLigne() {
        return `</tr>`;
    }

    // ============================================
    // Function : TOKEN IMAGE
    // ============================================
    ajouteActorTokenImage() {
        let content = "";
        let colActorPercentWidth = (100 - this.getOption("col1PercentWidth") - this.getOption("col2PercentWidth")) / this.listeActors.length;
        content = content + this.ajouteEnteteLigne("", "");
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            let donnee = perso.img;
            content = content + `<td style="width:` + colActorPercentWidth + `%;"><input type='image' style='border-style: none; max-width: `+ this.getOption("sizeTokenImage") + `px; max-heigth: `+ this.getOption("sizeTokenImage") + `px;' src='` + donnee + `' class='unActor' data-actor-id='` + perso.id + `' /></td>`;
        }
        content = content + this.ajouteFinLigne();
        this.content_form_window = this.content_form_window + content;
    }

    // ============================================
    // Function : NOM
    // ============================================
    ajouteActorNom() {
        let content = "";
        content = content + this.ajouteEnteteLigne(game.i18n.localize("Character"), game.i18n.localize("Name"));
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            let donnee = perso.name;
            content = content + `<th style='text-align: left; color:` + this.getOption("primaryColor") + `'>` + donnee.toUpperCase() + `</th>`;
        }
        content = content + this.ajouteFinLigne();
        this.content_form_window = this.content_form_window + content;
    }

    // ============================================
    // Function : CULTURE
    // ============================================
    ajouteActorCulture() {
        let content = "";
        content = content + this.ajouteEnteteLigne("", game.i18n.localize("tor2e.actors.biography.culture"));
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            let donnee = perso.system.biography.culture.value;
            content = content + `<td>` + donnee + `</td>`;
        }
        content = content + this.ajouteFinLigne();
        this.content_form_window = this.content_form_window + content;
    }

    // ============================================
    // Function : AVANTAGE CULTUREL
    // ============================================
    ajouteActorAvantageCulturel() {
        let content = "";
        content = content + this.ajouteEnteteLigne("", game.i18n.localize("tor2e.actors.biography.culturalBlessing"));
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            let donnee = perso.system.biography.culturalBlessing.value;
            content = content + `<td>` + donnee + `</td>`;
        }
        content = content + this.ajouteFinLigne();
        this.content_form_window = this.content_form_window +  content;
    }

    // ============================================
    // Function : VOCATION
    // ============================================
    ajouteActorVocation() {
        let content = "";
        content = content + this.ajouteEnteteLigne("", game.i18n.localize("tor2e.actors.biography.calling"));
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            let donnee = perso.system.biography.calling.value;
            content = content + `<td>` + game.i18n.localize("tor2e.callings.groups."+donnee) + `</td>`;
        }
        content = content + this.ajouteFinLigne();
        this.content_form_window = this.content_form_window +  content;
    }

    // ============================================
    // Function : PART D'OMBRE
    // ============================================
    ajouteActorPartOmbre() {
        let content = "";
        content = content + this.ajouteEnteteLigne("", game.i18n.localize("tor2e.actors.biography.shadowPath"));
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            let donnee = perso.system.biography.shadowPath.value;
            content = content + `<td>` + donnee + `</td>`;
        }
        content = content + this.ajouteFinLigne();
        this.content_form_window = this.content_form_window +  content;
    }

    // ============================================
    // Function : DEFAUTS D'OMBRE
    // ============================================
    ajouteActorDefautsOmbre() {
        let content = "";
        content = content + this.ajouteEnteteLigne("", game.i18n.localize("tor2e.actors.traits.flaws"));
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            let nbDefauts = 0;
            content = content + `<td>`;
            perso.items.forEach(a => {
                if (a.type === "trait" && a.system.group.value === "flaw") {
                    let donnee = a.name;
                    if( nbDefauts > 0 ) {
                        content = content + `<br>`;
                    }
                    content = content + donnee;
                    nbDefauts = nbDefauts + 1;
                }
            });
            content = content + `</td>`;   
        }
        content = content + this.ajouteFinLigne();
        this.content_form_window = this.content_form_window +  content;
    }

    // ============================================
    // Function : LIEN DE COMMUNAUTE
    // ============================================
    ajouteActorLienCommunaute() {
        let content = "";
        content = content + this.ajouteEnteteLigne("", game.i18n.localize("tor2e.actors.biography.fellowshipFocus"));
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            let donnee = perso.system.biography.fellowshipFocus.value;
            content = content + `<td>` + donnee + `</td>`;
        }
        content = content + this.ajouteFinLigne();
        this.content_form_window = this.content_form_window + content;
    }

    // ============================================
    // Function : NIVEAU DE VIE
    // ============================================
    ajouteActorNiveauDeVie(listectors) {
        let content = "";
        content = content + this.ajouteEnteteLigne("", game.i18n.localize("tor2e.actors.biography.standardOfLiving"));
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            let donnee = game.i18n.localize("tor2e.standardOfLivingGroups."+perso.system.biography.standardOfLiving.value);
            content = content + `<td>` + donnee + `</td>`;
        }
        content = content + this.ajouteFinLigne();
        this.content_form_window = this.content_form_window +  content;
    }

    // ============================================
    // Function : VERTUS
    // ============================================
    ajouteVertus() {
        let content = "";
        content = content + this.ajouteEnteteLigne("", game.i18n.localize("tor2e.actors.sections.virtues"));
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            let nbItems = 0;
            content = content + `<td style="vertical-align: top;">`;
            perso.items.forEach(a => {
                if (a.type === "virtues") {
                    let donnee = a.name;
                    if( nbItems > 0 ) {
                        content = content + `<br>`;
                    }
                    content = content + donnee;
                    nbItems = nbItems + 1;
                }
            });
            content = content + `</td>`;   
        }
        content = content + this.ajouteFinLigne();
        this.content_form_window = this.content_form_window +  content;
    }

    // ============================================
    // Function : RECOMPENSES
    // ============================================
    ajouteRecompenses() {
        let content = "";
        content = content + this.ajouteEnteteLigne("", game.i18n.localize("tor2e.actors.sections.rewards"));
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            let nbItems = 0;
            content = content + `<td style="vertical-align: top;">`;
            perso.items.forEach(a => {
                if (a.type === "reward") {
                    let donnee = a.name;
                    if( nbItems > 0 ) {
                        content = content + `<br>`;
                    }
                    content = content + donnee;
                    nbItems = nbItems + 1;
                }
            });
            content = content + `</td>`;   
        }
        content = content + this.ajouteFinLigne();
        this.content_form_window = this.content_form_window +  content;
    }

    // ============================================
    // Function : PARTICULARITES
    // ============================================
    ajouteParticularites() {
        let content = "";
        content = content + this.ajouteEnteteLigne("", game.i18n.localize("tor2e.actors.traits.distinctiveFeatures"));
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            let nbItems = 0;
            content = content + `<td style="vertical-align: top;">`;
            perso.items.forEach(a => {
                if (a.type === "trait" && a.system.group.value == "distinctiveFeature") {
                    let donnee = a.name;
                    if( nbItems > 0 ) {
                        content = content + `<br>`;
                    }
                    content = content + donnee;
                    nbItems = nbItems + 1;
                }
            });
            content = content +  `</td>`;   
        }
        content = content + this.ajouteFinLigne();
        this.content_form_window = this.content_form_window +  content;
    }

    // ============================================
    // Function : COMPETENCES D'ARME
    // ============================================
    ajouteCompetencesArme() {
        let content = "";
        // Compétence : Epées
        content = content + this.ajouteEnteteLigne(game.i18n.localize("tor2e.actors.sections.combatProficiencies"), game.i18n.localize("tor2e.combatProficiencies.swords"));
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            let rang = perso.system.combatProficiencies["swords"].value;
            content = content + `<td style="vertical-align: top;">`;
            content = content + this.ajouteRangs(rang, false);
            content = content + `</td>`;
        }
        content = content + this.ajouteFinLigne();
        
        // Compétence : Haches
        content = content + this.ajouteEnteteLigne("", game.i18n.localize("tor2e.combatProficiencies.axes"));
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            let rang = perso.system.combatProficiencies["axes"].value;
            content = content + `<td style="vertical-align: top;">`;
            content = content + this.ajouteRangs(rang, false);
            content = content + `</td>`;
        }
        content = content + this.ajouteFinLigne();
        
        // Compétence : Lances
        content = content + this.ajouteEnteteLigne("", game.i18n.localize("tor2e.combatProficiencies.spears"));
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            let rang = perso.system.combatProficiencies["spears"].value;
            content = content + `<td style="vertical-align: top;">`;
            content = content + this.ajouteRangs(rang, false);
            content = content + `</td>`;
        }
        content = content + this.ajouteFinLigne();
        
        // Compétence : Arcs
        content = content + this.ajouteEnteteLigne("", game.i18n.localize("tor2e.combatProficiencies.bows"));
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            let rang = perso.system.combatProficiencies["bows"].value;
            content = content + `<td style="vertical-align: top;">`;
            content = content + this.ajouteRangs(rang, false);
            content = content + `</td>`;
        }
        content = content + this.ajouteFinLigne();    
        
        // Compétence : Bagarre
        content = content + this.ajouteEnteteLigne("", game.i18n.localize("tor2e.combatProficiencies.brawling"));
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            let rangHaches = perso.system.combatProficiencies["axes"].value;
            let rangEpees = perso.system.combatProficiencies["swords"].value;
            let rangLances = perso.system.combatProficiencies["spears"].value;
            let rangArcs = perso.system.combatProficiencies["bows"].value;
            let rang = Math.max(rangHaches, rangEpees, rangLances, rangArcs)-1;
            if(rang == -1) {
                rang = 0;
            }
            content = content + `<td style="vertical-align: top;">`;
            content = content + this.ajouteRangs(rang, false);
            content = content + `</td>`;
        }
        content = content + this.ajouteFinLigne();
        this.content_form_window = this.content_form_window +  content;
    }

    // ============================================
    // Function : ARMES EQUIPEES
    // ============================================
    ajouteArmes() {
        let content = "";
        content = content + this.ajouteEnteteLigne(game.i18n.localize("tor2e.actors.sections.equipment-gear"), this.getTrad("trad_armes_equipees") + '<br><span style="font-style: italic; font-weight: lighter; font-size: smaller; color: #797373;">(' + this.getTrad("trad_degats_blessure") + ')</span>');
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            content = content + `<td style="vertical-align: top;">`;
            perso.items.forEach(a => {
                if (a.type === "weapon" && a.system.equipped.value == true) {
                    let donnee = a.name;
                    let degats = a.system.damage.value;
                    let blessure = a.system.injury.value;
                    content = content + donnee + ` <span style="font-style: italic; font-weight: lighter; font-size: smaller; color: #797373;">(` + degats + ` / ` + blessure + `)</span><br>`;
                }
            });
            content = content + `</td>`;   
        }
        content = content + this.ajouteFinLigne();
        this.content_form_window = this.content_form_window +  content;
    }

    // ============================================
    // Function : ARMURES EQUIPEES
    // ============================================
    ajouteArmures() {
        let content = "";
        content = content + this.ajouteEnteteLigne("", this.getTrad("trad_armures_equipees") + '<br><span style="font-style: italic; font-weight: lighter; font-size: smaller; color: #797373;">(' + this.getTrad("trad_protection") + ')</span>');
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            content = content + `<td style="vertical-align: top;">`;
            perso.items.forEach(a => {
                if (a.type === "armour" && a.system.equipped.value == true) {
                    let donnee = a.name;
                    let detail = a.system.protection.value;
                    content = content + donnee + ` <span style="font-style: italic; font-weight: lighter; font-size: smaller; color: #797373;">(` + detail + `)</span><br>`;
                }
            });
            content = content + `</td>`;   
        }
        content = content + this.ajouteFinLigne();
        this.content_form_window = this.content_form_window +  content;
    }


    // ============================================
    // Function : EQUIPEMENT NON MARTIAL
    // ============================================
    ajouteEquipementNonMartial() {
        let content = "";
        content = content + this.ajouteEnteteLigne("", game.i18n.localize("tor2e.actors.sections.equipment-gear"));
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            content = content + `<td style="vertical-align: top;">`;
            perso.items.forEach(a => {
                if (a.type === "miscellaneous") {
                    let donnee = a.name;
                    content = content + donnee + `<br>`;
                }
            });
            content = content + `</td>`;   
        }
        content = content + this.ajouteFinLigne();
        this.content_form_window = this.content_form_window +  content;
    }

    // ============================================
    // Function : STATS
    // ============================================
    ajouteStats(titre, nomStat) {
        let content = "";
        if(titre != "") {
            content = content + this.ajouteEnteteLigne(game.i18n.localize(titre), game.i18n.localize("tor2e.statureStats."+nomStat));
        } else {
            content = content + this.ajouteEnteteLigne("", game.i18n.localize("tor2e.statureStats."+nomStat));
        }
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            let rang = perso.system.stature[""+nomStat+""].value;
            content = content + `<th style='text-align: left; vertical-align: middle; white-space: nowrap;'>`;
            content = content + this.ajouteDiamant(rang);
            content = content + `</th>`;
        }
        this.ajouteFinLigne();
        this.content_form_window = this.content_form_window +  content;
    }

    ajouteDiamant(valeur) {
        return `<div style="display: inline-block; justify-content: center; flex: none; border: double; border-color: #ce5860; height: ` + this.getOption("diamondSize") + `px; text-align: center; transform:rotate(45deg); width:` + this.getOption("diamondSize") + `px;"><div style="display: table-cell; height: ` + this.getOption("diamondSize") + `px; transform: rotate(-45deg); vertical-align: middle; width:` + this.getOption("diamondSize") + `px; display: inline-block; justify-content: center; flex: none;">` + valeur + `</div></div>`;
    }

    // ============================================
    // Function : ATTRIBUTS & COMPETENCES
    // ============================================
    ajouteAttribut(nomAttribut) {
        let content = "";
        content = content + this.ajouteEnteteLigne(game.i18n.localize("tor2e.stats."+nomAttribut), this.getTrad("trad_rating_TN"));
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            let donnee = perso.system.attributes[""+nomAttribut+""].value;
            content = content + `<th style='text-align:left; vertical-align: middle; white-space: nowrap; '>`;
            content = content + this.ajouteDiamant(donnee);
            content = content + `  `;
            let tnBaseValue = parseInt(game.settings.get('tor2e','tnBaseValue'));
            content = content + this.ajouteDiamant((tnBaseValue-donnee));
            content = content + `</th>`;
        }
        content = content + this.ajouteFinLigne();
        this.content_form_window = this.content_form_window +  content;
    }

    ajouteCompetence(nomCompetence) {
        let content = "";
        content = content + this.ajouteEnteteLigne("", game.i18n.localize("tor2e.commonSkills."+nomCompetence));
        for (var i = 0; i < this.listeActors.length; i++) {
            let perso = this.listeActors[i];
            let rang = perso.system.commonSkills[""+nomCompetence+""].value;
            let fav = perso.system.commonSkills[""+nomCompetence+""].favoured.value;
            content = content + `<td>`;
            content = content + this.ajouteRangs(rang, fav);
            content = content + `</td>`;        
        }
        content = content + this.ajouteFinLigne();
        this.content_form_window = this.content_form_window +  content;
    }

    // ============================================
    // Function : CASES DE RANGS
    // ============================================
    ajouteRangs(rang, fav) {
        let content = "";
        let border_bas = "";
        if(fav == true) {
            border_bas = "border-bottom: solid; border-bottom-width: thin; border-color: " + this.getOption("primaryColor") + "; ";
        }
        for(var cpt = 0; cpt < 6; cpt++) {
            if(cpt<rang) {
                content = content + `<img style="border-style: none; ` + border_bas + ` max-width: `+ this.getOption("sizeDiamondRank") + `px; max-heigth: `+ this.getOption("sizeDiamondRank") + `px;" src="modules/tor-2e-macros/images/diamond-solid.png"/>`;
            } else {
                if( this.getOption("displayEmptyRanks") == true ) {
                    content = content + `<img style="border-style: none; ` + border_bas + ` max-width: `+ this.getOption("sizeDiamondRank") + `px; max-heigth: `+ this.getOption("sizeDiamondRank") + `px;" src="modules/tor-2e-macros/images/diamond-empty.png"/>`;
                }
            }
        }
        return content;
    }    

    // ============================================
    // Function : LIST OF ACTORS
    // ============================================
    setCommunityAndActors() {
        this.listeActors = [];
        this.actorCommunity = game.tor2e.macro.utility.getCommunity();
        if ( this.actorCommunity == null || this.actorCommunity === '' ) {
            ui.notifications.notify('===== ' + this.getTrad("trad_ErrorMessageCommunityNotFound") + ' ====');
        } else {
            let cpt = 0;
            this.actorCommunity.system.members.forEach(a => {
                let actor = game.actors.get(a.id);
                if (actor.type == "character") {
                    this.listeActors[cpt] = actor;
                    cpt = cpt + 1;
                }
            })
        }
    }   
}
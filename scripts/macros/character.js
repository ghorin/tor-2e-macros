import { MacroUtil } from "../utils.js";


export class MacroCharacter {

    constructor() {
    }

    // ===================================================
    // HAINE (adversaire) : AJOUT / DIM%INUTION
    // ==================================================
    async adv_increment_hate() {
        await this.incrementeHaine(1);
    }

    async adv_decrement_hate() {
        await this.incrementeHaine(-1);
    }

    async incrementeHaine(increment) {
        let macroUtil = new MacroUtil("adv");

        let tokens = canvas.tokens.controlled;
        let token = null;
        if (tokens.length > 0) {
            token = tokens[0];
        }
        if (token == null) {
            ui.notifications.warn(macroUtil.getTraduction("select-token"));
        } else {
            let monAdv = token.actor;
            let currentHate = monAdv.system.hate.value;
            let maxHate = monAdv.system.hate.max;

            let estEpuise = false;

            let labelWeary = game.i18n.localize("tor2e.actors.stateOfHealth.weary").toLowerCase();
            for (let monEffet of monAdv.effects) {
                if (monEffet.name.toLowerCase() === labelWeary) {
                    estEpuise = true;
                }
            }

            let newHateValue = currentHate + increment;

            if (increment > 0) {
                if (currentHate === 0 && estEpuise) {
                    game.tor2e.macro.utility.setHealthStatus("weary");
                }
                if (newHateValue > maxHate) {
                    newHateValue = maxHate;
                }

            } else if (increment < 0) {
                if (newHateValue < 0) {
                    newHateValue = 0;
                }
                if (newHateValue === 0 && !estEpuise) {
                    game.tor2e.macro.utility.setHealthStatus("weary");
                }
            }

            const monUpdate = {
                system: {
                    hate: {
                        value: newHateValue
                    }
                }
            };
            const monUpdated = await monAdv.update(monUpdate);
        }
    }

    // ===================================================
    // SYNTHESE DE LA SANTE PHYSIQUE DES PJ
    //      For each character display its
    //          - Endurance, 
    //          - Load and Fatigue
    //          - Health state
    // ==================================================
    async characters_List_Health_Physical() {
        // SETTINGS
        let style_character = game.settings.get("tor-2e-macros", "health_reports_style_character");
        let vertical_bar = game.settings.get("tor-2e-macros", "health_reports_vertical_bar");
        let theme_dark_compliant = game.settings.get("tor-2e-macros", "health_reports_theme");

        // TRADUCTION
        let macroUtil = new MacroUtil("characters_physical_report");

        let trad_titre_report = macroUtil.getTraduction("trad_titre_report");

        let trad_titre_endurance = macroUtil.getTraduction("trad_titre_endurance");
        let trad_titre_load = macroUtil.getTraduction("trad_titre_load");
        let trad_titre_fatigue = macroUtil.getTraduction("trad_titre_fatigue");
        let trad_titre_state = macroUtil.getTraduction("trad_titre_state");
        let trad_unit_duration_wound = macroUtil.getTraduction("trad_unit_duration_wound");

        // COMMUNITY => LIST OF PCs
        let listeActors = [];
        let cpt = 0;
        let actorCommunity = game.tor2e.macro.utility.getCommunity();
        if (actorCommunity == null || actorCommunity === '') {
            ui.notifications.notify('===== ' + trad_ErrorMessageCommunityNotFound + ' ====');
        } else {
            actorCommunity.system.members.forEach(a => {
                let actor = game.actors.get(a.id);
                if (actor.type == "character") {
                    listeActors[cpt] = actor;
                    cpt = cpt + 1;
                }
            })
        }

        // START MESSAGE
        let message = {
            content: "<table class='tor2e-chatreports-table'>"
        };

        // TABLE HEADER
        message.content = message.content + "<thead>";
        message.content = message.content + "   <tr class='tor2e-chatreports-tr'>";
        message.content = message.content + "       <th rowspan=2 class='tor2e-chatreports-titre'><img src='modules/tor-2e-macros/icons/health_physical-icon.webp' style='max-width: 24px; max-height: 24px; border : 0;' /></th>";
        message.content = message.content + "       <th colspan=4 class='tor2e-chatreports-titre'>" + trad_titre_report + "</th>";
        message.content = message.content + "   </tr>";
        message.content = message.content + "   <tr class='tor2e-chatreports-tr'>";
        message.content = message.content + "       <th class='tor2e-chatreports-th'>" + trad_titre_endurance + "</th>";
        message.content = message.content + "       <th class='tor2e-chatreports-th'>" + trad_titre_load + "</th>";
        message.content = message.content + "       <th class='tor2e-chatreports-th'>" + trad_titre_fatigue + "</th>";
        message.content = message.content + "       <th class='tor2e-chatreports-th'>" + trad_titre_state + "</th>";
        message.content = message.content + "   </tr>";
        message.content = message.content + "</thead>";

        message.content = message.content + "<tbody>";
        message.content = message.content + "   <tr style='max-height:5px; height:5px'><td style='max-height:5px; height:5px' colspan=5></td></tr>";


        // LOOP ON ALL PLAYER CHARACTERS
        for (var i = 0; i < listeActors.length; i++) {
            let perso = listeActors[i];

            // DATA : Endurance
            let endurance = perso.system.resources.endurance.value;
            let enduranceMax = perso.system.resources.endurance.max;

            let pourcentage_endurance = Math.round(endurance / enduranceMax * 100);
            if (pourcentage_endurance > 100) {
                pourcentage_endurance = 100;
            }

            // DATA : Load & Fatigue
            let travelFatigue = perso.system.resources.travelLoad.value;
            let gearLoad = 0;
            perso.items.forEach(i => {
                if (i.type === "miscellaneous" || i.type === "weapon" || i.type === "armour") {
                    if (i.system.dropped.value === false) {
                        gearLoad = gearLoad + i.system.load.value;
                    }
                }
            });
            let totalLoad = travelFatigue + gearLoad;

            let pourcentage_charge = 0;
            if (totalLoad >= enduranceMax) {
                pourcentage_charge = 100;
            } else {
                pourcentage_charge = Math.round(totalLoad / enduranceMax * 100);
            }

            // COLORS
            let color_endurance = "";
            if (theme_dark_compliant) {
                color_endurance = "#018572";
                if (endurance < totalLoad) {
                    color_endurance = "#AC323BFF";
                }

            } else {
                color_endurance = "#526496";
                if (endurance < totalLoad) {
                    color_endurance = "darkred";
                }
            }

            // DATA : Health state
            let stateWeary = false;
            let stateWounded = false;
            let statePoisoned = false;
            let woundDuration = 0;

            perso.effects.forEach(monEffet => {
                monEffet.statuses.forEach(monTypeEffet => {
                    if (monTypeEffet === "weary") {
                        stateWeary = true;
                    } else if (monTypeEffet === "wounded") {
                        stateWounded = true;
                        woundDuration = perso.system.stateOfHealth.wounded.value;
                    } else if (monTypeEffet == "poisoned") {
                        statePoisoned = true;
                    }
                })
            });

            // TABLE CONTENT
            message.content = message.content + "   <tr class='tor2e-chatreports-tr'>";

            // CHARACTER
            let nom = perso.name.substring(0, 10);
            let posSpace = nom.indexOf(" ");
            if (posSpace != -1) {
                nom = nom.substring(0, posSpace);
            }

            if (style_character) {
                message.content = message.content + "       <th class='tor2e-chatreports-th-name'><img src='" + perso.img + "' class='tor2e-chatreports-character-img' data-tooltip='" + perso.name + "' data-tooltip-direction='RIGHT'/></th>";
            } else {
                message.content = message.content + "       <th class='tor2e-chatreports-th-name'>" + nom + "</th>";
            }


            // ENDURANCE
            message.content = message.content + "       <td class='tor2e-chatreports-td tor2e-chatreports-td-endurance'>";

            message.content = message.content + "           <div class='container tor2e-chatreports-endurance-container'>";
            message.content = message.content + "               <div class='box tor2e-chatreports-endurance-box-fond'>&nbsp;</div>";
            message.content = message.content + "               <div class='box overlay tor2e-chatreports-endurance-box-endurance' style='width: " + pourcentage_endurance + "%; background-color: " + color_endurance + ";'>&nbsp;</div>";

            if (vertical_bar) {
                message.content = message.content + "";
                message.content = message.content + "               <div class='box overlay tor2e-chatreports-endurance-box-load' style='width: " + pourcentage_charge + "%; '>&nbsp;</div>";
                message.content = message.content + "";
            }

            message.content = message.content + "               <div class='box overlay tor2e-chatreports-endurance-box-values'>";
            message.content = message.content + "                   <span class='tor2e-chatreports-endurance-values'>&nbsp;" + endurance + "/" + enduranceMax + "</span>";
            message.content = message.content + "               </div>";
            message.content = message.content + "           </div";
            message.content = message.content + "       </td>";


            // LOAD
            message.content = message.content + "       <td class='tor2e-chatreports-td tor2e-chatreports-td-load' style='color: " + color_endurance + ";'>" + totalLoad + "</td>";

            // FATIGUE
            message.content = message.content + "       <td class='tor2e-chatreports-td tor2e-chatreports-td-fatigue'>" + travelFatigue + "</td>";

            // HEALTH STATE
            message.content = message.content + "       <td class='tor2e-chatreports-td tor2e-chatreports-td-state'>";

            if (stateWeary) {
                message.content = message.content + "       <img src='systems/tor2e/assets/images/icons/effects/weary.svg' class='tor2e-chatreports-state-img tor2e-chatreports-state-img-weary' />&nbsp;";
            }
            if (stateWounded) {
                message.content = message.content + "       <img data-tooltip='" + woundDuration + " " + trad_unit_duration_wound + "' data-tooltip-direction='LEFT' src='systems/tor2e/assets/images/icons/effects/wounded.svg' class='tor2e-chatreports-state-img tor2e-chatreports-state-img-wounded' />&nbsp;";
            }

            if (statePoisoned) {
                message.content = message.content + "       <img src='systems/tor2e/assets/images/icons/effects/poisoned.svg' class='tor2e-chatreports-state-img tor2e-chatreports-state-img-poisoned'/>";
            }


            message.content = message.content + "       </td>";
            message.content = message.content + "   </tr>";
            message.content = message.content + "";
            message.content = message.content + "";

        }



        // END MESSAGE 
        message.content = message.content + "</tbody>";
        message.content = message.content + "</table>";

        // POST THE MESSAGE IN THE CHAT
        ChatMessage.create(message);
    }

    // ==================================================
    // SYNTHESE DE LA SANTE PSYCHOLOGIQUE DES PJ
    // ==================================================
    async characters_List_Health_Moral() {
        // SETTINGS
        let style_character = game.settings.get("tor-2e-macros", "health_reports_style_character");
        let vertical_bar = game.settings.get("tor-2e-macros", "health_reports_vertical_bar");
        let theme_dark_compliant = game.settings.get("tor-2e-macros", "health_reports_theme");

        // TRADUCTION
        let macroUtil = new MacroUtil("characters_moral_report");

        let trad_titre_report = macroUtil.getTraduction("trad_titre_report");

        let trad_titre_hope = macroUtil.getTraduction("trad_titre_hope");
        let trad_titre_shadow = macroUtil.getTraduction("trad_titre_shadow");
        let trad_titre_scars = macroUtil.getTraduction("trad_titre_scars");
        let trad_titre_state = macroUtil.getTraduction("trad_titre_state");
        let trad_state_miserable = macroUtil.getTraduction("trad_state_miserable");

        let trad_titre_flaws = macroUtil.getTraduction("trad_flaws");

        // COMMUNITY => LIST OF PCs
        let listeActors = [];
        let cpt = 0;
        let actorCommunity = game.tor2e.macro.utility.getCommunity();
        if (actorCommunity == null || actorCommunity === '') {
            ui.notifications.notify('===== ' + trad_ErrorMessageCommunityNotFound + ' ====');
        } else {
            actorCommunity.system.members.forEach(a => {
                let actor = game.actors.get(a.id);
                if (actor.type == "character") {
                    listeActors[cpt] = actor;
                    cpt = cpt + 1;
                }
            })
        }

        // START MESSAGE
        let message = {
            content: "<table class='tor2e-chatreports-table'>"
        };

        // TABLE HEADER
        message.content = message.content + "<thead>";
        message.content = message.content + "   <tr class='tor2e-chatreports-tr'>";
        message.content = message.content + "       <th rowspan=2 class='tor2e-chatreports-titre'><img src='modules/tor-2e-macros/icons/health_moral-icon.webp' style='max-width: 24px; max-height: 24px; border : 0;' /></th>";
        message.content = message.content + "       <th colspan=5 class='tor2e-chatreports-titre'>" + trad_titre_report + "</th>";
        message.content = message.content + "   </tr>";
        message.content = message.content + "   <tr class='tor2e-chatreports-tr'>";
        message.content = message.content + "       <th class='tor2e-chatreports-th'>" + trad_titre_hope + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>";
        message.content = message.content + "       <th class='tor2e-chatreports-th'>" + trad_titre_shadow + "</th>";
        message.content = message.content + "       <th class='tor2e-chatreports-th'>" + trad_titre_scars + "</th>";
        message.content = message.content + "       <th class='tor2e-chatreports-th'>" + trad_titre_flaws + "</th>";
        message.content = message.content + "       <th class='tor2e-chatreports-th'>" + trad_titre_state + "</th>";
        message.content = message.content + "   </tr>";
        message.content = message.content + "</thead>";

        message.content = message.content + "<tbody>";
        message.content = message.content + "   <tr style='max-height:5px; height:5px'><td style='max-height:5px; height:5px' colspan=5></td></tr>";


        // LOOP ON ALL PLAYER CHARACTERS
        for (var i = 0; i < listeActors.length; i++) {
            let perso = listeActors[i];

            // DATA : Hope
            let hope = perso.system.resources.hope.value;
            let hopeMax = perso.system.resources.hope.max;

            let pourcentage_hope = Math.round(hope / hopeMax * 100);
            if (pourcentage_hope > 100) {
                pourcentage_hope = 100;
            }

            // DATA : Shadow
            let shadow = perso.system.resources.shadow.temporary.value + perso.system.resources.shadow.shadowScars.value;
            let scars = perso.system.resources.shadow.shadowScars.value;

            let pourcentage_shadow = 0;
            if (shadow >= hopeMax) {
                pourcentage_shadow = 100;
            } else {
                pourcentage_shadow = Math.round(shadow / hopeMax * 100);
            }

            // DATA : Flows
            let nbFlaws = 0;
            //let listFlaws = "";
            perso.items.forEach(monItem => {
                if (monItem.type === "trait") {
                    if (monItem.system.group.value === "flaw") {
                        nbFlaws++;
                        //listFlaws = listFlaws + "" + monItem.name + "<br>";
                    }
                }
            });
            //listFlaws = listFlaws + "";


            // COLORS
            let color_hope = "";
            if (theme_dark_compliant) {
                color_hope = "#018572";
                if (hope < shadow) {
                    color_hope = "#AC323BFF";
                }

            } else {
                color_hope = "#526496";
                if (hope < shadow) {
                    color_hope = "darkred";
                }
            }

            // DATA : Health state
            let stateMiserable = false;
            perso.effects.forEach(monEffet => {
                monEffet.statuses.forEach(monTypeEffet => {
                    if (monTypeEffet === "miserable") {
                        stateMiserable = true;
                    }
                })
            });

            // TABLE CONTENT
            message.content = message.content + "   <tr class='tor2e-chatreports-tr'>";

            // CHARACTER
            let nom = perso.name.substring(0, 10);
            let posSpace = nom.indexOf(" ");
            if (posSpace != -1) {
                nom = nom.substring(0, posSpace);
            }
            if (style_character) {
                message.content = message.content + "       <th class='tor2e-chatreports-th-name'><img src='" + perso.img + "' class='tor2e-chatreports-character-img' data-tooltip='" + perso.name + "' data-tooltip-direction='RIGHT'/></th>";
            } else {
                message.content = message.content + "       <th class='tor2e-chatreports-th-name'>" + nom + "</th>";
            }

            // HOPE
            message.content = message.content + "       <td class='tor2e-chatreports-td tor2e-chatreports-td-endurance'>";

            message.content = message.content + "           <div class='container tor2e-chatreports-endurance-container'>";
            message.content = message.content + "               <div class='box tor2e-chatreports-endurance-box-fond'>&nbsp;</div>";
            message.content = message.content + "               <div class='box overlay tor2e-chatreports-endurance-box-endurance' style='width: " + pourcentage_hope + "%; background-color: " + color_hope + ";'>&nbsp;</div>";

            if (vertical_bar) {
                message.content = message.content + "";
                message.content = message.content + "               <div class='box overlay tor2e-chatreports-endurance-box-load' style='width: " + pourcentage_shadow + "%; '>&nbsp;</div>";
                message.content = message.content + "";
            }

            message.content = message.content + "               <div class='box overlay tor2e-chatreports-endurance-box-values'>";
            message.content = message.content + "                   <span class='tor2e-chatreports-endurance-values'>&nbsp;" + hope + "/" + hopeMax + "</span>";
            message.content = message.content + "               </div>";
            message.content = message.content + "           </div";
            message.content = message.content + "       </td>";


            // SHADOW
            message.content = message.content + "       <td class='tor2e-chatreports-td tor2e-chatreports-td-load' style='color: " + color_hope + ";'>" + shadow + "</td>";

            // SCARS
            message.content = message.content + "       <td class='tor2e-chatreports-td tor2e-chatreports-td-load'>" + scars + "</td>";

            // FLAWS
            //message.content = message.content + "       <td class='tor2e-chatreports-td tor2e-chatreports-td-load'><span data-tooltip='" + listFlaws + "' data-tooltip-direction='LEFT'>" + nbFlaws + "</span></td>";
            message.content = message.content + "       <td class='tor2e-chatreports-td tor2e-chatreports-td-load'>" + nbFlaws + "</td>";

            // HEALTH STATE
            message.content = message.content + "       <td class='tor2e-chatreports-td tor2e-chatreports-td-state-moral'>";

            if (stateMiserable) {
                message.content = message.content + "       <img src='systems/tor2e/assets/images/icons/effects/miserable.svg' data-tooltip='" + trad_state_miserable + "' class='tor2e-chatreports-state-img tor2e-chatreports-state-img-miserable' />&nbsp;";
            }


            message.content = message.content + "       </td>";
            message.content = message.content + "   </tr>";
            message.content = message.content + "";
            message.content = message.content + "";

        }



        // END MESSAGE 
        message.content = message.content + "</tbody>";
        message.content = message.content + "</table>";

        // POST THE MESSAGE IN THE CHAT
        ChatMessage.create(message);
    }



    // ==================================================
    // SYNTHESE DE LA VAILLANCE & SAGESSE DES PJ
    // ==================================================
    async characters_List_Valour_Wisdom() {
        // ==================================================
        // - score de chaque PJ
        // - score de Vaillance le plus élevé (et qui)
        // - score de Sagesse le plus élevé (et qui)
        // ==================================================

        // Get traduction of terms
        let macroUtil = new MacroUtil("characters_List_Valour_Wisdom");

        let trad_DialogTitre = macroUtil.getTraduction("titre");
        let trad_DialogQuestion = macroUtil.getTraduction("question");

        let trad_TableVaillanceSagesse = macroUtil.getTraduction("vaillance_sagesse");
        let trad_TableVaillance = macroUtil.getTraduction("vaillance");
        let trad_TableSagesse = macroUtil.getTraduction("sagesse");

        // SETTINGS
        let style_character = game.settings.get("tor-2e-macros", "health_reports_style_character");

        // DATA
        var cpt = 0;
        let listeActors = [];
        let vaillanceMaxValue = 0;
        let sagesseMaxValue = 0;



        let actorCommunity = game.tor2e.macro.utility.getCommunity();
        if (actorCommunity == null || actorCommunity === '') {
            ui.notifications.notify('===== ' + trad_ErrorMessageCommunityNotFound + ' ====');
        } else {
            actorCommunity.system.members.forEach(a => {
                let actor = game.actors.get(a.id);
                if (actor.type == "character") {
                    listeActors[cpt] = actor;
                    cpt = cpt + 1;
                }
            })
        }

        // LOOP ON ALL ACTORS TO DETERMINE THE MAX VALOUR & WISDOM
        for (var i = 0; i < listeActors.length; i++) {
            let perso = listeActors[i];

            if (perso.system.stature.valour.value > vaillanceMaxValue) {
                vaillanceMaxValue = perso.system.stature.valour.value;
            }
            if (perso.system.stature.wisdom.value > sagesseMaxValue) {
                sagesseMaxValue = perso.system.stature.wisdom.value;
            }
        }

        // START MESSAGE
        let message = {
            content: "<table class='tor2e-chatreports-table'>",
            whisper: game.users.filter(u => u.isGM).map(u => u.id)
        };

        // TABLE HEADER
        message.content = message.content + "<thead>";
        message.content = message.content + "   <tr class='tor2e-chatreports-tr'>";
        message.content = message.content + "       <th rowspan=2 class='tor2e-chatreports-titre'><img src='modules/tor-2e-macros/icons/valour_wisdom.webp' style='max-width: 36px; max-height: 36px; border : 0;' /></th>";
        message.content = message.content + "       <th colspan=2 class='tor2e-chatreports-titre'>" + trad_TableVaillanceSagesse + "</th>";
        message.content = message.content + "   </tr>";
        message.content = message.content + "   <tr class='tor2e-chatreports-tr'>";
        message.content = message.content + "       <th class='tor2e-chatreports-th'>" + trad_TableVaillance + "</th>";
        message.content = message.content + "       <th class='tor2e-chatreports-th'>" + trad_TableSagesse + "</th>";
        message.content = message.content + "   </tr>";
        message.content = message.content + "</thead>";

        message.content = message.content + "<tbody>";
        message.content = message.content + "   <tr style='max-height:5px; height:5px'><td style='max-height:5px; height:5px' colspan=5></td></tr>";


        // LOOP ON ALL ACTORS
        for (var i = 0; i < listeActors.length; i++) {
            let perso = listeActors[i];

            // TABLE CONTENT
            message.content = message.content + "   <tr class='tor2e-chatreports-tr'>";

            // CHARACTER
            let nom = perso.name.substring(0, 10);
            let posSpace = nom.indexOf(" ");
            if (posSpace != -1) {
                nom = nom.substring(0, posSpace);
            }
            if (style_character) {
                message.content = message.content + "       <th class='tor2e-chatreports-th-name'><img src='" + perso.img + "' class='tor2e-chatreports-character-img' data-tooltip='" + perso.name + "' data-tooltip-direction='RIGHT'/></th>";
            } else {
                message.content = message.content + "       <th class='tor2e-chatreports-th-name'>" + nom + "</th>";
            }

            // VALOUR
            if (perso.system.stature.valour.value == vaillanceMaxValue) {
                message.content = message.content + "       <td class='tor2e-chatreports-td tor2e-chatreports-td-load' style='color:darkred; font-weight: bold'>" + perso.system.stature.valour.value + "</td>";
            } else {
                message.content = message.content + "       <td class='tor2e-chatreports-td tor2e-chatreports-td-load'>" + perso.system.stature.valour.value + "</td>";
            }

            // WISDOM
            if (perso.system.stature.wisdom.value == sagesseMaxValue) {
                message.content = message.content + "       <td class='tor2e-chatreports-td tor2e-chatreports-td-load' style='color:darkred; font-weight: bold'>" + perso.system.stature.wisdom.value + "</td>";
            } else {
                message.content = message.content + "       <td class='tor2e-chatreports-td tor2e-chatreports-td-load'>" + perso.system.stature.wisdom.value + "</td>";
            }

            message.content = message.content + "       </td>";
            message.content = message.content + "   </tr>";
            message.content = message.content + "";
            message.content = message.content + "";

        }

        // END MESSAGE 
        message.content = message.content + "</tbody>";
        message.content = message.content + "</table>";

        // POST THE MESSAGE IN THE CHAT
        ChatMessage.create(message);
    }


    // ==================================================
    // SYNTHESE EXPERIENCE DES PJ
    // ==================================================
    async characters_List_Experience() {
        // ==================================================
        // Pour chaque PJ : afficher :
        // - points de Progression non utilisés
        // - points d'Aventure non utilisés
        // ==================================================

        // Get traduction of terms
        let macroUtil = new MacroUtil("characters_List_Experience");

        let trad_TableExperience = macroUtil.getTraduction("experience");
        let trad_TableProgression = macroUtil.getTraduction("progression");
        let trad_TableAventure = macroUtil.getTraduction("aventure");

        // SETTINGS
        let style_character = game.settings.get("tor-2e-macros", "health_reports_style_character");

        // DATA
        var cpt = 0;
        let listeActors = [];

        let actorCommunity = game.tor2e.macro.utility.getCommunity();
        if (actorCommunity == null || actorCommunity === '') {
            ui.notifications.notify('===== ' + trad_ErrorMessageCommunityNotFound + ' ====');
        } else {
            actorCommunity.system.members.forEach(a => {
                let actor = game.actors.get(a.id);
                if (actor.type == "character") {
                    listeActors[cpt] = actor;
                    cpt = cpt + 1;
                }
            })
        }

        // START MESSAGE
        let message = {
            content: "<table class='tor2e-chatreports-table'>",
            whisper: game.users.filter(u => u.isGM).map(u => u.id)
        };

        // TABLE HEADER
        message.content = message.content + "<thead>";
        message.content = message.content + "   <tr class='tor2e-chatreports-tr'>";
        message.content = message.content + "       <th rowspan=2 class='tor2e-chatreports-titre'><img src='modules/tor-2e-macros/icons/experience.webp' style='max-width: 36px; max-height: 36px; border : 0;' /></th>";
        message.content = message.content + "       <th colspan=2 class='tor2e-chatreports-titre'>" + trad_TableExperience + "</th>";
        message.content = message.content + "   </tr>";
        message.content = message.content + "   <tr class='tor2e-chatreports-tr'>";
        message.content = message.content + "       <th class='tor2e-chatreports-th'>" + trad_TableProgression + "</th>";
        message.content = message.content + "       <th class='tor2e-chatreports-th'>" + trad_TableAventure + "</th>";
        message.content = message.content + "   </tr>";
        message.content = message.content + "</thead>";

        message.content = message.content + "<tbody>";
        message.content = message.content + "   <tr style='max-height:5px; height:5px'><td style='max-height:5px; height:5px' colspan=5></td></tr>";


        // LOOP ON ALL ACTORS
        for (var i = 0; i < listeActors.length; i++) {
            let perso = listeActors[i];

            // TABLE CONTENT
            message.content = message.content + "   <tr class='tor2e-chatreports-tr'>";

            // CHARACTER
            let nom = perso.name.substring(0, 10);
            let posSpace = nom.indexOf(" ");
            if (posSpace != -1) {
                nom = nom.substring(0, posSpace);
            }
            if (style_character) {
                message.content = message.content + "       <th class='tor2e-chatreports-th-name'><img src='" + perso.img + "' class='tor2e-chatreports-character-img' data-tooltip='" + perso.name + "' data-tooltip-direction='RIGHT'/></th>";
            } else {
                message.content = message.content + "       <th class='tor2e-chatreports-th-name'>" + nom + "</th>";
            }

            // PTS DE PROGRESSION
            message.content = message.content + "       <td class='tor2e-chatreports-td tor2e-chatreports-td-load'>" + perso.system.skillPoints.value + "</td>";

            // PTS D'AVENTURE
            message.content = message.content + "       <td class='tor2e-chatreports-td tor2e-chatreports-td-load'>" + perso.system.adventurePoints.value + "</td>";

            message.content = message.content + "       </td>";
            message.content = message.content + "   </tr>";
            message.content = message.content + "";
            message.content = message.content + "";

        }

        // END MESSAGE 
        message.content = message.content + "</tbody>";
        message.content = message.content + "</table>";

        // POST THE MESSAGE IN THE CHAT
        ChatMessage.create(message);
    }


    // ==================================================
    // REMET SANTE PHYSIQUE MAX DU TOKEN SELECTIONNE
    // ==================================================
    async characters_Set_EtatPhysiqueMax() {
        // ==================================================
        // PLAYER CHARACTERS
        //      - Endurance au max
        //      - Fatigue de voyage à zéro
        //      - Etat Epuisé mis à Non
        //      - Etat Blessé mis à Non
        //      - Etat Empoisonné mis à Non
        //
        // ADVERSARIES
        //      - Endurance au max
        //      - Might au max
        //      - Etat Epuisé mis à Non
        //      - Etat Blessé mis à Non
        //      - Etat Empoisonné mis à Non
        // ==================================================
        // Get traduction of terms
        let macroUtil = new MacroUtil("characters_Set_EtatPhysiqueMax");

        let trad_selectToken = macroUtil.getTraduction("select_token_character");
        let trad_actionTermineePj = macroUtil.getTraduction("action_terminee_pj");
        let trad_actionTermineeAdv = macroUtil.getTraduction("action_terminee_adv");

        let idEtat_Epuise = "weary";
        let idEtat_Blesse = "wounded";
        let idEtat_TempEpuise = "temporary-weary";
        let idEtat_TempBlesse = "convalescent";
        let idEtat_Empoisonne = "poisoned";

        // ETAT DE SANTE
        let etatEpuise = false;
        let etatBlessure = false;
        let etatEmpoisonne = false;

        let messagefin = "";

        let tokens = canvas.tokens.controlled;
        let token = null;
        if (tokens.length > 0) {
            token = tokens[0];
        }
        if (token == null) {
            ui.notifications.warn(trad_selectToken);
        } else {
            let monPersonnage = token.actor;

            if (monPersonnage.type === "character" || monPersonnage.type === "adversary") {
                if (monPersonnage.type === "character") {
                    let maxEndurance = monPersonnage.system.resources.endurance.max;

                    const monUpdate = {
                        system: {
                            resources: {
                                endurance: {
                                    value: maxEndurance
                                },
                                travelFatigue: {
                                    value: 0
                                },
                                travelLoad: {
                                    value: 0
                                }
                            },
                            stateOfHealth: {
                                poisoned: {
                                    value: 0
                                },
                                weary: {
                                    value: 0
                                },
                                wounded: {
                                    value: 0
                                }
                            }
                        }
                    };
                    const monUpdated = await monPersonnage.update(monUpdate);
                    messagefin = monPersonnage.name + " : " + trad_actionTermineePj;

                } else if (monPersonnage.type === "adversary") {
                    let maxEndurance = monPersonnage.system.endurance.max;
                    let maxPuissance = monPersonnage.system.might.max;

                    const monUpdate = {
                        system: {
                            endurance: {
                                value: maxEndurance
                            },
                            might: {
                                value: maxPuissance
                            },
                            stateOfHealth: {
                                poisoned: {
                                    value: 0
                                },
                                weary: {
                                    value: 0
                                },
                                wounded: {
                                    value: 0
                                }
                            }
                        }
                    };

                    const monUpdated = await monPersonnage.update(monUpdate);

                    messagefin = monPersonnage.name + " : " + trad_actionTermineeAdv;
                }

                monPersonnage.effects.forEach(monEffet => {
                    monEffet.statuses.forEach(monTypeEffet => {
                        if (monTypeEffet === idEtat_Epuise) {
                            etatEpuise = true;
                        } else if (monTypeEffet === idEtat_TempEpuise) {
                            etatEpuise = true;
                        } else if (monTypeEffet === idEtat_Blesse) {
                            etatBlessure = true;
                        } else if (monTypeEffet == idEtat_TempBlesse) {
                            etatBlessure = true;
                        } else if (monTypeEffet == idEtat_Empoisonne) {
                            etatEmpoisonne = true;
                        }
                    })
                });

                if (etatEpuise) {
                    game.tor2e.macro.utility.setHealthStatus("weary");
                }
                if (etatBlessure) {
                    game.tor2e.macro.utility.setHealthStatus("wounded");
                }
                if (etatEmpoisonne) {
                    game.tor2e.macro.utility.setHealthStatus("poisoned");
                }

                ui.notifications.info(messagefin);
            }
        }
    }

    // ==================================================
    // AJOUT DE LA PROGRESSION EN FIN DE SEANCE
    // ==================================================
    async characters_Add_SessionProgression() {
        // Get traduction of terms
        let macroUtil = new MacroUtil("characters_Add_SessionExperience");

        let trad_DialogTitre = macroUtil.getTraduction("titre");
        let trad_DialogPtsAventure = macroUtil.getTraduction("trad_DialogPtsAventure");
        let trad_DialogPtsProgression = macroUtil.getTraduction("trad_DialogPtsProgression");
        let trad_DialogPtsTresor = macroUtil.getTraduction("trad_DialogPtsTresor");
        let trad_DialogDestinataires = macroUtil.getTraduction("trad_DialogDestinataires");
        let trad_DialogTouslesjoueurs = macroUtil.getTraduction("trad_DialogTouslesjoueurs");
        let trad_DialogJoueursCommunity = macroUtil.getTraduction("trad_DialogJoueursCommunity");
        let trad_DialogJoueursActifsCommunity = macroUtil.getTraduction("trad_DialogJoueursActifsCommunity");
        let trad_DialogJoueurspresents = macroUtil.getTraduction("trad_DialogJoueurspresents");
        let trad_DialogContinuer = macroUtil.getTraduction("trad_DialogContinuer");
        let trad_DialogTouches = macroUtil.getTraduction("trad_DialogTouches");

        let trad_Experience = macroUtil.getTraduction("trad_Experience");
        let trad_Personnages = macroUtil.getTraduction("trad_Personnages");
        let trad_PtsAventure = macroUtil.getTraduction("trad_PtsAventure");
        let trad_PtsProgression = macroUtil.getTraduction("trad_PtsProgression");
        let trad_PtsTresor = macroUtil.getTraduction("trad_PtsTresor");

        let macroDialogTrad = new MacroUtil("dialogs");
        let trad_annuler = macroDialogTrad.getTraduction("annuler");
        let trad_valider = macroDialogTrad.getTraduction("valider");


        // Déterminer la situation de la partie
        let actorCommunity = game.tor2e.macro.utility.getCommunity();
        let existeActveCommunity = false;
        if (actorCommunity != null) {
            existeActveCommunity = true;
        }
        let nbCommunities = 0;
        game.actors.forEach(c => {
            if (c.type === "community") {
                nbCommunities++;
            }
        });


        // Construire les listes de personnages selon les types de choix
        let cpt = 0;
        // ==> Tous les PJ
        let listAllPc = [];
        cpt = 0;
        game.actors.forEach(c => {
            if (c.type === "character") {
                listAllPc[cpt] = c;
                cpt++;
            }
        });
        listAllPc.sort((a, b) => {
            const nameA = a.name.toUpperCase(); // ignore upper and lowercase
            const nameB = b.name.toUpperCase(); // ignore upper and lowercase
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            // names must be equal
            return 0;
        });

        // ==> Tous les PJ actifs (joueur connecté) 
        let listAllActivePc = [];
        cpt = 0;
        game.users.forEach(u => {
            if (u.active == true && u.character != null) {
                let perso = u.character;
                if (perso != null) {
                    if (perso.type === "character") {
                        listAllActivePc[cpt] = perso;
                        cpt++;
                    }
                }
            }
        });
        listAllActivePc.sort((a, b) => {
            const nameA = a.name.toUpperCase(); // ignore upper and lowercase
            const nameB = b.name.toUpperCase(); // ignore upper and lowercase
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            // names must be equal
            return 0;
        });
        let nbLignes = cpt - 1 + 4;


        // ==> Les PJ de la Communauté active
        let listActiveCommunityPC = [];
        cpt = 0;
        if (existeActveCommunity) {
            actorCommunity.system.members.forEach(a => {
                let actor = game.actors.get(a.id);
                if (actor.type == "character") {
                    listActiveCommunityPC[cpt] = actor;
                    cpt++;
                }
            })
        }
        listActiveCommunityPC.sort((a, b) => {
            const nameA = a.name.toUpperCase(); // ignore upper and lowercase
            const nameB = b.name.toUpperCase(); // ignore upper and lowercase
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            // names must be equal
            return 0;
        });

        // ==> Les PJ actifs de la Communauté active
        let listActiveCommunityActivePC = [];
        cpt = 0;
        if (existeActveCommunity) {
            actorCommunity.system.members.forEach(a => {
                let perso = game.actors.get(a.id);
                if (perso.type === "character") {
                    game.users.forEach(u => {
                        if (u.active == true && u.character != null) {
                            if (u.character.id === a.id) {
                                listActiveCommunityActivePC[cpt] = perso;
                                cpt++;
                            }
                        }
                    });
                }
            })
        }
        listActiveCommunityActivePC.sort((a, b) => {
            const nameA = a.name.toUpperCase(); // ignore upper and lowercase
            const nameB = b.name.toUpperCase(); // ignore upper and lowercase
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            // names must be equal
            return 0;
        });


        let formHtml = "<P><table><tr><th colspan='4'><h2>" + trad_DialogTitre + "</h2></th></tr>";
        formHtml = formHtml + "<tr>";
        formHtml = formHtml + " <th></th>";
        formHtml = formHtml + " <th nowrap style='text-align: center;'><label><u>" + trad_DialogPtsProgression + "</u></label></th>";
        formHtml = formHtml + " <th nowrap style='text-align: center;'><label><u>" + trad_DialogPtsAventure + "</u></label></th>";
        formHtml = formHtml + " <th nowrap style='text-align: center;'><label><u>" + trad_DialogPtsTresor + "</u></label></th>";
        formHtml = formHtml + "</tr>";
        formHtml = formHtml + "<tr>";
        formHtml = formHtml + " <td></td>";
        formHtml = formHtml + " <td nowrap style='text-align: center;'><input name='ptsProgression' style='width: 50px' value='3'></td>";
        formHtml = formHtml + " <td nowrap style='text-align: center;'><input name='ptsAventure' style='width: 50px' value='3'/></td>";
        formHtml = formHtml + " <td nowrap style='text-align: center;'><input name='ptsTresor' style='width: 50px' value='0'/></td>";
        formHtml = formHtml + "</tr>";
        formHtml = formHtml + "<tr><th nowrap><label>" + trad_DialogDestinataires + " : </label></th><td colspan='3'><select id='destinataires' multiple rows=15 style='width: 500px; height: " + (100 + (nbLignes * 30)) + "px;'>";
        formHtml = formHtml + "<option value='all_pc'>" + trad_DialogTouslesjoueurs + "</option>";
        formHtml = formHtml + "<option value='all_active_pc'>" + trad_DialogJoueurspresents + "</option>";
        if (existeActveCommunity) {
            formHtml = formHtml + "<option value='all_pc_of_active_community'>" + trad_DialogJoueursCommunity + "</option>";
            formHtml = formHtml + "<option value='all_active_pc_of_active_community' selected>" + trad_DialogJoueursActifsCommunity + "</option>";
        }
        game.users.forEach(t => {
            if (t.character != null) {
                let perso = t.character;
                if (perso != null) {
                    if (perso.type == "character") {
                        formHtml = formHtml + "<option value='" + t.name + "'>" + perso.name + " (" + t.name + ")</option>";
                    }
                }
            }
        });

        formHtml = formHtml + "</select></td></tr><tr><td></td><td colspan='3'>" + trad_DialogTouches + "</td></tr>";
        formHtml = formHtml + "<tr style='text-align:left;'>";
        formHtml = formHtml + "<th style='width:20%;'>" + trad_DialogTouslesjoueurs + "</th>";
        formHtml = formHtml + "<th style='width:20%;'>" + trad_DialogJoueurspresents + "</th>";
        if (existeActveCommunity) {
            formHtml = formHtml + "<th style='width:30%;'>" + trad_DialogJoueursCommunity + "</th>";
            formHtml = formHtml + "<th style='width:30%;'>" + trad_DialogJoueursActifsCommunity + "</th>";
        }
        formHtml = formHtml + "</tr><tr>";

        // Tous les PJ
        formHtml = formHtml + "<td style='vertical-align: top;'><ul>";
        for (var i = 0; i < listAllPc.length; i++) {
            formHtml = formHtml + "<li>" + listAllPc[i].name + "</li>";
        }
        formHtml = formHtml + "</ul></td>";

        // Tous les PJ actifs
        formHtml = formHtml + "<td style='vertical-align: top;'><ul>";
        for (var i = 0; i < listAllActivePc.length; i++) {
            formHtml = formHtml + "<li>" + listAllActivePc[i].name + "</li>";
        }
        formHtml = formHtml + "</ul>";

        if (existeActveCommunity) {
            // Tous les PJ de la Communauté active
            formHtml = formHtml + "<td style='vertical-align: top;'><ul>";
            for (var i = 0; i < listActiveCommunityPC.length; i++) {
                formHtml = formHtml + "<li>" + listActiveCommunityPC[i].name + "</li>";
            }
            formHtml = formHtml + "</ul></td>";

            // Tous les PJ actifs de la Communauté active
            formHtml = formHtml + "<td style='vertical-align: top;'><ul>";
            for (var i = 0; i < listActiveCommunityActivePC.length; i++) {
                formHtml = formHtml + "<li>" + listActiveCommunityActivePC[i].name + "</li>";
            }
            formHtml = formHtml + "</ul></td>";
        }
        formHtml = formHtml + "";
        formHtml = formHtml + "";
        formHtml = formHtml + "</table>";


        let hauteurFenetre = (nbLignes * 22) + 650;
        new Dialog({
            title: trad_DialogTitre,
            content: formHtml,
            buttons: {
                button_cancel: {
                    label: trad_annuler,
                    callback: () => { },
                    icon: `<i class="fas fa-times"></i>`
                },
                button_save: {
                    label: trad_valider,
                    callback: html => {
                        let ptsAventure = html.find('[name=ptsAventure]')[0]?.value || null;
                        let ptsProgression = html.find('[name=ptsProgression]')[0]?.value || null;
                        let ptsTresor = html.find('[name=ptsTresor]')[0]?.value || null;
                        if (ptsAventure != null && ptsProgression != null && ptsTresor != null) {
                            let cible = html.find('select#destinataires');
                            var options = cible[0].selectedOptions;
                            var values = Array.from(options).map(({ value }) => value);


                            let monMessage = "<table><tr><th>=== " + trad_Experience + " ===</th></tr>";
                            monMessage = monMessage + "<tr><td>+" + ptsAventure + " " + trad_PtsAventure + "</td></tr>";
                            monMessage = monMessage + "<tr></td><td>+" + ptsProgression + " " + trad_PtsProgression + "</td></tr>";
                            monMessage = monMessage + "<tr></td><td>+" + ptsTresor + " " + trad_PtsTresor + "</td></tr>";
                            monMessage = monMessage + "<tr><th>=== " + trad_Personnages + " ===</th></tr>";

                            if (values == "all_pc") {
                                for (var i = 0; i < listAllPc.length; i++) {
                                    this.ajouteExperience(listAllPc[i], ptsAventure, ptsProgression, ptsTresor);
                                    monMessage = monMessage + "<tr><td>" + listAllPc[i].name + "</td></tr>";
                                }
                            } else if (values == "all_active_pc") {
                                for (var i = 0; i < listAllActivePc.length; i++) {
                                    this.ajouteExperience(listAllActivePc[i], ptsAventure, ptsProgression, ptsTresor);
                                    monMessage = monMessage + "<tr><td>" + listAllActivePc[i].name + "</td></tr>";
                                }
                            } else if (values == "all_pc_of_active_community") {
                                for (var i = 0; i < listActiveCommunityPC.length; i++) {
                                    this.ajouteExperience(listActiveCommunityPC[i], ptsAventure, ptsProgression, ptsTresor);
                                    monMessage = monMessage + "<tr><td>" + listActiveCommunityPC[i].name + "</td></tr>";
                                }
                            } else if (values == "all_active_pc_of_active_community") {
                                for (var i = 0; i < listActiveCommunityActivePC.length; i++) {
                                    this.ajouteExperience(listActiveCommunityActivePC[i], ptsAventure, ptsProgression, ptsTresor);
                                    monMessage = monMessage + "<tr><td>" + listActiveCommunityActivePC[i].name + "</td></tr>";
                                }
                            } else {
                                // Specific PCs
                                for (var i = 0; i < values.length; i++) {
                                    game.users.forEach(t => {
                                        if (t.name === values[i]) {
                                            if (t.character != null) {
                                                let perso = t.character;
                                                if (perso != null) {
                                                    if (perso.type == "character") {
                                                        this.ajouteExperience(perso, ptsAventure, ptsProgression, ptsTresor);
                                                        monMessage = monMessage + "<tr><td>" + perso.name + "</td></tr>";
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }

                            }
                            monMessage = monMessage + "</tr></table>"
                            let message = {
                                content: monMessage
                            };
                            ChatMessage.create(message);
                        }
                    }
                }
            },
            default: 'button_save',
            close: () => { }
        }, { width: 800, height: hauteurFenetre }).render(true);
    }

    async ajouteExperience(perso, ptsAventure, ptsProgression, ptsTresor) {
        if (perso.type == "character") {
            let curPtsProgression = perso.system.skillPoints.value;
            let curPtsProgressionTotal = perso.system.skillPoints.total.value;
            let curPtsAventure = perso.system.adventurePoints.value;
            let curPtsAventureTotal = perso.system.adventurePoints.total.value;
            let curPtsTresor = perso.system.treasure.value;


            let newPtsAventure = parseInt(curPtsAventure, 10) + parseInt(ptsAventure, 10);
            let newPtsAventureTotal = parseInt(curPtsAventureTotal, 10) + parseInt(ptsAventure, 10);
            let newPtsProgression = parseInt(curPtsProgression, 10) + parseInt(ptsProgression, 10);
            let newPtsProgressionTotal = parseInt(curPtsProgressionTotal, 10) + parseInt(ptsProgression, 10);
            let newPtsTresor = parseInt(curPtsTresor, 10) + parseInt(ptsTresor, 10);

            const monUpdate = {
                system: {
                    skillPoints: {
                        value: newPtsProgression,
                        total: {
                            value: newPtsProgressionTotal
                        }
                    },
                    adventurePoints: {
                        value: newPtsAventure,
                        total: {
                            value: newPtsAventureTotal
                        }
                    },
                    treasure: {
                        value: newPtsTresor
                    }
                }
            };
            perso.update(monUpdate);
        }
    }

    async characters_reduceWoundDuration() {
        // TRADUCTION
        let macroUtil = new MacroUtil("characters_wounds");

        let trad_title = macroUtil.getTraduction("title").toUpperCase();
        let trad_still_wounded = macroUtil.getTraduction("still_wounded");
        let trad_no_more_wounded = macroUtil.getTraduction("no_more_wounded");
        let trad_days = macroUtil.getTraduction("days");
        let trad_no_wounded_hero = macroUtil.getTraduction("no_wounded_hero");

        let actorCommunity = game.tor2e.macro.utility.getCommunity();

        if (actorCommunity == null || actorCommunity === '') {
            ui.notifications.notify('===== ' + trad_ErrorMessageCommunityNotFound + ' ====');
        } else {
            let style_character = game.settings.get("tor-2e-macros", "health_reports_style_character");

            let message = {
                content: "<table class='tor2e-chatreports-table'>"
            };
            message.content = message.content + "<thead>";
            message.content = message.content + "   <tr'>";
            message.content = message.content + "       <th colspan=2' class='tor2e-chatreports-titre'>" + trad_title + "</th>";
            message.content = message.content + "   </tr>";
            message.content = message.content + "</thead><tbody>";

            let cpt = 0;
            actorCommunity.system.members.forEach(a => {
                let perso = game.actors.get(a.id);
                if (perso.type == "character") {
                    let maBlessure = perso.isWounded();

                    if (maBlessure != null && maBlessure != false) {
                        let nbDays = perso.system.stateOfHealth.wounded.value;

                        if (nbDays > 0) {
                            nbDays = nbDays - 1;
                        } else {
                            nbDays = 0;
                        }

                        cpt++;

                        const monUpdate = {
                            system: {
                                stateOfHealth: {
                                    wounded: {
                                        value: nbDays
                                    }
                                }
                            }
                        };

                        // CHARACTER
                        let nom = perso.name.substring(0, 10);
                        let posSpace = nom.indexOf(" ");
                        if (posSpace != -1) {
                            nom = nom.substring(0, posSpace);
                        }

                        let messageMaj = "";

                        const monUpdated = perso.update(monUpdate);
                        if (nbDays === 0) {
                            let monEffet = new Array();
                            
                            monEffet.push(maBlessure.id);
                            perso._deleteStatusEffectsByIds(monEffet);

                            messageMaj = trad_no_more_wounded;
                        } else {
                            messageMaj = trad_still_wounded + " " + nbDays + " " + trad_days;
                        }
                        if (style_character) {
                            message.content = message.content + "       <tr class='tor2e-chatreports-tr'><th class='tor2e-chatreports-th-name'><img src='" + perso.img + "' class='tor2e-chatreports-character-img' data-tooltip='" + perso.name + "' data-tooltip-direction='RIGHT'/></th>";
                        } else {
                            message.content = message.content + "       <tr class='tor2e-chatreports-tr'><th class='tor2e-chatreports-th-name'>" + nom + "</th>";
                        }
                        message.content = message.content + "   <td>" + messageMaj + "</td></tr>";
                    };
                }
            })
            if (cpt === 0) {
                message.content = message.content + "       <tr class='tor2e-chatreports-tr'><td colspan=2>" + trad_no_wounded_hero + "</td>";
            }
            // END MESSAGE 
            message.content = message.content + "</tbody>";
            message.content = message.content + "</table>";
            ChatMessage.create(message);
        }

    }
}

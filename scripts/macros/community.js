import { MacroUtil } from "../utils.js";

export class MacroCommunity {

    constructor() {
    }

    // ===================================================
    // CHANGE REGION FOR HUNT LIMIT
    // ===================================================    
    async community_huntLimit() {
        let actorCommunity = game.tor2e.macro.utility.getCommunity();

        if ( actorCommunity == null || actorCommunity === '' ) {
            ui.notifications.notify('===== ' + trad_ErrorMessageCommunityNotFound + ' ====');
        } else {        
            let macroUtil = new MacroUtil("community_EyeAwareness");

            let trad_Title = macroUtil.getTraduction("trad_Title");
            let trad_Legend = macroUtil.getTraduction("trad_Legend");
            let trad_SelectRegionQuestion = macroUtil.getTraduction("trad_SelectRegionQuestion");
            let trad_SelectRegionBorder = macroUtil.getTraduction("trad_SelectRegionBorder");
            let trad_SelectRegionWild = macroUtil.getTraduction("trad_SelectRegionWild");
            let trad_SelectRegionDark = macroUtil.getTraduction("trad_SelectRegionDark");
            let trad_SelectRegionNoSelection = macroUtil.getTraduction("trad_SelectRegionNoSelection");
            let trad_SelectRegionSeuilChasse = macroUtil.getTraduction("trad_SelectRegionSeuilChasse");
            let trad_MessageEyeAwarenessHigherHuntThreshold = macroUtil.getTraduction("trad_MessageEyeAwarenessHigherHuntThreshold");
            let trad_HuntTitle = macroUtil.getTraduction("trad_HuntTitle");
            let trad_StartHuntNow = macroUtil.getTraduction("trad_StartHuntNow");
            let trad_WaitNextEye = macroUtil.getTraduction("trad_WaitNextEye");

            let trad_ModChasseTitre = macroUtil.getTraduction("trad_HuntModifier");
            let trad_HuntModPlus4 = "<b>+4</b> : " + macroUtil.getTraduction("trad_HuntModPlus4");
            let trad_HuntModPlus2 = "<b>+2</b> : " + macroUtil.getTraduction("trad_HuntModPlus2");
            let trad_HuntModMinus2 = "<b>-2</b> : " + macroUtil.getTraduction("trad_HuntModMinus2");
            let trad_HuntModMinus4 = "<b>-4</b> : " + macroUtil.getTraduction("trad_HuntModMinus4");

            let macroDialogTrad = new MacroUtil("dialogs");
            let trad_annuler = macroDialogTrad.getTraduction("annuler");
            let trad_valider = macroDialogTrad.getTraduction("valider");


            let huntLimit = actorCommunity.system.eyeAwareness.max; 

            new Dialog({
                title: trad_Title,
                content: `
                <form>
                    <div class="form-group">
                        <table>
                            <tr>
                                <th style="vertical-align: top; margin: 15px; padding: 15px;">
                                    <label style="white-space: nowrap;">` + trad_SelectRegionQuestion + `</label>
                                </th>
                                <td style="vertical-align: top; margin: 15px; padding: 15px;">
                                    <select id="region" multiple rows=3 style="width: 150px !important; min-width: 150px; max-width: 150px;">
                                        <option value="border-land">` + trad_SelectRegionBorder + `</option>
                                        <option value="wild-land">` + trad_SelectRegionWild + `</option>
                                        <option value="dark-land">` + trad_SelectRegionDark + `</option>
                                    </select>
                                </td>
                                <td style="vertical-align: top; margin: 15px; padding: 15px;">
                                <u><b>` + trad_Legend + `</b></u><br> 
                                ` + trad_SelectRegionBorder + ` : 18 <br> 
                                ` + trad_SelectRegionWild + ` : 16 <br> 
                                ` + trad_SelectRegionDark + ` : 14
                                </td>
                            </tr>
                            <tr>
                                <th style="vertical-align: top; margin: 15px; padding: 15px;">
                                    <label style="white-space: nowrap;">` + trad_ModChasseTitre + `</label>
                                </th>
                                <td style="vertical-align: top; margin: 15px; padding: 15px;">
                                    <select id="modchasse" multiple rows=7 style="width: 100px !important; min-width: 100px; max-width: 100px; height: 120px !important;">
                                        <option value="4">+4</option>
                                        <option value="2">+2</option>
                                        <option value="0" selected>0</option>
                                        <option value="-2">-2</option>
                                        <option value="-4">-4</option>
                                    </select>
                                </td>
                                <td style="vertical-align: top; margin: 15px; padding: 15px;">
                                <u><b>` + trad_Legend + `</b></u><br> 
                                ` + trad_HuntModPlus4 + ` : 18 <br> 
                                ` + trad_HuntModPlus2 + ` : 16 <br> 
                                ` + trad_HuntModMinus2 + ` : 14 <br>
                                ` + trad_HuntModMinus4 + ` : 14
                                </td>
                            </tr>
                        </table>
                    </div>
                </form>`,
                buttons: {
                    button_cancel: {
                        label: trad_annuler,
                        callback: () => {},
                        icon: `<i class="fas fa-times"></i>`
                    },
                    button_save: {
                        label: trad_valider,
                        callback: html => {

                            let region = html.find('select#region')[0]?.value || null;
                            let mod = 0;
                            let modchasse = html.find('select#modchasse')[0]?.value || null;
                            if (modchasse !== null) {
                                mod = parseInt(modchasse);
                            }
                            let titreRegion = "";
        
                            if (region !== null) {    
                                if (region === "border-land") {
                                    huntLimit = 18 + mod;
                                    titreRegion = trad_SelectRegionBorder;
                                } else if (region === "wild-land") {
                                    huntLimit = 16 + mod;
                                    titreRegion = trad_SelectRegionWild;
                                } else if (region === "dark-land") {
                                    huntLimit = 14 + mod;
                                    titreRegion = trad_SelectRegionDark;
                                }
        
                                const monUpdate = {
                                    system: {
                                        eyeAwareness: {
                                            max: huntLimit
                                        }
                                    }
                                };
                                //const monUpdated = actorCommunity.update(monUpdate);
                                this.updateCommunityHuntThreshold(actorCommunity, huntLimit, titreRegion, trad_SelectRegionSeuilChasse);
        
        
                                // Is the Hunt Threshold activated now ?
                                let currentEye = actorCommunity.system.eyeAwareness.value; 
                                if (currentEye > (huntLimit-1)) {
                                    let d = new Dialog({
                                        title: trad_HuntTitle,
                                        content: trad_MessageEyeAwarenessHigherHuntThreshold,
                                        buttons: {
                                            no: {
                                              icon: '<i class="fas fa-times"></i>',
                                              label: trad_WaitNextEye
                                            },
                                            yes: {
                                              icon: '<i class="fas fa-check"></i>',
                                              label: trad_StartHuntNow,
                                              callback: (html) => {
                                                this.startTheHunt();
                                              }
                                            },
                                        },
                                        default: 'yes',
                                    }).render(true);
                                }
        
                            } else {
                                ui.notifications.notify('===== ' + trad_SelectRegionNoSelection + ' ====');
                            }
                        },
                        icon: `<i class="fas fa-check"></i>`
                      }
                },
                default: 'button_save',
                close: () => {}
            },{width:800, height:450}).render(true);
        }
    }

    // ===================================================
    // CHANGE LE SEUL DE CHASSE (lié à la région)
    // ===================================================       
    async updateCommunityHuntThreshold(actorCommunity, huntLimit, titreRegion, titreSeuilChasse) {
        // UPDATE THE HUNT THRESHOLD
        const monUpdate = {
            system: {
                eyeAwareness: {
                    max: huntLimit
                }
            }
        };
        const monUpdated = await actorCommunity.update(monUpdate);

        // INFORM THE LOREMASTER IN THE CHAT
        let eyeImageHunt = '<img src="modules/tor-2e-macros/images/EyeOfSauron.webp" style="width: auto; height: 30px; border: none;">';
        let monMessage = '<table><tr style="text-align: center;"><th colspan=2>' + titreRegion + '</th></tr><tr style="text-align: center;"><td rowspan=2>' + eyeImageHunt + '</td><th>' + titreSeuilChasse + '</th></tr><tr style="text-align: center;"><th>' + huntLimit + '</th></tr></table>';
        let message = {
            content: monMessage
        };
        game.users.forEach(t => {
            if (t.role === 4) {
                message.whisper = ChatMessage.getWhisperRecipients(t.name);
                ChatMessage.create(message);
            }
        });        
    }

    
    // ===================================================
    // Manage EYE AWARENESS : increase and alert 
    // ===================================================
    async community_eyeAwareness() {
        // -----------------------------------------------
        // Get traduction of terms
        // -----------------------------------------------
        let macroUtil = new MacroUtil("community_EyeAwareness");
        let trad_MessageEyeAwareness = macroUtil.getTraduction("trad_MessageEyeAwareness");
        let trad_MessageHuntThreshold = macroUtil.getTraduction("trad_MessageHuntThreshold");
        let trad_MessageEyeIncrease = macroUtil.getTraduction("trad_MessageEyeIncrease");
        let trad_MessageEyeMax = macroUtil.getTraduction("trad_MessageEyeMax");
        let trad_ErrorMessageCommunityNotFound = macroUtil.getTraduction("trad_ErrorMessageCommunityNotFound");

        
        // -----------------------------------------------
        // Get current default Company
        // -----------------------------------------------
        let actorCommunity = game.tor2e.macro.utility.getCommunity();
        if ( actorCommunity == null || actorCommunity === '' ) {
            ui.notifications.notify('===== ' + trad_ErrorMessageCommunityNotFound + ' ====');
        } else {
            // -----------------------------------------------
            // Get the Eye Awareness values from the Community sheet
            //  - startEye : Starting Value of Eye Awareness
            //  - currentEye : Current Value of Eye Awareness
            // -----------------------------------------------
            let valid = 1;
            let startEye = actorCommunity.system.eyeAwareness.initial.value;
            let currentEye = actorCommunity.system.eyeAwareness.value; 

            if ( startEye == null ) {
                startEye = 0;
                //ui.notifications.notify('===== ' + trad_ErrorMessageStartEyeZero + ' ====');
                valid = 0;
            }
            if ( currentEye == null ) {
                currentEye = 0;
                //ui.notifications.notify('===== ' + trad_ErrorMessageCurrentEyeZero + ' ====');
                valid = 0;
            }

            if ( valid === 1 ) {
                // -----------------------------------------------
                // Images
                // -----------------------------------------------
                // Image for the chat
                let eyeImage = '<img src="modules/tor-2e-macros/images/EyeOfSauron.webp" style="width: auto; height: 30px; border: none;">';

                // Image for the Hunt
                let eyeImageHunt = '<img src="modules/tor-2e-macros/images/EyeOfSauron.webp" style="border: none;">';

                // -----------------------------------------------
                // Sons
                // -----------------------------------------------
                // Eye awareness increase sound
                let settingEyeIncreaseSoundActive = true;
                let settingEyeIncrease = 'modules/tor-2e-macros/sounds/eyeIncrease.mp3';

                // Eye awareness max sound
                let settingEyeMaxSoundActive = true;
                let settingEyeMaxSound = 'modules/tor-2e-macros/sounds/eyeEvent.mp3';
     

                // -----------------------------------------------
                // Options
                // -----------------------------------------------
                // Visibilité : Loremaster, joueurs ?
                let settingVisibility = game.settings.get("tor-2e-macros", "Eye awareness visibility");


                // -----------------------------------------------
                // huntLimit : à partir de quand ils sont détectés
                // -----------------------------------------------
                let huntLimit = actorCommunity.system.eyeAwareness.max; 


                if (currentEye > (huntLimit - 2)) {
                    // -----------------------------------------------
                    // Cas 1 : on atteint la limite
                    //      ==> currentEye is one less than the hunt limit
                    //      ==> we triggers the Hunt
                    // -----------------------------------------------
                    this.updateEyeAwareness(actorCommunity, startEye);
                    
                    //play a sound effect and a Narrator effect - use your sound effect and image to display on Chat
                    if ( settingEyeMaxSoundActive ) {
                        AudioHelper.play({ src: settingEyeMaxSound, volume: 1, loop: false }, true);
                    }

                    let monMessage = '<table><tr style="text-align: center;"><th>' + trad_MessageEyeMax + '</th></tr><tr style="text-align: center;"><th>' + eyeImageHunt + '</th></tr></table>';

                    let message = {
                        content: monMessage
                    };
                    if ( settingVisibility === 'loremaster' ) {
                        game.users.forEach(t => {
                            if (t.role === 4) {
                                message.whisper = ChatMessage.getWhisperRecipients(t.name);
                                ChatMessage.create(message);
                            }
                        });
                    } else {
                        ChatMessage.create(message);
                    }
                    /*
        CONST.CHAT_MESSAGE_TYPES
        EMOTE: 3
        IC: 2
        OOC: 1
        OTHER: 0
        ROLL: 5
        WHISPER: 4


        NarratorTools.chatMessage.narrate('gfdgdf  gdgg')       ==> chat + scene
        NarratorTools.chatMessage.describe('gfdgdf  gdgg')      ==> chat seulement


        https://github.com/elizeuangelo/fvtt-module-narrator-tools/wiki

                    */
                    //NarratorTools.chatMessage.narrate(trad_MessageEyeMax + ' ' + eyeImage);
                } else {
                    // -----------------------------------------------
                    // Cas 2 : on augmente la vigilance de l'Oeil
                    //      ==> currentEye isn't one less than the hunt limit
                    //      ==> we increase the currentEye by 1
                    // -----------------------------------------------
                    this.updateEyeAwareness(actorCommunity, ++currentEye);
                    
                    //play a sound effect and a Narrator effect - use your sound effect and image to display on Chat
                    if ( settingEyeIncreaseSoundActive ) {
                        AudioHelper.play({ src: settingEyeIncrease, volume: 1, loop: false }, true);
                    }

                    let monMessage = '<table><tr style="text-align: center;"><th colspan="3">' + trad_MessageEyeIncrease + '</th></tr><tr style="text-align: center;"><th>' + trad_MessageEyeAwareness + '</th><th rowspan="2">' + eyeImage + '</th><th>' + trad_MessageHuntThreshold + '</th></tr><tr style="text-align: center;"><td>' + currentEye + '</td><td>' + huntLimit + '</td></tr></table>';
                    
                    let message = {
                        content: monMessage
                    };
                    if ( settingVisibility === 'loremaster' ) {
                        game.users.forEach(t => {
                            if (t.role === 4) {
                                message.whisper = ChatMessage.getWhisperRecipients(t.name);
                                ChatMessage.create(message);
                            }
                        });
                    } else {
                        ChatMessage.create(message);
                    }

                    
                // NarratorTools.chatMessage.narrate(trad_MessageEyeIncrease + ' ' + currentEye + '.' + ' ' + eyeImage);
                }
            }
        }
    }

    // ===================================================
    // UPDATE EYE AWARENESS IN THE ACTOR COMMUNITY
    // ===================================================    
    async updateEyeAwareness(actorCommunity, value) {
        let updatedValue = {
            system: {
                eyeAwareness: {
                    value: value
                }
            }
        };
        await actorCommunity.update(updatedValue);
    }

    // ===================================================
    // START THE HUNT
    // ===================================================    
    async startTheHunt() {
        let macroUtil = new MacroUtil("community_EyeAwareness");
        let trad_MessageEyeMax = macroUtil.getTraduction("trad_MessageEyeMax");


        let settingEyeImageHunt = game.settings.get("tor-2e-macros", "Eye awareness hunt image");
        let eyeImageHunt = "";
        if (settingEyeImageHunt == null || settingEyeImageHunt === '' ) {
            eyeImageHunt = '<img src="modules/tor-2e-macros/images/EyeOfSauron.webp" style="border: none;">';
        } else {
            eyeImageHunt = '<img src="' + settingEyeImageHunt + '" style="border: none;">';
        }        

        // Eye awareness max sound
        let settingEyeMaxSoundActive = game.settings.get("tor-2e-macros", "Eye awareness max sound active");
        if (settingEyeMaxSoundActive == null || settingEyeMaxSoundActive === '' ) {
            settingEyeMaxSoundActive = true;
        }

        let settingEyeMaxSound = game.settings.get("tor-2e-macros", "Eye awareness max sound");
        if (settingEyeMaxSound == null || settingEyeMaxSound === '' ) {
            settingEyeMaxSound = 'modules/tor-2e-macros/sounds/eyeEvent.mp3';
        }        

        let settingVisibility = game.settings.get("tor-2e-macros", "Eye awareness visibility");


        //play a sound effect and a Narrator effect - use your sound effect and image to display on Chat
        if ( settingEyeMaxSoundActive ) {
            AudioHelper.play({ src: settingEyeMaxSound, volume: 1, loop: false }, true);
        }

        let monMessage = '<table><tr style="text-align: center;"><th>' + trad_MessageEyeMax + '</th></tr><tr style="text-align: center;"><th>' + eyeImageHunt + '</th></tr></table>';

        let message = {
            content: monMessage
        };
        if ( settingVisibility === 'loremaster' ) {
            game.users.forEach(t => {
                if (t.role === 4) {
                    message.whisper = ChatMessage.getWhisperRecipients(t.name);
                    ChatMessage.create(message);
                }
            });
        } else {
            ChatMessage.create(message);
        }

        let actorCommunity = game.tor2e.macro.utility.getCommunity();
        let startEye = actorCommunity.system.eyeAwareness.initial.value;
        this.updateEyeAwareness(actorCommunity, startEye);
    }
}

import { MacroUtil } from "../utils.js";

export class MacroToken {

    constructor() {
    }


    // ===========================================================================================
    //           OVERLAY ICON ON THE TOKEN
    // ===========================================================================================
    // DEAD (mort)
    token_Set_DeadOrNot() {
        if(game.release.generation == "12") {
            canvas.tokens.controlled.forEach(token => {
                token.actor.toggleStatusEffect("dead", { overlay: true });
            });
        } else {
            const icone = "icons/svg/skull.svg";
            canvas.tokens.controlled.forEach(token => {
                token.toggleEffect(icone, { overlay: true });
            });
        }
    }
    // UNCONSCIOUS (inconscient)
    token_Set_UnconsciousOrNot() {
        if(game.release.generation == "12") {
            canvas.tokens.controlled.forEach(token => {
                token.actor.toggleStatusEffect("unconscious", { overlay: true });
             });  
        } else {
            const icone = "icons/svg/unconscious.svg";
            canvas.tokens.controlled.forEach(token => {
                token.toggleEffect(icone, { overlay: true });
            });
        }
    }

    // ===========================================================================================
    //           HIDDEN OR SHOWED
    // ===========================================================================================
    // Show or hide the selected token(s) or tile(s) in the current canvas
    token_Set_Hidden_or_not() {
        // Tokens
        const updateTokens = [];
        for (let token of canvas.tokens.controlled) {
            updateTokens.push({
                _id: token.id,
                hidden: !token.document.hidden
            });
        };
        canvas.scene.updateEmbeddedDocuments("Token", updateTokens);

        // Tiles
        const updateTiles = [];
        for (let tile of canvas.tiles.controlled) {
            updateTiles.push({
               _id: tile.id,
               hidden: !tile.document.hidden
            });
        };
        canvas.scene.updateEmbeddedDocuments("Tile", updateTiles);
    }    

    // ===========================================================================================
    //           CHANGE THE TOKEN SIZE ON CURRENT SCENE
    // ===========================================================================================
    token_Set_Size() {
        // Get traduction of terms
        let macroUtil = new MacroUtil("tokens_Set_Size");
        let trad_DialogTitre = macroUtil.getTraduction("titre");
        let trad_DialogFermer = macroUtil.getTraduction("fermer");
        let trad_selectToken = macroUtil.getTraduction("alerte_select_token");

        let tokens = canvas.tokens.controlled;
        if (tokens.length == 0) {
            ui.notifications.warn(trad_selectToken);
        } else {
            const transformations = {
                minuscule: {
                    label: "0.25",
                    scale: 0.25
                },
                petit: {
                    label: "0.5",
                    scale: 0.5
                },
                moyen: {
                    label: "0.75",
                    scale: 0.75
                },
                normal: {
                    label: "1",
                    scale: 1
                },
                grand: {
                    label: "1.5",
                    scale: 1.5
                },
                geant: {
                    label: "2",
                    scale: 2
                },
                gigantesque: {
                    label: "3",
                    scale: 3
                },
                immense: {
                    label: "5",
                    scale: 5
                },
                superimmense: {
                    label: "7",
                    scale: 7
                },
                megaimmense: {
                    label: "9",
                    scale: 9
                }, 
            };
    
    
            new Dialog({
                title: trad_DialogTitre ,
                content: `
                    <form>
                        <div style="display: flex; align-content: center;">
                        </div>
                    </form>`,
                buttons: this.getTransformations(transformations),
                default: trad_DialogFermer ,
                close: () => { }
            }).render(true);
        }
    }

    getTransformations(transformations) {
        let gameVersion = game.version;
        let mySplit = gameVersion.split(".");
        let majorVersion = mySplit[0];

        let transformButtons = {};
        Object.entries(transformations).forEach(([key, transform]) => {
            transformButtons[key] = {
                label: transform.label,
                callback: (html) => {
                    if( majorVersion == "12") {
                        this.resizeToken(transform.scale);
                    } else {
                        this.redimToken(transform.scale);
                    }
                }
            }
        });
        return transformButtons;
    }
    resizeToken(facteur) {
        canvas.tokens.updateAll(
          (t) => ({
             texture: {
                scaleX: facteur,
                scaleY: facteur,
             },
          }),
          (t) => t.controlled,
            { animate: false }
        );        
    }    
    redimToken(facteur) {
        const update = {
            scale: facteur
        };

        const updates = [];
        for (let token of canvas.tokens.controlled) {
          updates.push({
            _id: token.id,
            scale: facteur
          });
        };
        canvas.scene.updateEmbeddedDocuments("Token", updates);
    
    }    


    // ===========================================================================================
    // Display to all players the token image in a large view
    // ===========================================================================================
    token_DisplayTokenImage() {
        let tokens = canvas.tokens.controlled;
        let token = null;
        if (tokens.length > 0) {
            token = tokens[0];
        }
        if (token == null) {
            ui.notifications.warn("Please select token first.");
        } else {
            let tActor = token.actor;
            let ip = new ImagePopout(tActor.img, {
                title: tActor.name,
                shareable: true,
                uuid: tActor.uuid
            }).render(true);
            ip.shareImage();
        }
    }
}    
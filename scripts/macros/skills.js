import { MacroUtil } from "../utils.js";

class myDialog extends Dialog
{
    processSkill(skill_name, skill_attribute)
    {

        let options = {};
        /*options = {
            actorId: actor_data._id,
            skillName: skill_name,
            actionName: "Number",
            associateAttribute: skill_attribute
        }*/

        game.tor2e.macro.utility.rollSkillMacro(skill_name, options);
        this.close();
    }

    activateListeners(html)
    {
        super.activateListeners(html);
        html.find('.skillList').click(ev =>
        {
            const skill_name = $(ev.currentTarget).data("skill-name");
            const skill_attribute = $(ev.currentTarget).data("skill-associatedattribute");
            this.processSkill(skill_name, skill_attribute);
        });
    }
}

export class MacroSkills {

    constructor() {
    }
    // ==================================================
    // AFFICHE LES COMPETENCES D'UN PJ ET PERMET DE LANCER UN JET
    // ==================================================
    async skills_private_roll() {
        // Get traduction of terms
        let macroUtil = new MacroUtil("skill_private_roll");

        let trad_DialogTitre = macroUtil.getTraduction("titre");
        let trad_DialogCompetences = macroUtil.getTraduction("competence");
        let trad_DialogContinuer = macroUtil.getTraduction("continuer");
        let trad_DialogAlertNoTokenSelected = macroUtil.getTraduction("no_token_selected");
        let trad_DialogBonus = macroUtil.getTraduction("bonus");
        let trad_annuler = macroUtil.getTraduction("annuler");
        let trad_no_skill_selected = macroUtil.getTraduction("no_skill_selected");
        let trad_only_one_skill = macroUtil.getTraduction("only_one_skill");
        
        let arrayCompetences = ["athletics", "awareness", "awe", "battle", "courtesy", "craft", "enhearten", "explore", "healing", "hunting", "insight", "lore", "persuade", "riddle", "scan", "song", "stealth", "travel"];


        let tokens = canvas.tokens.controlled;
        if (tokens.length == 0) {
            ui.notifications.warn(trad_DialogAlertNoTokenSelected);
            return;
        } 

        let nb_tokens_characters = 0;
        tokens.forEach(t => {
            if(t.actor?.type === "character") {
                nb_tokens_characters++;
            }
        });
        if(nb_tokens_characters == 0) {
            ui.notifications.warn(trad_DialogAlertNoTokenSelected);
            return;
        }

        let formHtml = "<table>";
        formHtml = formHtml + "<tr><th nowrap style='text-align: left;'><label>"+trad_DialogBonus+" : </label></th>";
        formHtml = formHtml + "<td style='vertical-align: top;'><input name='champ-bonus' style='width: 30px; text-align: center;' value='0'>d</input></td></tr>";
        
        formHtml = formHtml + "<tr><th nowrap colspan=2 style='text-align: left;'><label>"+trad_DialogCompetences+" : </label></th></tr>";
        formHtml = formHtml + "<tr>";
        formHtml = formHtml + "<td colspan=2 style='vertical-align: top;'><select id='competences' multiple rows=20 style='width: 200px; height: 350px;'>";
        arrayCompetences.forEach(c => {
            formHtml = formHtml + "<option value='" + game.i18n.localize("tor2e.commonSkills."+c) + "'>" + game.i18n.localize("tor2e.commonSkills."+c) + "</option>";
        });
        formHtml = formHtml + "</select></td>";
        formHtml = formHtml + "</tr>";
        formHtml = formHtml + "</table>";

        
        new Dialog({
            title: trad_DialogTitre,
            content: formHtml,
            buttons: {
                button_cancel: {
                    label: trad_annuler,
                    callback: () => { },
                    icon: `<i class="fas fa-times"></i>`
                },
                button_roll: {
                    icon: "<i class='fas fa-check'></i>",
                    label: trad_DialogContinuer,
                    callback: html => {
                        let monBonus = html.find('[name=champ-bonus]')[0]?.value || 0;
                        let maComp = html.find('select#competences');
                        let options = maComp[0].selectedOptions;

                        if(Array.from(options).map(({ value }) => value).length > 1) {
                            ui.notifications.warn(trad_only_one_skill);
                            return;
                        }

                        let maCompName = Array.from(options).map(({ value }) => value).toString();
                        
                        if(maCompName == null || maCompName == "") {
                            ui.notifications.warn(trad_no_skill_selected);
                        } else {
                            this.set_roll_mode("selfroll");
                            tokens.forEach(t => {
                                if(t.actor?.type === "character") {
                                    this.lance_actor_self_roll(t.actor, maCompName, monBonus);
                                }
                            });
                            this.sleep(1000).then(() => { this.set_roll_mode("publicroll"); });        
                        }
                    }
                }
            },
            default: 'button_roll',
            close: () => { }
        },{width:290, height:550}).render(true);
    }

    async sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    async set_roll_mode(mode) {
        //['publicroll', 'gmroll', 'blindroll', 'selfroll'];
        await game.settings.set('core', 'rollMode', mode);
        await ChatLog._setRollMode(mode);
    }

    async lance_actor_self_roll(actor, competence, monBonus) {
        await game.tor2e.macro.utility.rollSkillMacro(competence, false, monBonus, actor, monBonus);
    }

    // ==================================================
    // AFFICHE LES COMPETENCES D'UN PJ ET PERMET DE LANCER UN JET
    // ==================================================
    async skills_character_roll() {
        let macroUtil = new MacroUtil("skill_roll");
        let trad_SelectCharacter = macroUtil.getTraduction("select_character");
        const trad_selectToken_error = "tor2e.macro.error.noActorAvailable"
        const trad_title = game.i18n.localize("tor2e.actors.sections.commonSkills") + " : "
        const trad_ColumnName_Skill = "" //game.i18n.localize("tor2e.actors.skills.title")
        const trad_Column_SkillRange = ""
        const trad_Column_difficulty = game.i18n.localize("tor2e.roll.difficulty")
        const trad_attribute = game.i18n.localize("tor2e.actors.sections.attributes")
        const trad_attribute_strength_name = game.i18n.localize("tor2e.stats.strength")
        const trad_attribute_heart_name = game.i18n.localize("tor2e.stats.heart")
        const trad_attribute_wits_name = game.i18n.localize("tor2e.stats.wits")

        const user_id = game.userId;
        const user_character_id = game.user._id;

        let current_user_actor = game.actors.find((actor) =>
        {
            return actor._id === user_character_id;
        });

        if (current_user_actor === undefined && game.user.role === 4)
        {
            const tokens = canvas.tokens.controlled;
            const token_actor = tokens.length !== 0? tokens[0] : null;
            current_user_actor = token_actor? token_actor.actor : null;
        }

        if (!current_user_actor)                        return ui.notifications.warn(game.i18n.localize(trad_selectToken_error));
        if (current_user_actor.type != "character")    return ui.notifications.warn(trad_SelectCharacter);

        const commonSkills_list = current_user_actor.system.commonSkills;
        let skill_table_id = 0;
        let content_form_window = "";

        const roll_dices_button_image = 'systems/tor2e/assets/images/icons/miscellaneous/dice-roll.png'
        const skill_images_route = 'systems/tor2e/assets/images/icons/skills/skill-'
        const skill_image_format = '.webp'

        const table_separator_style = `<div style='width:0.4%;background-color:black'></div>`;

        content_form_window = content_form_window + this.createActorSkillResumeWindow(commonSkills_list, content_form_window, current_user_actor, trad_attribute_strength_name, trad_attribute_heart_name, trad_attribute_wits_name, trad_Column_difficulty, trad_ColumnName_Skill, trad_Column_SkillRange, skill_images_route, skill_image_format, skill_table_id);

        let data = {
            title: trad_title + " " + current_user_actor.name,
            content: content_form_window,
            buttons: {
                //    use: {
                //        label: "Fermer",
                //        callback: html => close()
                //    }
            },
            default: "Fermer",
            close: () => { }
        };
        const options = { width: 900, height: 600 };
        let mydiag = new myDialog(data, options);
        mydiag.render(true);

    }

    createActorSkillResumeWindow(commonSkills_list, content_form_window, current_user_actor, trad_attribute_strength_name, trad_attribute_heart_name, trad_attribute_wits_name, trad_Column_difficulty, trad_ColumnName_Skill, trad_Column_SkillRange, skill_images_route, skill_image_format, skill_table_id)
    {
        const strength_skills = Object.keys(commonSkills_list).filter((skill) =>
        {
            return commonSkills_list[skill].roll.associatedAttribute === 'strength'
        });

        const heart_skills = Object.keys(commonSkills_list).filter((skill) =>
        {
            return commonSkills_list[skill].roll.associatedAttribute === 'heart'
        });

        const wits_skills = Object.keys(commonSkills_list).filter((skill) =>
        {
            return commonSkills_list[skill].roll.associatedAttribute === 'wits'
        });
        content_form_window = content_form_window + `<div style='text-align:center;'>`
        content_form_window = content_form_window + this.createSkillTableFromAttribute(commonSkills_list, strength_skills, current_user_actor.extendedData.getStrengthTn(), trad_attribute_strength_name, trad_Column_difficulty, trad_ColumnName_Skill, trad_Column_SkillRange, skill_images_route, skill_image_format, skill_table_id);
        content_form_window = content_form_window + this.createSkillTableFromAttribute(commonSkills_list, heart_skills, current_user_actor.extendedData.getHeartTn(), trad_attribute_heart_name, trad_Column_difficulty, trad_ColumnName_Skill, trad_Column_SkillRange, skill_images_route, skill_image_format, skill_table_id);
        content_form_window = content_form_window + this.createSkillTableFromAttribute(commonSkills_list, wits_skills, current_user_actor.extendedData.getWitsTn(), trad_attribute_wits_name, trad_Column_difficulty, trad_ColumnName_Skill,trad_Column_SkillRange, skill_images_route, skill_image_format, skill_table_id);
        content_form_window = content_form_window + `</div>`;
        return content_form_window;
    }

    createSkillTableFromAttribute(commonSkills_list, attribute_skill_list, attribute_value, attribute_name, trad_Column_difficulty, trad_ColumnName_Skill, trad_Column_SkillRange, skill_images_route, skill_image_format, skill_table_id)
    {
        const column_difficulty_name = trad_Column_difficulty;
        const table_init = `<div style=''><table style='border-right:1px solid var(--color-border-light-tertiary); border-left:1px solid var(--color-border-light-tertiary);'>` + `<tr style='text-align:center'><th style=''></th><th style=''>` + trad_ColumnName_Skill.toUpperCase() + `</th><th style=''>` + trad_Column_SkillRange + `</th></tr>`;
        const attribute_difficulty_display = `<div style='display:inline-table;align-items: center;justify-content: center; width:33%; padding:5px'>
        <div style='text-align:center;'><h2 style='margin:5px; color:darkred'>` + attribute_name.toUpperCase() + `</h2><div style='border:solid; border-style: dashed; border-width:1px; padding:5px;padding-top:7px'>` + column_difficulty_name + `<hr><div style='font-weight: bold;font-size: 1.3em'>` + attribute_value + `</div></div></div>`;
        
        let skill_table_form = attribute_difficulty_display;
        skill_table_form = skill_table_form + table_init;
        
        attribute_skill_list.forEach((skill) =>
        {
            const skill_label = commonSkills_list[skill].label;
            const skill_name = game.i18n.localize(skill_label);
            const skill_level = commonSkills_list[skill].value;
            const skill_isFavoured = commonSkills_list[skill].favoured.value;
            const skill_roll_associatedAttribute = commonSkills_list[skill].roll.associatedAttribute;

            skill_table_form = skill_table_form + `<tr style='text-align:center'>`;
            skill_table_form = skill_table_form + `<td><input type="image" src="` + skill_images_route + skill + skill_image_format + `" width="48px" height="48px" class="skillList" data-skill-name="${skill_name}" data-skill-associatedattribute="${skill_roll_associatedAttribute}" border="2px" id="skill${skill_table_id}"></input></td>`;
            skill_table_form = skill_table_form + `<td style='text-align:center; font-size: 1em; ${skill_isFavoured ? 'text-decoration: underline' : ''}'>` + skill_name + `</td>`;

            let skill_level_icon = "";
            const skill_level_icon_style = "font-size: 1em;width: 0.7em;height: 0.7em;transform: rotate(45deg);border: 1.5px solid #AC323BFF;margin: 0 0.2em;flex: none;background-color: #AC323BFF;display:inline-block";
            for (let i = 0; i < skill_level; i++)
            {
                skill_level_icon = skill_level_icon + `<div style="${skill_level_icon_style}"></div>`;
            }
            if (skill_level === 0)
            {
                skill_level_icon = `<div>-</div>`;
            }
            skill_table_form = skill_table_form + `<td data-skill-level>${skill_level_icon}</td>`;
            skill_table_form = skill_table_form + `</tr>`;
            skill_table_id = skill_table_id + 1;
        });

        skill_table_form = skill_table_form + `</table></div></div>`;

        return skill_table_form;
    }
}
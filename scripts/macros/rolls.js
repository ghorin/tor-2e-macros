import { MacroUtil } from "../utils.js";

export class MacroRoll {

    constructor() {
    }

    async roll_ManualRoll() {
        // Get traduction of terms      
        let macroUtil = new MacroUtil("roll_RollDices");

        let trad_rollDialogTitle = macroUtil.getTraduction("trad_rollDialogTitle");
        let trad_rollFeatDice = macroUtil.getTraduction("trad_rollFeatDice");
        let trad_rollSuccessDice = macroUtil.getTraduction("trad_rollSuccessDice");
        let trad_rollBestOfTwoDice = macroUtil.getTraduction("trad_rollBestOfTwoDice");
        let trad_rollWorstOfTwoDice = macroUtil.getTraduction("trad_rollWorstOfTwoDice");
        let trad_rollDialogLaunch = macroUtil.getTraduction("trad_rollDialogLaunch");
        let trad_rollDialogCancel = macroUtil.getTraduction("trad_rollDialogCancel");

        new Dialog({
            title: trad_rollDialogTitle,
            content: `
            <form>
                <div >
                    <table>
                        <tr style='text-align:center'>
                            <th><label style="white-space: nowrap;">` + trad_rollFeatDice + `</label></th>
                            <th><label style="white-space: nowrap;">` + trad_rollSuccessDice + `</label></th>
                        </tr>
                        <tr style='text-align:center; vertical-align: bottom;'>
                            <td style='text-align:center; vertical-align: bottom;'>
                                <select id="FeatDice"  style="height: 24px !important; min-height: 24px; max-height: 24px; width: 150px !important; min-width: 150px; max-width: 150px;" />
                                    <option value="0df">0</option>
                                    <option value="1df" selected>1</option>
                                    <option value="2dfkh">` + trad_rollBestOfTwoDice + `</option>
                                    <option value="2dfkl">` + trad_rollWorstOfTwoDice + `</option>
                                </select>
                            </td>
                            <td style='text-align:center; vertical-align: bottom;'>
                                <select id="SuccessDice" style="height: 24px !important; min-height: 24px; max-height: 24px; width: 70px !important; min-width: 70px; max-width: 70px;" />
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </td>                            
                        </tr>
                    </table>
                </div>
            </form>`,
            buttons: {
                cancel: {
                    label: trad_rollDialogCancel,
                },
                yes: {
                    icon: "<i class='fa-solid fa-dice-d20'></i>",
                    label: trad_rollDialogLaunch,
                    callback: (html) => {
                        // Feat Dice(s)
                        let featDice = html.find('select#FeatDice')[0]?.value || null;

                        // Success Dice(s)
                        let successDice = html.find('select#SuccessDice')[0]?.value || null;

                        if (featDice != null && successDice != null) {
                            (async () => {
                                if(macroUtil.isFoundryV12()) {
                                    const roll = await new Roll(`${featDice} + ${successDice}ds`).evaluate({evaluateSync: false});
                                    roll.toMessage();
                                } else {
                                    const roll = await new Roll(`${featDice} + ${successDice}ds`).evaluate({aync: false});
                                    roll.toMessage();    
                                }
                            })();
                        }
                    }
                }
            },
            default: 'yes',
            close: html => { },
        }, { width: 400, height: 170 }).render(true);
    }
}
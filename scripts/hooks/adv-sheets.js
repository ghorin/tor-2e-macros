import { MacroUtil } from "../utils.js";

export class AdvSheets {

    // CONSTRUCOR of the Macrobar
    constructor() {
        this.setHookOnRenderSheet();
    }

    setHookOnRenderSheet() {
        Hooks.on("renderActorSheet", object => {
            let DEBUG_OneAdvOnly = false;
            // RENDER STATES :  https://foundryvtt.com/api/classes/client.Application.html#RENDER_STATES
            if (object._priorState === 0 || object._priorState === -1) {
                if (object.actor.extendedData.isAdversary) {
                    // NOUVELLE FEUILLE ADV
                    if(DEBUG_OneAdvOnly)    console.log(object);

                    let newAdvActor = null;
                    let newAdvToken = null;
                    let newAdvIsLinked = null
                    let newIsToken = null;

                    newAdvActor = object.actor
                    if(object.token === null) {
                        if(DEBUG_OneAdvOnly)    console.log("... is a prototype token");
                        newAdvToken = newAdvActor.prototypeToken;
                        newAdvIsLinked = newAdvToken.actorLink;
                        newIsToken = false;
                    } else {
                        if(DEBUG_OneAdvOnly)    console.log("... is a token");
                        newAdvToken = object.token;
                        newAdvIsLinked = newAdvToken.isLinked;
                        newIsToken = true;
                    }
                    
                    if(DEBUG_OneAdvOnly) {
                        console.log("===============");
                        console.log("=== NOUVEAU ===");
                        console.log("===============");
                        console.log("   - Actor : " + newAdvActor.name + " / " + newAdvActor.id);
                        console.log("   - Token : " + newAdvToken.name + " / " + newAdvToken.id);
                        console.log("   - is linked : " + newAdvIsLinked);
                    }

                    // FERMER LES FEUILLES ADV PRECEDENTES (si il y en a une)
                    let setting_AdvOnlyOneSheet = game.settings.get("tor-2e-macros", "adv_sheet_OnlyOne");
                    if (setting_AdvOnlyOneSheet) {
                        let listeFeuillesAdv = Object.values(ui.windows).filter(i => i.object?.type === 'adversary');

                        for (let cpt = listeFeuillesAdv.length - 1; cpt > -1; cpt--) {

                            let current = listeFeuillesAdv[cpt];

                            let currentAdvActor = null;
                            let currentAdvToken = null;
                            let currentAdvIsLinked = null;
                            let currentIsToken = null;

                            currentAdvActor = current.actor;

                            if(current.token === null) {
                                if(DEBUG_OneAdvOnly)    console.log("... is a prototype token");
                                currentAdvToken = currentAdvActor.prototypeToken;
                                currentAdvIsLinked = currentAdvToken.actorLink;
                                currentIsToken = false;
                            } else {
                                if(DEBUG_OneAdvOnly)    console.log("... is a token");
                                currentAdvToken = current.token;
                                currentAdvIsLinked = currentAdvToken.isLinked;
                                currentIsToken = true;
                            }
        
                            if(DEBUG_OneAdvOnly) {
                                console.log("=== CURRENT ===");
                                console.log("   - Actor : " + currentAdvActor.name + " / " + currentAdvActor.id);
                                console.log("   - Token : " + currentAdvToken.name + " / " + currentAdvToken.id);
                                console.log("   - is linked : " + currentAdvIsLinked);
                            }


                            if(newAdvIsLinked) {
                                // New adv : its token is linked to the actor sheet
                                if(currentAdvIsLinked) {
                                    // Current adv : its token is linked to the actor sheet
                                    if(currentAdvActor.id != newAdvActor.id) {
                                        if(DEBUG_OneAdvOnly)    console.log("       => (1.1) not the same => CLOSE");
                                        listeFeuillesAdv[cpt].close();
                                    } else {
                                        if(DEBUG_OneAdvOnly)    console.log("       => (1.2) same => DON'T CLOSE");
                                    }
                                } else {
                                    // Current adv : its token is NOT linked to the actor sheet
                                    if(DEBUG_OneAdvOnly)    console.log("       => (2.0) not the same => CLOSE");
                                    listeFeuillesAdv[cpt].close();
                                }
                            } else {
                                if(currentAdvIsLinked) {
                                    // Current adv : its token is linked to the actor sheet
                                    if(DEBUG_OneAdvOnly)    console.log("       => (3.0) not the same => CLOSE");
                                    listeFeuillesAdv[cpt].close();
                                } else {
                                    // Current adv : its token is NOT linked to the actor sheet
                                    if(newIsToken) {
                                        // New adv : is a token
                                        if(currentIsToken) {
                                            // Current adv : is a token
                                            if(currentAdvToken.id != newAdvToken.id) {
                                                if(DEBUG_OneAdvOnly)    console.log("       => (4.1.1) New and Current are tokens => diff token => CLOSE");
                                                listeFeuillesAdv[cpt].close();
                                            } else {
                                                if(DEBUG_OneAdvOnly)    console.log("       => (4.1.2) New and Current are tokens => same token => DON'T CLOSE");
                                            }
                                        } else {
                                            // Current adv : is not a token
                                            if(DEBUG_OneAdvOnly)    console.log("       => (4.2) New is token, Current is not token => CLOSE");
                                            listeFeuillesAdv[cpt].close();
                                        }
                                    } else {
                                        // New adv : is not a token
                                        if(currentIsToken) {
                                            // Current adv : is a token
                                            if(DEBUG_OneAdvOnly)    console.log("       => (4.3) New is not token, Current is token => CLOSE");
                                            listeFeuillesAdv[cpt].close();
                                        } else {
                                            // Current adv : is not a token
                                            let currentPrototypeTokenName = currentAdvToken.name;
                                            let newPrototypeTokenNAme = newAdvToken.name;
                                            if(currentPrototypeTokenName === newPrototypeTokenNAme) {
                                                if(DEBUG_OneAdvOnly)    console.log("       => (4.4.1) New is not token, Current is not token => same prototype token name => DON'T CLOSE");
                                            } else {
                                                if(DEBUG_OneAdvOnly)    console.log("       => (4.4.2) New is not token, Current is not token => different prototype token name => CLOSE");
                                                listeFeuillesAdv[cpt].close();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // AFFICHER FICHE ADV A LA POSITION PARAMETREE
                    let setting_AdvSheetSamePosition = game.settings.get("tor-2e-macros", "adv_sheet_SamePosition");
                    let echelle = game.settings.get("tor-2e-macros", "adv_sheet_Echelle");
                    if (setting_AdvSheetSamePosition) {
                        let posLeft = game.settings.get("tor-2e-macros", "adv_sheet_PosLeft");
                        let posTop = game.settings.get("tor-2e-macros", "adv_sheet_PosTop");
                        let largeur = game.settings.get("tor-2e-macros", "adv_sheet_Largeur");
                        let hauteur = game.settings.get("tor-2e-macros", "adv_sheet_Hauteur");
                        object.actor.sheet.setPosition({ left: posLeft, top: posTop, width: largeur, height: hauteur });
                    }
                    if( echelle != 1.0) {
                        object.actor.sheet.setPosition({ scale: echelle });
                    }
                    object.actor.sheet.render(false);
                }
            }
        });
    }
}

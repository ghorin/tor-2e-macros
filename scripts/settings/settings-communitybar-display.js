export class SettingsCommunitybarDisplay extends FormApplication {
    constructor(object, options = {}) {
        super(object, options);
    }

    /**
    * Default Options for this FormApplication
    */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: "communitybar-settings-display-form",
            title: "Communitybar Display settings",
            template: "./modules/tor-2e-macros/html/settings/communitybar-settings-display.html",
            width: 500,
            closeOnSubmit: true
        });
    }


    getData() {
        // Size of the Communitybar tokens 
        let communityBarTokenHeightValue = game.settings.get("tor-2e-macros", "Community-Bar-Token-Height");
        let myValueLarge = "";
        let myValueMedium = "";
        let myValueSmall = "";
        let myValueTiny = "";
        if (communityBarTokenHeightValue === "large") {
            myValueLarge = "selected";
        } else if (communityBarTokenHeightValue === "medium") {
            myValueMedium = "selected";
        } else if (communityBarTokenHeightValue === "small") {
            myValueSmall = "selected";
        } else if (communityBarTokenHeightValue === "tiny") {
            myValueTiny = "selected";
        }

        // Display mode Horizontal or Vertical
        let communityBarDisplayModeValue = game.settings.get("tor-2e-macros", "Community-Bar-Display-Mode");
        let myValueDisplayModeHorizontal = "";
        let myValueDisplayModeVertical = "";
        if (communityBarDisplayModeValue === "horizontal") {
            myValueDisplayModeHorizontal = "selected";
        } else if (communityBarDisplayModeValue === "vertical") {
            myValueDisplayModeVertical = "selected";
        }

        // Display of Tooltip : gauche / droite / haut / bas
        let communityBarDisplayTooltipPositionValue = game.settings.get("tor-2e-macros", "Community-Bar-Display-Tooltip-Position");
        let myValueDisplayTooltipPositionGauche = "";
        let myValueDisplayTooltipPositionDroite = "";
        let myValueDisplayTooltipPositionHaut = "";
        let myValueDisplayTooltipPositionBas = "";
        if (communityBarDisplayTooltipPositionValue === "LEFT") {
            myValueDisplayTooltipPositionGauche = "selected";
        } else if (communityBarDisplayTooltipPositionValue === "RIGHT") {
            myValueDisplayTooltipPositionDroite = "selected";
        } else if (communityBarDisplayTooltipPositionValue === "UP") {
            myValueDisplayTooltipPositionHaut = "selected";
        } else if (communityBarDisplayTooltipPositionValue === "DOWN") {
            myValueDisplayTooltipPositionBas = "selected";
        }

        let data = {
            valueLarge: myValueLarge,
            valueMedium: myValueMedium,
            valueSmall: myValueSmall,
            valueTiny: myValueTiny,
            displayModeHorizontal: myValueDisplayModeHorizontal,
            displayModeVertical: myValueDisplayModeVertical,
            displayTooltipPositionGauche: myValueDisplayTooltipPositionGauche,
            displayTooltipPositionDroite: myValueDisplayTooltipPositionDroite,
            displayTooltipPositionHaut: myValueDisplayTooltipPositionHaut,
            displayTooltipPositionBas: myValueDisplayTooltipPositionBas
        };
        this.render;

        return data;
    }

    _updateObject(event, formData) {
        this.updateSettings(formData.token_height, formData.display_mode, formData.display_tooltip_position);
        this.render();
    }

    async updateSettings(token_height_value, display_mode_value, display_tooltip_position_value) {
        await game.settings.set("tor-2e-macros", "Community-Bar-Token-Height", token_height_value);
        await game.settings.set("tor-2e-macros", "Community-Bar-Display-Mode", display_mode_value);
        await game.settings.set("tor-2e-macros", "Community-Bar-Display-Tooltip-Position", display_tooltip_position_value);

        let settingActivateCommunitybar = game.settings.get("tor-2e-macros", "Community-Bar-Activate");
        if(settingActivateCommunitybar) {
            game.tor2eMacros.communitybar.refresh_communitybar();
        }        
    }
}
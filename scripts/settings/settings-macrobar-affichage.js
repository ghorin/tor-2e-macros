import { MacroUtil } from "../utils.js";

export class SettingsMacrobarAffichage extends FormApplication {
    constructor(object, options = {}) {
        super(object, options);
    }

    /**
    * Default Options for this FormApplication
    */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: "macrobar-settings-affichage",
            title: "Macrobar Display settings",
            template: "./modules/tor-2e-macros/html/settings/macrobar-settings-affichage.html",
            width: 500,
            closeOnSubmit: true
        });
    }

    getData() {
        // TOR2e Macrobar
        let macrobarActivateValue = game.settings.get("tor-2e-macros", "Macro-Bar-Activate");
        let macrobarActivateValueToSend = "";
        if (macrobarActivateValue) {
            macrobarActivateValueToSend = "checked";
        }

        // FOUNDRY Hotbar
        let foundrybarActivateValue = game.settings.get("tor-2e-macros", "Foundry-Bar-Activate");
        let foundrybarActivateValueToSend = "";
        if (foundrybarActivateValue) {
            foundrybarActivateValueToSend = "checked";
        }

        // TOR2e Communitybar
        let communitybarActivateValue = game.settings.get("tor-2e-macros", "Community-Bar-Activate");
        let communitybarActivateValueToSend = "";
        if (communitybarActivateValue) {
            communitybarActivateValueToSend = "checked";
        }

        // Return data
        let data = {
            macrobarActivate: macrobarActivateValueToSend,
            foundrybarActivate: foundrybarActivateValueToSend,
            communitybarActivate: communitybarActivateValueToSend,
            macrobarPosLeft: game.settings.get("tor-2e-macros", "Macro-Bar-PosLeft"),
            macrobarPosTop: game.settings.get("tor-2e-macros", "Macro-Bar-PosTop"),
            communityPosLeft: game.settings.get("tor-2e-macros", "Community-Bar-PosLeft"),
            communityPosTop: game.settings.get("tor-2e-macros", "Community-Bar-PosTop"),
        };
        this.render;
        return data;
    }

    _updateObject(event, formData) {
        let macrobarActivate_old = !!game.settings.get("tor-2e-macros", "Macro-Bar-Activate");
        let foundrybarActivate_old = !!game.settings.get("tor-2e-macros", "Foundry-Bar-Activate");
        let communitybarActivate_old = !!game.settings.get("tor-2e-macros", "Community-Bar-Activate");

        let macrobarActivate_new = !!formData.macrobarActivate;
        let foundrybarActivate_new = !!formData.foundrybarActivate;
        let communitybarActivate_new = !!formData.communitybarActivate;

        let anyChange = false;

        if (macrobarActivate_old != macrobarActivate_new) {
            anyChange = true;
        }
        if (foundrybarActivate_old != foundrybarActivate_new) {
            anyChange = true;
        }
        if (communitybarActivate_old != communitybarActivate_new) {
            anyChange = true;
        }

        this.render();

        if (anyChange) {
            game.settings.set("tor-2e-macros", "Macro-Bar-Activate", formData.macrobarActivate);
            game.settings.set("tor-2e-macros", "Foundry-Bar-Activate", formData.foundrybarActivate);
            game.settings.set("tor-2e-macros", "Community-Bar-Activate", formData.communitybarActivate);

            let macroUtil = new MacroUtil("macrobar-settings");
            Dialog.confirm({
                title: macroUtil.getTraduction("reload-titre"),
                content: macroUtil.getTraduction("reload-question"),
                yes: () => { foundry.utils.debouncedReload(); },
                no: () => { console.log("Reload to be done later.") },
                defaultYes: false
            });

        }
    }
}
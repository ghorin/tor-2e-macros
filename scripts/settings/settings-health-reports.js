export class SettingsHealthReports extends FormApplication {
    constructor(object, options = {}) {
        super(object, options);
    }

    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: "health_reports_settings",
            title: "Reports settings",
            template: "./modules/tor-2e-macros/html/settings/settings-health-reports.html",
            width: 650,
            height: 450,
            closeOnSubmit: true
        });
    }

    getData() {
        // Setting : Use image instead of the character name (name is the default)
        let reports_style_characterValue = game.settings.get("tor-2e-macros", "health_reports_style_character");
        let reports_style_characterValueToSend = "";
        if (reports_style_characterValue) {
            reports_style_characterValueToSend = "checked";
        }

        // Setting : Vertical bar for total Load
        let reports_vertical_barValue = game.settings.get("tor-2e-macros", "health_reports_vertical_bar");
        let reports_vertical_barValueToSend = "";
        if (reports_vertical_barValue) {
            reports_vertical_barValueToSend = "checked";
        }

        // Setting : Report Theme
        let reports_themeValue = game.settings.get("tor-2e-macros", "health_reports_theme");
        let reports_themeValueToSend = "";
        if (reports_themeValue) {
            reports_themeValueToSend = "checked";
        }

        // Return data
        let data = {
            health_reports_style_character: reports_style_characterValueToSend,
            health_reports_vertical_bar: reports_vertical_barValueToSend,
            health_reports_theme: reports_themeValueToSend
        };
        this.render;
        return data;
    }
    
    _updateObject(event, formData) {
        game.settings.set("tor-2e-macros", "health_reports_style_character", formData.health_reports_style_character);
        game.settings.set("tor-2e-macros", "health_reports_theme", formData.health_reports_theme);
        this.updateSettings(formData.health_reports_vertical_bar);
        this.render();
    }

    async updateSettings(myValue) {
        await game.settings.set("tor-2e-macros", "health_reports_vertical_bar", myValue);
        let settingActivateCommunitybar = game.settings.get("tor-2e-macros", "Community-Bar-Activate");
        if(settingActivateCommunitybar) {
            game.tor2eMacros.communitybar.refresh_communitybar();
        }
    }
}
import { MacroUtil } from "../utils.js";
import { SettingsMacrobarAffichage } from "./settings-macrobar-affichage.js";
import { SettingsMacrobarContenu } from "./settings-macrobar-contenu.js";
import { SettingsHealthReports } from "./settings-health-reports.js";
import { SettingsAdvSheets } from "./settings-adv-sheets.js";
import { SettingsCommunitybarContenu } from "./settings-communitybar-contenu.js";
import { SettingsCommunitybarDisplay } from "./settings-communitybar-display.js";



export class MacroSettings {
    /*
    game.setting    scope           world       Valeur identique pour tous les joueurs (yc MJ) du monde, non modifiable pour les joueurs
                                    client      Valeur définie au niveau du navigateur, chaque joueur pour avoir un réglage différent
                    config          true        Le paramètre apparait dans l'écran des paramètres
                                    false       Le paramètre n'apparait pas dans l'écran des paramètres. Note: le mettre dans un sous-menu pour le faire apparaitre au MJ 
    registerMenu    restricted      true        Seul le MJ voit le sous-menu et peut y accéder
                                    false       Tous les joueurs voient le sous-menu et peuvent y accéder
    */

    constructor() {
        // SETTINGS MACROBAR
        this.macrobar_options();
        this.groupBar_options();

        // SETTINGS COMMUNITYBAR
        this.communitybar_options();

        // SETTINGS EYE OF MORDOR
        this.community_options();

        // SETTINGS REPORTING
        this.reports_options();

        // SETTINGS ADVERSARY SHEETS
        this.adv_sheets_options();

        // SETTINGS MODULE
        game.settings.register('tor-2e-macros', 'last_read_version', {
            name: "Last used version of TOR2E Macros & Macrobar module",
            config: false,
            scope: 'world',
            type: String,
            default: "0.0.0"
        });
    }

    // ===================================================
    // OPTIONS FOR COMMUNITYBAR
    // ===================================================
    communitybar_options() {
        let macroUtil = new MacroUtil("communitybar-settings");

        // TODO Add translation
        // TODO change top/left settings to hidden (false)

        // -------------------------------------------------------------
        // CONTENTS options
        // -------------------------------------------------------------
        game.settings.register('tor-2e-macros', 'Community-Bar-Activate', {
            name: 'Activate TOR2e Community Bar',
            config: false,
            scope: 'world',
            type: Boolean,
            default: true
        });
        game.settings.register('tor-2e-macros', 'Community-Bar-PosLeft', {
            name: '... Horizontal position (pixels from left)',
            config: false,
            scope: 'client',
            type: Number,
            default: 50
        });
        game.settings.register('tor-2e-macros', 'Community-Bar-PosTop', {
            name: '... Vertical position (pixels from top)',
            config: false,
            scope: 'client',
            type: Number,
            default: 300
        });
        // Visibilité de la barre pour les joueurs
        game.settings.register("tor-2e-macros", "Community-Bar-Players-Can-See", {
            name: macroUtil.getTraduction("players-can-see"),
            hint: macroUtil.getTraduction("players-can-see-hint"),
            config: false,
            scope: 'world',
            type: Boolean,
            default: "true"
        });
        // Niveau de détails de la barre pour les joueurs
        game.settings.register("tor-2e-macros", "Community-Bar-Players-Details", {
            name: macroUtil.getTraduction("players-details"),
            hint: macroUtil.getTraduction("players-details-hint"),
            config: false,
            scope: 'world',
            type: Boolean,
            default: "false"
        });

        // "tor-2e-macros.communitybar-settings-contenu.bouton-options-contenu": "Community bar contents",
        game.settings.registerMenu("tor-2e-macros", "TOR2e Community Bar Contents", {
            name: "",
            label: macroUtil.getTraduction("bouton-options-contenu"),
            icon: "fas fa-bars",
            type: SettingsCommunitybarContenu,
            restricted: true                 // Restrict this submenu to gamemaster only?
        });



      

        // -------------------------------------------------------------
        // DISPLAY options (LM & Players if bar activated for players)
        // -------------------------------------------------------------
        let players_can_see_community_bar = game.settings.get("tor-2e-macros", "Community-Bar-Players-Can-See");
        if (game.user.isGM || players_can_see_community_bar) {
            game.settings.register('tor-2e-macros', 'Community-Bar-Token-Height', {
                name: macroUtil.getTraduction("token-height"),
                hint: macroUtil.getTraduction("token-height-hint"),
                config: false,
                type: String,
                scope: 'client',
                default: "large"
            });
            game.settings.register('tor-2e-macros', 'Community-Bar-Display-Mode', {
                name: macroUtil.getTraduction("display-mode"),
                hint: macroUtil.getTraduction("display-mode-hint"),
                config: false,
                type: String,
                scope: 'client',
                default: 'horizontal'
            });

            game.settings.register('tor-2e-macros', 'Community-Bar-Display-Tooltip-Position', {
                name: macroUtil.getTraduction("display-tooltip-position"),
                hint: macroUtil.getTraduction("display-tooltip-position-hint"),
                config: false,
                type: String,
                scope: 'client',
                default: 'DOWN'
            });

            let myMenuName = "";
            if(!game.user.isGM) {
                myMenuName = "Community bar";
            }
            game.settings.registerMenu("tor-2e-macros", "TOR2e Community Bar Display mode", {
                name: myMenuName,
                label: macroUtil.getTraduction("bouton-options-display"),
                icon: "fas fa-bars",
                type: SettingsCommunitybarDisplay,
                restricted: false                 // Restrict this submenu to gamemaster only?
            });                

        }
    }

    async refresh_communitybar() {
        await game.tor2eMacros.communitybar.refresh_communitybar();
    }

    adv_sheets_options() {
        let macroUtil = new MacroUtil("adversary-sheets-settings");

        // Settings visible in the Adversary Sheets settings screen
        game.settings.register('tor-2e-macros', 'adv_sheet_Echelle', {
            name: 'Default scale of adversary sheets',
            config: false,
            scope: 'world',
            type: Number,
            range: {
                min: 0.1,
                max: 2.0,
                step: 0.05
            },
            default: 1.0
        });
        game.settings.register('tor-2e-macros', 'adv_sheet_OnlyOne', {
            name: "Display only 1 Adversary sheet at same time",
            hint: "If activated, when you open an Adversary sheet, all other Adversary sheets will be closed. Clic on ALT-O in order to switch this setting.",
            config: false,
            scope: 'world',
            type: Boolean,
            default: false
        });
        game.settings.register('tor-2e-macros', 'adv_sheet_SamePosition', {
            name: "Open all Adversary sheets at same position",
            hint: "If activated, when you open an Adversary sheet, it will open at the same position than previously configured. Clic on ALT-P in order to switch this setting. Clic on SHIFT-ALT-P on an Adversary sheet in order to save its window configuration as new default for adversary sheets.",
            config: false,
            scope: 'world',
            type: Boolean,
            default: false
        });
        // Settings hidden
        game.settings.register('tor-2e-macros', 'adv_sheet_PosLeft', {
            name: 'Default LEFT position of adversary sheets',
            config: false,
            scope: 'world',
            type: Number,
            default: 50
        });
        game.settings.register('tor-2e-macros', 'adv_sheet_PosTop', {
            name: 'Default TOP position of adversary sheets',
            config: false,
            scope: 'world',
            type: Number,
            default: 50
        });
        game.settings.register('tor-2e-macros', 'adv_sheet_Largeur', {
            name: 'Default WIDTH of adversary sheets',
            config: false,
            scope: 'world',
            type: Number,
            default: 700
        });
        game.settings.register('tor-2e-macros', 'adv_sheet_Hauteur', {
            name: 'Default HEIGHT of adversary sheets',
            config: false,
            scope: 'world',
            type: Number,
            default: 700
        });
        game.settings.registerMenu("tor-2e-macros", "Adversary sheets", {
            name: macroUtil.getTraduction("titre-settings"),
            label: macroUtil.getTraduction("bouton-options-adv-sheets"),
            icon: "fas fa-bars",
            type: SettingsAdvSheets,
            restricted: true                 // Restrict this submenu to gamemaster only?
        });
    }

    reports_options() {
        let macroUtil = new MacroUtil("health-reports-settings");

        game.settings.register('tor-2e-macros', 'health_reports_style_character', {
            name: macroUtil.getTraduction("style-character-title"),
            config: false,
            scope: 'world',
            type: Boolean,
            default: "false"
        });
        game.settings.register('tor-2e-macros', 'health_reports_vertical_bar', {
            name: macroUtil.getTraduction("vertical-bar"),
            config: false,
            scope: 'world',
            type: Boolean,
            default: false
        });
        game.settings.register('tor-2e-macros', 'health_reports_theme', {
            name: macroUtil.getTraduction("theme"),
            config: false,
            scope: 'world',
            type: Boolean,
            default: false
        });
        game.settings.registerMenu("tor-2e-macros", "Health Reports", {
            name: macroUtil.getTraduction("titre-settings"),
            label: macroUtil.getTraduction("bouton-options-reports"),
            icon: "fas fa-bars",
            type: SettingsHealthReports,
            restricted: true                 // Restrict this submenu to gamemaster only?
        });
    }

    groupBar_options() {
        game.settings.register('tor-2e-macros', 'groupBar-tokens', {
            name: 'Display group bar tokens',
            config: false,
            scope: 'world',
            type: Boolean,
            default: true
        });
        game.settings.register('tor-2e-macros', 'groupBar-message', {
            name: 'Display group bar Message',
            config: false,
            scope: 'world',
            type: Boolean,
            default: true
        });
        game.settings.register('tor-2e-macros', 'groupBar-oeil', {
            name: 'Display group bar Oeil',
            config: false,
            scope: 'world',
            type: Boolean,
            default: true
        });
        game.settings.register('tor-2e-macros', 'groupBar-infos', {
            name: 'Display group bar Infos',
            config: false,
            scope: 'world',
            type: Boolean,
            default: true
        });
        game.settings.register('tor-2e-macros', 'groupBar-sante', {
            name: 'Display group bar Santé',
            config: false,
            scope: 'world',
            type: Boolean,
            default: true
        });
        game.settings.register('tor-2e-macros', 'groupBar-des', {
            name: 'Display group bar Dés',
            config: false,
            scope: 'world',
            type: Boolean,
            default: true
        });
        game.settings.register('tor-2e-macros', 'groupBar-pjs', {
            name: 'Display group bar PJs',
            config: false,
            scope: 'world',
            type: Boolean,
            default: true
        });
    }

    // ===================================================
    // OPTIONS FOR MACROBAR 
    // ===================================================
    macrobar_options() {
        let macroUtil = new MacroUtil("macrobar-settings");

        // OPTIONS D'AFFICHAGE
        game.settings.register('tor-2e-macros', 'Macro-Bar-Activate', {
            name: 'Activate TOR2e Macro Bar',
            config: false,
            scope: 'world',
            type: Boolean,
            default: true
        });
        game.settings.register('tor-2e-macros', 'Foundry-Bar-Activate', {
            name: 'Activate Foundry Hotbar',
            config: false,
            scope: 'world',
            type: Boolean,
            default: true
        });
        game.settings.register('tor-2e-macros', 'Macro-Bar-PosLeft', {
            name: '... Horizontal position (pixels from left)',
            config: false,
            scope: 'world',
            type: Number,
            default: 50
        });
        game.settings.register('tor-2e-macros', 'Macro-Bar-PosTop', {
            name: '... Vertical position (pixels from top)',
            config: false,
            scope: 'world',
            type: Number,
            default: 900
        });

        game.settings.registerMenu("tor-2e-macros", "TOR2e Macro Bar Display", {
            name: macroUtil.getTraduction("titre-settings"),
            label: macroUtil.getTraduction("bouton-options-affichage"),
            icon: "fas fa-bars",
            type: SettingsMacrobarAffichage,
            restricted: true                 // Restrict this submenu to gamemaster only?
        });


        // OPTIONS DE CONTENU
        game.settings.register('tor-2e-macros', 'Macro-Bar-Tokens', {
            name: 'Display Tokens macros',
            config: false,
            scope: 'world',
            type: Boolean,
            default: true,

        });
        game.settings.register('tor-2e-macros', 'Macro-Bar-Messages', {
            name: 'Display Messages macros',
            config: false,
            scope: 'world',
            type: Boolean,
            default: true,

        });
        game.settings.register('tor-2e-macros', 'Macro-Bar-Eye', {
            name: 'Display Eye macros',
            config: false,
            scope: 'world',
            type: Boolean,
            default: true,

        });
        game.settings.register('tor-2e-macros', 'Macro-Bar-Reports', {
            name: 'Display Reports macros',
            config: false,
            scope: 'world',
            type: Boolean,
            default: true,

        });
        game.settings.register('tor-2e-macros', 'Macro-Bar-Health', {
            name: 'Display Health macros',
            config: false,
            scope: 'world',
            type: Boolean,
            default: true,

        });
        game.settings.register('tor-2e-macros', 'Macro-Bar-DiceRolls', {
            name: 'Display Dice macros',
            config: false,
            scope: 'world',
            type: Boolean,
            default: true,

        });
        game.settings.register('tor-2e-macros', 'Macro-Bar-Characters', {
            name: 'Display Characters macros',
            config: false,
            scope: 'world',
            type: Boolean,
            default: true,

        });

        game.settings.registerMenu("tor-2e-macros", "TOR2e Macro Bar Contents", {
            name: "",
            label: macroUtil.getTraduction("bouton-options-contenu"),
            icon: "fas fa-bars",
            type: SettingsMacrobarContenu,
            restricted: true                 // Restrict this submenu to gamemaster only?
        });
    }

    // ===================================================
    // OPTIONS FOR COMMUNITY and EYE AWARENESS 
    // ===================================================
    community_options() {
        let macroUtil = new MacroUtil("community_EyeAwareness");

        // Visibilité du Eye Awareness
        game.settings.register("tor-2e-macros", "Eye awareness visibility", {
            name: macroUtil.getTraduction("trad_SettingVisibility"),
            hint: macroUtil.getTraduction("trad_SettingVisibility_hint"),
            config: true,
            scope: 'world',
            type: String,
            choices: {
                "all": macroUtil.getTraduction("trad_SettingVisibility_All"),
                "loremaster": macroUtil.getTraduction("trad_SettingVisibility_LM")
            },
            default: "all"
        });
    }
}

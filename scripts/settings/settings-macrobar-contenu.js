export class SettingsMacrobarContenu extends FormApplication {
    constructor(object, options = {}) {
        super(object, options);
    }

    /**
    * Default Options for this FormApplication
    */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: "macrobar-settings-form",
            title: "Macrobar Contents settings",
            template: "./modules/tor-2e-macros/html/settings/macrobar-settings-contenu.html",
            width: 500,
            closeOnSubmit: true
        });
    }

    getData() {
        // Tokens
        let macrobarGroupTokensValue = game.settings.get("tor-2e-macros", "Macro-Bar-Tokens");
        let macrobarGroupTokensValueToSend = "";
        if( macrobarGroupTokensValue ) {
            macrobarGroupTokensValueToSend = "checked";
        }

        // Messages
        let macrobarGroupMessagesValue = game.settings.get("tor-2e-macros", "Macro-Bar-Messages");
        let macrobarGroupMessagesValueToSend = "";
        if( macrobarGroupMessagesValue ) {
            macrobarGroupMessagesValueToSend = "checked";
        }

        // Eye
        let macrobarGroupEyeValue = game.settings.get("tor-2e-macros", "Macro-Bar-Eye");
        let macrobarGroupEyeValueToSend = "";
        if( macrobarGroupEyeValue ) {
            macrobarGroupEyeValueToSend = "checked";
        }
        
        // Reports
        let macrobarGroupReportsValue = game.settings.get("tor-2e-macros", "Macro-Bar-Reports");
        let macrobarGroupReportsValueToSend = "";
        if( macrobarGroupReportsValue ) {
            macrobarGroupReportsValueToSend = "checked";
        }

        // Health
        let macrobarGroupHealthValue = game.settings.get("tor-2e-macros", "Macro-Bar-Health");
        let macrobarGroupHealthValueToSend = "";
        if( macrobarGroupHealthValue ) {
            macrobarGroupHealthValueToSend = "checked";
        }

        // Dice Rolls
        let macrobarGroupDiceRollsValue = game.settings.get("tor-2e-macros", "Macro-Bar-DiceRolls");
        let macrobarGroupDiceRollsValueToSend = "";
        if( macrobarGroupDiceRollsValue ) {
            macrobarGroupDiceRollsValueToSend = "checked";
        }

        // Characters
        let macrobarGroupCharactersValue = game.settings.get("tor-2e-macros", "Macro-Bar-Characters");
        let macrobarGroupCharactersValueToSend = "";
        if( macrobarGroupCharactersValue ) {
            macrobarGroupCharactersValueToSend = "checked";
        }
        
        // Return data
        let data = {
            macrobarGroupTokens: macrobarGroupTokensValueToSend,
            macrobarGroupMessages: macrobarGroupMessagesValueToSend,
            macrobarGroupEye: macrobarGroupEyeValueToSend,
            macrobarGroupReports: macrobarGroupReportsValueToSend,
            macrobarGroupHealth: macrobarGroupHealthValueToSend,
            macrobarGroupDiceRolls: macrobarGroupDiceRollsValueToSend,
            macrobarGroupCharacters: macrobarGroupCharactersValueToSend,
        };
        this.render;
        return data;
    }

    _updateObject(event, formData) {
        this.majSettings(formData);
        this.render();
    }

    async majSettings(formData) {
        let nb_change = 0;

        let macrobarGroupTokensValue = await game.settings.get("tor-2e-macros", "Macro-Bar-Tokens");
        let macrobarGroupMessagesValue = await game.settings.get("tor-2e-macros", "Macro-Bar-Messages");
        let macrobarGroupEyeValue = await game.settings.get("tor-2e-macros", "Macro-Bar-Eye");
        let macrobarGroupReportsValue = await game.settings.get("tor-2e-macros", "Macro-Bar-Reports");
        let macrobarGroupHealthValue = await game.settings.get("tor-2e-macros", "Macro-Bar-Health");
        let macrobarGroupDiceRollsValue = await game.settings.get("tor-2e-macros", "Macro-Bar-DiceRolls");
        let macrobarGroupCharactersValue = await game.settings.get("tor-2e-macros", "Macro-Bar-Characters");
        
        if(macrobarGroupTokensValue != formData.macrobarGroupTokens )   nb_change = nb_change + 1;
        if(macrobarGroupMessagesValue != formData.macrobarGroupMessages )   nb_change = nb_change + 1;
        if(macrobarGroupEyeValue != formData.macrobarGroupEye )   nb_change = nb_change + 1;
        if(macrobarGroupReportsValue != formData.macrobarGroupReports )   nb_change = nb_change + 1;
        if(macrobarGroupHealthValue != formData.macrobarGroupHealth )   nb_change = nb_change + 1;
        if(macrobarGroupDiceRollsValue != formData.macrobarGroupDiceRolls )   nb_change = nb_change + 1;
        if(macrobarGroupCharactersValue != formData.macrobarGroupCharacters )   nb_change = nb_change + 1;

        await game.settings.set("tor-2e-macros", "Macro-Bar-Tokens", formData.macrobarGroupTokens);
        await game.settings.set("tor-2e-macros", "Macro-Bar-Messages", formData.macrobarGroupMessages);
        await game.settings.set("tor-2e-macros", "Macro-Bar-Eye", formData.macrobarGroupEye);
        await game.settings.set("tor-2e-macros", "Macro-Bar-Reports", formData.macrobarGroupReports);
        await game.settings.set("tor-2e-macros", "Macro-Bar-Health", formData.macrobarGroupHealth);
        await game.settings.set("tor-2e-macros", "Macro-Bar-DiceRolls", formData.macrobarGroupDiceRolls);
        await game.settings.set("tor-2e-macros", "Macro-Bar-Characters", formData.macrobarGroupCharacters);
        if(nb_change > 0) {
            let settingActivateMacrobar = game.settings.get("tor-2e-macros", "Macro-Bar-Activate");
            if (settingActivateMacrobar) {
                game.tor2eMacros.macrobar.refresh_macrobar();
            }
        }
    }
}
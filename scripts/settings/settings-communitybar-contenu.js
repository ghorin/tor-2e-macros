export class SettingsCommunitybarContenu extends FormApplication {
    constructor(object, options = {}) {
        super(object, options);
    }

    /**
    * Default Options for this FormApplication
    */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: "communitybar-settings-form",
            title: "Communitybar Contents settings",
            template: "./modules/tor-2e-macros/html/settings/communitybar-settings-contenu.html",
            width: 500,
            closeOnSubmit: true
        });
    }

    getData() {
        // Display the Community bar to the Players 
        let communityBarPlayersCanSeeValue = game.settings.get("tor-2e-macros", "Community-Bar-Players-Can-See");
        let communityBarPlayersCanSeeValueToSend = "";
        if( communityBarPlayersCanSeeValue ) {
            communityBarPlayersCanSeeValueToSend = "checked";
        }

        // Messages
        let communityBarPlayersDetailsValue = game.settings.get("tor-2e-macros", "Community-Bar-Players-Details");
        let communityBarPlayersDetailsValueToSend = "";
        if( communityBarPlayersDetailsValue ) {
            communityBarPlayersDetailsValueToSend = "checked";
        }

        
        // Return data
        let data = {
            players_can_see: communityBarPlayersCanSeeValueToSend,
            players_details: communityBarPlayersDetailsValueToSend,
        };
        this.render;

        return data;
    }

    _updateObject(event, formData) {
        console.log("players_can_see="+formData.players_can_see);
        console.log("players_details="+formData.players_details);
        game.settings.set("tor-2e-macros", "Community-Bar-Players-Can-See", formData.players_can_see);
        game.settings.set("tor-2e-macros", "Community-Bar-Players-Details", formData.players_details);

        this.render();
    }

}
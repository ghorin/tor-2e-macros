export class SettingsAdvSheets extends FormApplication {
    constructor(object, options = {}) {
        super(object, options);
    }

    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: "adv_sheets_settings",
            title: "Adversary Sheets settings",
            template: "./modules/tor-2e-macros/html/settings/settings-adv-sheets.html",
            width: 650,
            height: 400,
            closeOnSubmit: true
        });
    }

    getData() {
        // Setting : Use image instead of the character name (name is the default)
        let adv_SheetEchelleValueToSend = game.settings.get("tor-2e-macros", "adv_sheet_Echelle");

        // Setting : Adversary sheet : allow displaying seveeral or only 1 at same time ?
        let adv_OnlyOneSheetValue = game.settings.get("tor-2e-macros", "adv_sheet_OnlyOne");
        let adv_OnlyOneSheetValueToSend = "";
        if (adv_OnlyOneSheetValue) {
            adv_OnlyOneSheetValueToSend = "checked";
        }

        // Setting : Report Theme
        let adv_SheetSamePositionValue = game.settings.get("tor-2e-macros", "adv_sheet_SamePosition");
        let adv_SheetSamePositionValueToSend = "";
        if (adv_SheetSamePositionValue) {
            adv_SheetSamePositionValueToSend = "checked";
        }

        // Return data
        let data = {
            adv_sheet_Echelle: adv_SheetEchelleValueToSend,
            adv_sheet_OnlyOne: adv_OnlyOneSheetValueToSend,
            adv_sheet_SamePosition: adv_SheetSamePositionValueToSend
        };
        this.render;
        return data;
    }

    _updateObject(event, formData) {
        game.settings.set("tor-2e-macros", "adv_sheet_Echelle", formData.adv_sheet_Echelle);
        game.settings.set("tor-2e-macros", "adv_sheet_OnlyOne", formData.adv_sheet_OnlyOne);
        game.settings.set("tor-2e-macros", "adv_sheet_SamePosition", formData.adv_sheet_SamePosition);
        this.render();
    }
}
// UTILITIES
import { MacroGlobals } from "./scripts/constants.js";
import { MacroUtil } from "./scripts/utils.js";

// MACROS
import { MacroChat } from "./scripts/macros/chat.js";
import { MacroToken } from "./scripts/macros/token.js";
import { MacroCharacter } from "./scripts/macros/character.js";
import { MacroCombat } from "./scripts/macros/combat.js";
import { MacroDev } from "./scripts/macros/dev.js";
import { MacroCommunity } from "./scripts/macros/community.js";
import { MacroRoll } from "./scripts/macros/rolls.js";
import { MacroMj } from "./scripts/macros/mj.js";
import { MacroSkills } from "./scripts/macros/skills.js";

// SETTINGS
import { MacroSettings } from "./scripts/settings/settings.js";

// BARS
import { Macrobar } from "./scripts/bars/macrobar.js";
import { Communitybar } from "./scripts/bars/communitybar.js";

// HOOKS
import { AdvSheets } from "./scripts/hooks/adv-sheets.js";

export class Tor2eMacros {
    constructor() {
        this.TOR2E_MACROS_DEBUG = false;

        // Settings
        this.settings = new MacroSettings();

        // Macros
        this.chat = new MacroChat();
        this.token = new MacroToken();
        this.characters = new MacroCharacter();
        this.combat = new MacroCombat();
        this.dev = new MacroDev();
        this.community = new MacroCommunity();
        this.roll = new MacroRoll();
        this.mj = new MacroMj();
        this.skills = new MacroSkills();

        // Bars
        let settingActivateCommunitybar = game.settings.get("tor-2e-macros", "Community-Bar-Activate");
        let settingPlayersCanSeeCommunitybar = game.settings.get("tor-2e-macros", "Community-Bar-Players-Can-See");
        if (game.user.isGM) {
            // Foundry bar
            let settingActivateFoundrybar = game.settings.get("tor-2e-macros", "Foundry-Bar-Activate");
            if (!settingActivateFoundrybar) {
                const rootStyle = document.querySelector(':root').style;
                rootStyle.setProperty('--macrobar-hotbar-visibility', 'hidden');
            } else {
                const rootStyle = document.querySelector(':root').style;
                rootStyle.setProperty('--macrobar-hotbar-visibility', 'visible');
            }

            // Macrobar
            let settingActivateMacrobar = game.settings.get("tor-2e-macros", "Macro-Bar-Activate");
            if (settingActivateMacrobar) {
                this.macrobar = new Macrobar();
            }

            // Communitybar
            if (settingActivateCommunitybar) {
                this.communitybar = new Communitybar();
            }

            // Hooks on Adversary sheets
            this.adv_sheets = new AdvSheets();
        } else {
            // Communitybar for players
            if (settingPlayersCanSeeCommunitybar) {
                this.communitybar = new Communitybar();
            }
        }
    }

    // ==========================================
    // SKILLS
    // ==========================================
    // Skill character roll : List the character skills and clic on one for roll it
    async skills_character_roll() {
        this.skills.skills_character_roll();
    }
    async skills_private_roll() {
        this.skills.skills_private_roll();
    }
    
    // ==========================================
    // MJ
    // ==========================================
    // Characters List : List all characters with their characteristics
    async mj_charactersSynthesis() {
        this.mj.mj_charactersSynthesis();
    }
    async listCharactersActiveEffects() {
        this.mj.listCharactersActiveEffects();
    }

    // ==========================================
    // ROLL
    // ==========================================
    // Manual Roll : Launch a manual roll by the player
    async roll_ManualRoll() {
        this.roll.roll_ManualRoll();
    }

    // ==========================================
    // COMMUNITY
    // ==========================================
    // Community : Eye Awareness => Add +1 to Eye Awareness
    async community_eyeAwareness() {
        this.community.community_eyeAwareness();
    }
    async community_huntLimit() {
        this.community.community_huntLimit();
    }

    // ==========================================
    // DEV
    // ==========================================
    // Dev : Suppression of actors or items
    async dev_Delete_all() {
        this.dev.dev_Delete_all();
    }
    // Dev : Debug an actor or Item data in the Console
    async dev_Debug() {
        this.dev.dev_Debug();
    }

    // ==========================================
    // COMBAT
    // ==========================================
    // Combat : Launch an attack for an Adversary
    async combat_lanceAttaque() {
        this.combat.combat_lanceAttaque();
    }

    // ==========================================
    // ADVERSARIES
    // ==========================================
    // Add 1pt of Hate to the selected adversary
    async adv_increment_hate() {
        await this.characters.adv_increment_hate();
    }
    // Remove 1pt of Hate to the selected adversary
    async adv_decrement_hate() {
        await this.characters.adv_decrement_hate();
    }

    // ==========================================
    // CHARACTERS
    // ==========================================
    // Characters Physical health
    async characters_List_Health_Physical() {
        this.characters.characters_List_Health_Physical();
    }
    // Characters Moral health
    async characters_List_Health_Moral() {
        this.characters.characters_List_Health_Moral();
    }
    // Characters Valour & Wisdom 
    async characters_List_Valour_Wisdom() {
        this.characters.characters_List_Valour_Wisdom();
    }
    // PJ sélectionné : Remettre la santé physique au max
    async characters_Set_EtatPhysiqueMax() {
        this.characters.characters_Set_EtatPhysiqueMax();
    }
    // Characters : Ajouter la progression (pts d'aventure et de compétences)
    async characters_Add_SessionProgression() {
        this.characters.characters_Add_SessionProgression();
    }
    // Characters : 1 day less for Wounds
    async characters_reduceWoundDuration() {
        this.characters.characters_reduceWoundDuration();
    }
    // Characters : Experience report
    async characters_List_Experience() {
        this.characters.characters_List_Experience();
    }

    

    // ==========================================
    // CHAT
    // ==========================================
    // Send a message in the chat
    async chat_SendMessage() {
        this.chat.sendMessage();
    }

    // Send a message in a bubble of the player character or of a token selected by the GM
    async chat_SendMessageBuble() {
        this.chat.sendMessageBuble();
    }

    // ==========================================
    // TOKENS
    // ==========================================    
    // Selected token(s) : set Dead or not over the token 
    async token_Set_DeadOrNot() {
        this.token.token_Set_DeadOrNot();
    }
    // Selected token(s) : set Unconscious or not over the token 
    async token_Set_UnconsciousOrNot() {
        this.token.token_Set_UnconsciousOrNot();
    }
    // Selected token(s) or tile(s) : hide or not
    async token_Set_Hidden_or_not() {
        this.token.token_Set_Hidden_or_not();
    }
    // Selected token(s) : Change size
    async token_Set_Size() {
        this.token.token_Set_Size();
    }
    // Token image display
    async token_DisplayTokenImage() {
        this.token.token_DisplayTokenImage();
    }

    // ==========================================
    // MODULE MANAGEMENT
    // ==========================================    
    async readme() {
        let macroUtil = new MacroUtil("general-module");
        const module = game.modules.get(MacroGlobals.MODULE_ID);
        let current_version = module.version;
        let last_read_version = game.settings.get('tor-2e-macros', 'last_read_version');
        if (!last_read_version) last_read_version = "0.0.0";
        if (current_version != last_read_version) {
            let currentLang = game.i18n.lang;
            let message = "";
            if (currentLang === 'fr') {
                message = `
                <form>
                    <h2>TOR2e Macros, Macrobar & Communitybar - Version ` + current_version + `</h2>
                    <p>
                        <ul>
                            <li><span style='color: darkred; font-weight: bold;'>Evolution</span><br>
                                - 12.0.4 : Quelques améliorationsvisuelles minime afin d'être compatible avec le module TOR2e Theme
                                - 12.0.5 : Ajout du lien de l'Attachement (= lien de communauté) en bulle d'aide sur les photos de la Communitybar
                            </li>
                        </ul><br>
                    </p>
                    <p>
                        <b><u>Documentation</u></b><br>
                        <ul>
                            <li><a href='http://ostolinde.free.fr/anneau_unique/aides/v2/TOR-FoundryVTT-FR.pdf'>Jouer à l'Anneau Unique 2e dans Foundry VTT</a>
                            <li><a href='https://gitlab.com/ghorin/tor-2e-macros/-/issues/new'>Soumettre un bug ou une suggestion</a>
                            <li><a href='https://gitlab.com/ghorin/tor-2e-macros/-/blob/main/CHANGELOG?ref_type=heads'>Notes de version complètes</a>
                        </ul>
                    </p>
                </form>`;
            } else {
                message = `
                    <form>
                        <h2>TOR2e Macros & Macrobar - Release ` + current_version + `</h2>
                        <p>
                            <ul>
                                <li><span style='color: darkred; font-weight: bold;'>Change</span><br>
                                    - 12.0.4 : Style adjustments for compatibility with TOR2e Theme module
                                    - 12.0.5 : Add the Attachment (= focus) as tooltip in the Communitybar
                                </li>
                            </ul><br>
                        </p>
                        <p>
                            <b><u>Documentation</u></b><br>
                            <ul>
                                <li><a href='http://ostolinde.free.fr/anneau_unique/aides/v2/TOR-FoundryVTT-EN.pdf'>Play The One Ring 2e in Foundry VTT</a>
                                <li><a href='https://gitlab.com/ghorin/tor-2e-macros/-/issues/new'>Reporting a new issue or suggestion</a>
                                <li><a href='https://gitlab.com/ghorin/tor-2e-macros/-/blob/main/CHANGELOG?ref_type=heads'>Full change log</a>
                            </ul>
                        </p>
                    </form>`;
            }

            new Dialog({
                title: MacroGlobals.MODULE_NAME,
                content: message,
                buttons: {
                    fermer: {
                        icon: "<i class='fa-solid fa-xmark'></i>",
                        label: macroUtil.getTraduction("fermer-re-afficher"),
                        callback: () => { },
                    },
                    lu: {
                        icon: "<i class='fa-solid fa-check'></i>",
                        label: macroUtil.getTraduction("lu-ne-plus-afficher"),
                        callback: () => { game.settings.set('tor-2e-macros', 'last_read_version', current_version); },
                    }
                },
                default: 'fermer',
                close: html => { },
            }, { width: 550, height: 400 }).render(true);
        }
    }
}

// ========================================================================================= 
//                  HOOKS on READY
// ========================================================================================= 
Hooks.on("ready", async function () {
    game.tor2eMacros = new Tor2eMacros();
    console.log("=== Tor2eMacros : Ready ===");
    if (game.user.isGM) {
        game.tor2eMacros.readme();
    }
});

// ========================================================================================= 
//                  HOOKS on ACTIVE EFFECTS
// ========================================================================================= 
// Update the Community bar (if activated) when an active-effect is removed
Hooks.on("deleteActiveEffect", (activeEffect, options, id) => {
    let settingActivateCommunitybar = game.settings.get("tor-2e-macros", "Community-Bar-Activate");
    if (settingActivateCommunitybar) {
        if (activeEffect.parent.type === "character") {
            let effectName = Array.from(activeEffect.statuses)[0];
            // note : on peut aussi tester le contenu du Set par : effect.statuses.has("weary") 
            if (effectName === "weary" || effectName === "poisoned" || effectName === "wounded" || effectName === "miserable") {
                game.tor2eMacros.communitybar.refresh_communitybar();
            }
        }
    }
});

// Update the Community bar (if activated) when an active-effect is added
Hooks.on("createActiveEffect", (activeEffect, options, id) => {
    let settingActivateCommunitybar = game.settings.get("tor-2e-macros", "Community-Bar-Activate");
    if (settingActivateCommunitybar) {
        if (activeEffect.parent.type === "character") {
            let effectName = Array.from(activeEffect.statuses)[0];
            if (effectName === "weary" || effectName === "poisoned" || effectName === "wounded" || effectName === "miserable") {
                game.tor2eMacros.communitybar.refresh_communitybar();
            }
        }
    }
});

// ========================================================================================= 
//                  HOOKS on ACTORS
// ========================================================================================= 
Hooks.on('updateActor', async function (actor, data, options, userID) {
    if (actor.type === "character") {
        // Change of the Endurance / Hope
        let settingActivateCommunitybar = game.settings.get("tor-2e-macros", "Community-Bar-Activate");
        if (settingActivateCommunitybar) {
            // ajouter la vérification que ce character est bien dans la feuille de communauté active
            let refrechCommunityBar = false;
            if (data.system?.resources?.endurance != undefined) refrechCommunityBar = true;             // current Endurance points 
            if (data.system?.resources?.endurance?.max != undefined) refrechCommunityBar = true;             // max Endurance points 
            if (data.system?.resources?.travelLoad?.value != undefined) refrechCommunityBar = true;             // current travel load points
            if (data.system?.resources?.hope != undefined) refrechCommunityBar = true;             // current Hope points
            if (data.system?.resources?.hope?.max != undefined) refrechCommunityBar = true;             // max Hope points
            if (data.system?.resources?.shadow?.temporary != undefined) refrechCommunityBar = true;             // current temporary shadow points 
            if (data.system?.resources?.shadow?.shadowScars?.value != undefined) refrechCommunityBar = true;             // shadow scars

            if (refrechCommunityBar) {
                game.tor2eMacros.communitybar.refresh_communitybar();
            }
        }
    } else if (actor.type === "community") {
        let settingActivateCommunitybar = game.settings.get("tor-2e-macros", "Community-Bar-Activate");
        let settingActivateMacrobar = game.settings.get("tor-2e-macros", "Macro-Bar-Activate");

        let idCurrentCommunity = game.settings.get("tor2e", "communityCurrentActor");
        if (idCurrentCommunity === actor.id) {
            if (settingActivateCommunitybar) {
                game.tor2eMacros.communitybar.refresh_communitybar();
            }
            if (settingActivateMacrobar) {
                game.tor2eMacros.macrobar.refresh_macrobar();
            }
        }
    }
});

// ========================================================================================= 
//                  HOOKS on ITEMS
// ========================================================================================= 
Hooks.on('updateItem', async function (item, data, options, userID) {
    // Detect if a weapon or armour was dropped / undropped from a character sheet => change of Total Load => impact on the vertical bar of Endurance bar in Community bar
    let settingActivateCommunitybar = game.settings.get("tor-2e-macros", "Community-Bar-Activate");
    let vertical_bar = game.settings.get("tor-2e-macros", "health_reports_vertical_bar");
    if (settingActivateCommunitybar && vertical_bar) {
        let refrechCommunityBar = false;
        if (data.system?.dropped != undefined) refrechCommunityBar = true;             // item was dropped or undropped 
        if (data.system?.load?.value != undefined) refrechCommunityBar = true;             // item Load value was changed 

        if (refrechCommunityBar) {
            if(item.actor != null) {
                let idActor = item.actor.id;
                if (idActor != null) {
                    let actorCommunity = game.tor2e.macro.utility.getCommunity();
                    if (actorCommunity != null && actorCommunity != '') {
                        let characterInActiveCommunity = false;
                        actorCommunity.system.members.forEach(a => {
                            if (a.id === idActor) {
                                // The Actor on which the Item was dropped or its load modified is inside the current active community => refresh the Community bar
                                game.tor2eMacros.communitybar.refresh_communitybar();
                            }
                        })
                    }
                }
            }
        }
    }
});
Hooks.on('deleteItem', async function (item, data, options, userID) {
    // detect if a war or non-war gear was drag-drop into a character sheet or was removed => change of Total Load => impact on the vertical bar of Endurance bar in Community bar
    let settingActivateCommunitybar = game.settings.get("tor-2e-macros", "Community-Bar-Activate");
    let vertical_bar = game.settings.get("tor-2e-macros", "health_reports_vertical_bar");
    if (settingActivateCommunitybar && vertical_bar) {
        if(item.actor != null) {
            let idActor = item.actor.id;
            if (idActor != null) {
                let actorCommunity = game.tor2e.macro.utility.getCommunity();
                if (actorCommunity != null && actorCommunity != '') {
                    let characterInActiveCommunity = false;
                    actorCommunity.system.members.forEach(a => {
                        if (a.id === idActor) {
                            // The Actor on which the Item was dropped is inside the current active community => refresh the Community bar
                            game.tor2eMacros.communitybar.refresh_communitybar();
                        }
                    })
                }
            }
        }
    }
});
Hooks.on('createItem', async function (item, data, options, userID) {
    // detect if a war or non-war gear was drag-drop into a character sheet or was removed => change of Total Load => impact on the vertical bar of Endurance bar in Community bar
    let settingActivateCommunitybar = game.settings.get("tor-2e-macros", "Community-Bar-Activate");
    let vertical_bar = game.settings.get("tor-2e-macros", "health_reports_vertical_bar");
    if (settingActivateCommunitybar && vertical_bar) {
        if(item.actor != null) {
            let idActor = item.actor.id;
            if (idActor != null) {
                let actorCommunity = game.tor2e.macro.utility.getCommunity();
                if (actorCommunity != null && actorCommunity != '') {
                    let characterInActiveCommunity = false;
                    actorCommunity.system.members.forEach(a => {
                        if (a.id === idActor) {
                            // The Actor on which the Item was dropped is inside the current active community => refresh the Community bar
                            game.tor2eMacros.communitybar.refresh_communitybar();
                        }
                    })
                }
            }
        }
    }
});


// ========================================================================================= 
//                  HOOKS on INIT : KEYBINDINGS
// ========================================================================================= 
Hooks.on("init", async function () {
    let macroUtil = new MacroUtil("adversary-sheets-settings");

    // KEYBINDING : ALT-P => Toggle Adversary sheet opening for position / size / scale : use Foundry/TOR2e default or Specific saved configuration
    game.keybindings.register('tor-2e-macros', 'Adv-SheetSamePosition', {
        name: 'Adversary sheets specifc conf',
        hint: 'Toggle setting for opening Adversay Sheets with default Foundry position & size or with a specific position & size (previously saved with ALT-SHIFT-P)',
        editable: [{
            key: 'KeyP',
            modifiers: ["Alt"]
        }],
        onDown: () => {
            if (game.user.isGM) {
                let currentSettingValue = game.settings.get('tor-2e-macros', 'adv_sheet_SamePosition');
                if (currentSettingValue) {
                    game.settings.set('tor-2e-macros', 'adv_sheet_SamePosition', false);
                    ui.notifications.info(macroUtil.getTraduction("adv_WindowsDefaultFoundryConf"));
                } else {
                    game.settings.set('tor-2e-macros', 'adv_sheet_SamePosition', true);
                    ui.notifications.info(macroUtil.getTraduction("adv_WindowsSpecificConf"));
                }
            }
        }
    });

    // KEYBINDING : ALT-SHIFT-P => Save the current adversary sheet position as new default position for adv sheets
    game.keybindings.register('tor-2e-macros', 'Adv-SaveDefaultSheetConf', {
        name: 'Save Adversary sheet configuration',
        hint: 'Save the position & size of the currently focused Adversary sheet. Will be used for next opening Adversary sheets (if ALT-P option is activated)',
        editable: [{
            key: 'KeyP',
            modifiers: ["Alt", "Shift"]
        }],
        onDown: () => {
            if (game.user.isGM) {
                let currentWindow = ui.activeWindow;
                if (currentWindow != null && currentWindow.object?.type === 'adversary') {
                    let posLeft = currentWindow.position.left;
                    game.settings.set("tor-2e-macros", "adv_sheet_PosLeft", posLeft);
                    let posTop = currentWindow.position.top;
                    game.settings.set("tor-2e-macros", "adv_sheet_PosTop", posTop);
                    let largeur = currentWindow.position.width;
                    game.settings.set("tor-2e-macros", "adv_sheet_Largeur", largeur);
                    let hauteur = currentWindow.position.height;
                    game.settings.set("tor-2e-macros", "adv_sheet_Hauteur", hauteur);
                    ui.notifications.info(macroUtil.getTraduction("adv_WindowsSaved"));
                } else {
                    ui.notifications.info(macroUtil.getTraduction("adv_WindowsSaved_NoAdvSelected"));
                }
            }
        }
    });

    // KEYBINDING : ALT-SHIFT-F => Close all opened Adversary sheets
    game.keybindings.register('tor-2e-macros', 'Adv-CloseAllSheets', {
        name: 'Close all Adversary sheets',
        editable: [{
            key: 'KeyF',
            modifiers: ["Alt", "Shift"]
        }],
        onDown: () => {
            if (game.user.isGM) {
                let listeFeuillesAdv = Object.values(ui.windows).filter(i => i.object?.type === 'adversary');
                for (let cpt = listeFeuillesAdv.length - 1; cpt > -1; cpt--) {
                    listeFeuillesAdv[cpt].close();
                }
            }
        }
    });

    // KEYBINDING : ALT-O => Toggle adversy sheets opening at same time : only 1 / several
    game.keybindings.register('tor-2e-macros', 'Adv-OnlyOneSheet', {
        name: 'Only 1 Adversary sheet',
        hint: 'Toogle setting for allowing several or only one Adversary sheet opened at same time',
        editable: [{
            key: 'KeyO',
            modifiers: ["Alt"]
        }],
        onDown: () => {
            if (game.user.isGM) {
                let currentSettingValue = game.settings.get('tor-2e-macros', 'adv_sheet_OnlyOne');
                if (currentSettingValue) {
                    game.settings.set('tor-2e-macros', 'adv_sheet_OnlyOne', false);
                    ui.notifications.info(macroUtil.getTraduction("adv_WindowsSeveral"));
                } else {
                    game.settings.set('tor-2e-macros', 'adv_sheet_OnlyOne', true);
                    ui.notifications.info(macroUtil.getTraduction("adv_WindowsOnlyOne"));
                }
            }
        }
    });

    // KEYBINDING : ALT-SHIFT-R => Reload the Macrobar and Communitybar
    game.keybindings.register('tor-2e-macros', 'Macrobar-Reload', {
        name: 'Close and restart the Macrobar',
        hint: 'Useful if the Macrobar has crashed or if you have added a new member to the Community and want the Macrobar to add a slot for that player-character',
        editable: [{
            key: 'KeyR',
            modifiers: ["Alt", "Shift"]
        }],
        onDown: () => {
            let settingActivateCommunitybar = game.settings.get("tor-2e-macros", "Community-Bar-Activate");
            let settingActivateMacrobar = game.settings.get("tor-2e-macros", "Macro-Bar-Activate");
            let settingPlayersCanSeeCommunitybar = game.settings.get("tor-2e-macros", "Community-Bar-Players-Can-See");
            if (game.user.isGM) {
                if (settingActivateMacrobar) {
                    game.tor2eMacros.macrobar.refresh_macrobar();
                }
                if (settingActivateCommunitybar) {
                    game.tor2eMacros.communitybar.refresh_communitybar();
                }
            } else {
                if (settingPlayersCanSeeCommunitybar) {
                    game.tor2eMacros.communitybar.refresh_communitybar();
                }
            }
        }
    });

});
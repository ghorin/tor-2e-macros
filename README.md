# tor-2e-macros

TOR2E - Unofficial macros and specifc hotbars (Macrobar & Communitybar) for The One Ring 2nd edition

## Compatibility with Foundry VTT
- Foundry v11
- Foundry v12

## Contents
This module provides 
    - several macros that are specific to The One Ring 2nd edition by Fria Ligans & Francesco Nepitello.
    - a specific hotbar called "Macrobar" that contains all the Macros (provided by this module) organized by categories. This may be used "in place of" or "near" the Foundry core hotbar.
    - a specific hotbar called "Communitybar" that contains the current active Community and its player-character members. It displays also some health information.
    - specific keybindings for quick changing some settings (for Adversary sheets) and reload the Macrobar

## Support
If you want to support my work on The One Ring, you may <a href="https://www.buymeacoffee.com/Ghorin">Buy me a coffee</a>.

## Installation
Install from FoundryVTT setup / Add-on modules / 
   - Search for the module with filter "macros for tor2e"
   - Install module with Manifest URl : https://gitlab.com/ghorin/tor-2e-macros/-/raw/main/module.json

## How to use

### The macros
All the Macros provided by this module are available via 2 ways :
- inside Macro compendiums :
    - tor-2e-macros-french : for french speaking players
    - tor-2e-macros-spanish : for spanish speaking players          Thanks to Torment
    - tor-2e-macros-english : for english speaking players
    - tor-2e-macros-portugues : for português speaking players      Thanks to eduFernandes
    - tor-2e-macros-polski : for polski speaking players            Thanks to Neotix

    ==> Open the Compendium corresponding to your language, and drag&drop the macros into your Hotbar.

- by manually creating Hotbar macros (type = Script) and writing down in it the macro command line. The command line is indicated in below table.

Several macros have specific settings that may be changed by the LoreMaster. Go into Foundry Game settings tab > Configure Settings > TOR2e Macros and Macrobar.
- Visibility of Eye of Mordor : in case you use the Eye of Mordor core rule, you define here whether you want to players to see in the chat the "Eye Awareness Increase" and "The Hunt has started" alerts.
- Reports : for the Macros that displays reports in the chat, you can define here :
    - "Player character : image" : By default the PCs are displayed by their name. Activate the checkbox here if you prefer to have the character image instead.
    - "Physical / moral health ..." : In the Health reports, the Endurance is compared to the total Load in order to see if the character might be Weary. It also compares the Hope to the Shadow in order to see if the character might be Miserable. If you don't activate that option, the Endurance/Hope and the Load/Shadow will be displayed separated. If you select that option, a vertical red bar will be displayed for Load/Shadow on the Endurance/Hope bars respectively. 
    - "Dark theme colors" : By default the colors used in the reports might not be easy to read within a Foundry with dark theme UI (like provided by "Tolkien UI (Dark)" module). In that case, you may select that option and the colors used in reports will be more easy to read.


### The Macrobar
By default after first installation of the Macros module, both Official Foundry Hotbar and the Macrobar are displayed :
- The official Foundry Hotbar is displayed in the bottom-left of the screen
- The Macrobar is displayed on the left higher than the Hotbar. 

The macrobar provides several ready-to-use macros that are mostly specific to The One Ring system. 
They are all organized into categories which title is showned in white font in vertical mode on the left of its macros.
You click on a slot to launch the related macro.

You can move the Macrobar by drag&dropping the left reddish anchor. You can thus put the Macrobar at the position that best suits your screen size / resolution and your taste. The new position is automatically saved and the Macrobar will load at this place at next connection to the game.
You can fold / unfold the categories by two ways :
1) Click on a category title will fold or unfold its contents. The state of the folded/unfolded category is automatically saved and it will display in this mode at next connection to the game.
2) click on the "left / right arrows" button on the left of the macrobar will switch the Macrobar into 3 display modes :
    a. Display the Saved configuration: categories will be folded / unfolded based on the last saved configuration.
    b. Display the Maximum : all categories are unfolded (but nothing is saved, this is a temporary display)
    c. Display the Minium : all categories are folded (but nothing is saved, this is a temporary display)

The LoreMaster can change general settings for the Macrobar by going into : Game settings tab > Configure Settings > TOR2e Macros and Macrobar. It gives 3 differents kinds of settings :
    a. Display : where you decide to display or not the Official Foundry Hotbar, and the Macrobar. You can have both at same time, or only one, or none
    b. Contents : here you can decide of categories of Macros that you will never use and thus you uncheck the unuseful categories => they won't be displayed in the Macrobar

### The Communitybar
The Communitybar is an optional bar that may be activated or not (in Foundry settings menu). It can be displayed along with the official Hotbar and along the Macrobar.
It displays a simple view of the current active Community and all its player-characters members. For each PC, it gives extra health information that may be detailed or approximate (for the players) depending of your choice of settings.
From the Communitybar :
- by clicking on the Community image or on a PC image, you automatically opens its character sheet.
- by clicking on the Endurance or Hope bar, you center the scene canvas on the related character token (if it's already in the scene)
- by drag&dropping the PC image to the scene canvas, you add the character token on the scene

## CREDITS
Special thanks to 
- Throdo Proudfoot : Developer of TOR1e and TOR2e systems  
- eduFernandes : Português language and Eye Awareness initial code and idea
- Torment : Spanish language
- Matt (3Skulls.tv) : Initial code and idea for a quick and simple Roll Dice tool
- ShadownHeart : Full code for "Skill roll" macro
- Neotix : Polski language



## List of macros

### Macros for tokens and tiles

| Name | Description | Command line |
| ------ | ------ |------ |
| Hidden or not | Hide or unhide selected token(s) or tile(s) | game.tor2eMacros.token_Set_Hidden_or_not() |
| Token size | Change the size of a selected token | game.tor2eMacros.token_Set_Size() |
| Token image share  | Enlarge and show the selected token image to all players | game.tor2eMacros.token_DisplayTokenImage() |


### Macros for messages

| Name | Description | Command line |
| ------ | ------ | ------ |
| Chat message | Send a chat message via a selected list of players | game.tor2eMacros.chat_SendMessage() |
| Bubble message | Add a bubble message to a selcted token | game.tor2eMacros.chat_SendMessageBuble() |


### Macros for Eye of Mordor, and Adversaries

| Name | Description | Command line |
| ------ | ------ | ------ |
| Increase Eye Awareness | Will increase the current Eye Awareness in the default Community sheet and start the Hunt in the threshold is met | game.tor2eMacros.community_eyeAwareness() |
| Change Region | Ask for the new Region and apply the related Hunt Threshold | game.tor2eMacros.community_huntLimit() |
| Hate up | Increment the Hate current value of the selected adversary token | game.tor2eMacros.adv_increment_hate() |
| Hate down | Decrement the Hate current value of the selected adversary token | game.tor2eMacros.adv_decrement_hate() |

### Macros for reports

| Name | Description | Command |
| ------ | ------ |------ |
| Physical health report | List (in the chat) an overview of players characters physical health (Endurance, Fatigue, Weary and Wound conditions) | game.tor2eMacros.characters_List_Health_Physical() |
| Moral health report | List (in the chat) an overview of players characters moral health (Hope, Miserable condition) | game.tor2eMacros.characters_List_Health_Moral() |
| Wisdom & Valour report | List (in the chat) an overview of players characters Wisdom and Valour values | game.tor2eMacros.characters_List_Valour_Wisdom() |
| Characters synthesis | Display a synthesis of all player characters (of current company) with their main characteristics  | game.tor2eMacros.mj_charactersSynthesis() |
| Session Progression | Add Adventure and Skills points at end of a game session  | game.tor2eMacros.characters_Add_SessionProgression() |


### Macros for Health status toggle

| Name | Description | Command line |
| ------ | ------ | ------ |
| Toggle WEARY status | Put or remove Weary status  | game.tor2e.macro.utility.setHealthStatus("weary") |
| Toggle POISONED status | Put or remove Poisoned status  | game.tor2e.macro.utility.setHealthStatus("poisoned") |
| Toggle WOUNDED status | Put or remove Wounded status  | game.tor2e.macro.utility.setHealthStatus("wounded") |
| Toggle MISERABLE status | Put or remove Miserable status  | game.tor2e.macro.utility.setHealthStatus("miserable") |
| Unconscious or not | Add or remove a large Unconscious effect icon over selected token(s) | game.tor2eMacros.token_Set_DeadOrNot() |
| Dead or not | Add or remove a large Dead effect icon over selected token(s) | game.tor2eMacros.token_Set_DeadOrNot() |
| Max physical health | Change the character of the selected token : current Endurance is put to Max Endurance, all Fatigue points are removed  | game.tor2eMacros.characters_Set_EtatPhysiqueMax() |
| One more day... | Decrease the duration of wounds and remove the Wounded status if duration is now to zero | game.tor2eMacros.characters.characters_reduceWoundDuration() |


### Macros for dices roll

| Name | Description | Command line |
| ------ | ------ | ------ |
| Skill roll | Select a PC token, run the macro, it displays the list of skills and clic on the one to roll  | game.tor2eMacros.skills_character_roll() |
| Private Skill roll | Select one or several PC token(s), run the macro, choose the skill and optional die bonus and roll ! | game.tor2eMacros.skills_private_roll() |
| Attack | Attack action : display the list of equipped weapon, click on one for attacking  | game.tor2eMacros.combat_lanceAttaque() |
| Manual dice roll | Roll dice with a manual choice of feat and success dices  | game.tor2eMacros.roll_ManualRoll() |


## License
L'Anneau Unique, La Terre du Milieu et Le Seigneur des Anneaux, ainsi que les personnages, objets, événements et lieux associés sont des marques déposées ou des marques enregistrées de Saul Zaentz Company d/b/a Middle-­earth Enterprises (SZC) et sont utilisés sous licence de Sophisticated Games Ltd. Tous droits réservés.

## Property rights
By respect for property rights and the licence, this module doesn't contain any description text, rule description or illustration coming the The One Ring books.


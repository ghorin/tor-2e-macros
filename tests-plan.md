<html>
  <head>
    <title>Tests Plan</title>
  </head>
  <body>
    <table>
      <thead>
        <tr>
          <th>PERIMETER</th>
          <th>FUNCTION</th>
          <th>TEST</th>
          <th>EXPECTED RESULT</th>
          <th>NOTES</th>
        </tr>
      </thead>
      <!-- PERIMETER -->
      <tr>
        <th>PERIMETER</th>
        <th>Function</th>
        <td>Test</td>
        <td>
          Expected result
          <ul>
            <li>result 1</li>
            <li>result 2</li>
            <li>result 3</li>
          </ul>
        </td>
        <td></td>
      </tr>
    </table>
  </body>
</html>